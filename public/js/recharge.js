host = window.location.host;
web_url = window.location;
var b2c = (typeof b2c != "undefined") ?  b2c : {};
var opr = {"AC":"aircel.png","AT":"airtel_mo.png","CG":"BSNL.png","DC":"videocon.png","DP":"mtnl.png","ID":"idea.png","LM":"default.png","MT":"mts.png","RC":"Reliance.png","RG":"Reliance.png","TD":"Docomo.png","TI":"tata_indicom.png","UN":"uninor.png","VF":"vodafone.png","T":"tata_indicom.png","AD":"Airtel_digiTV.png","BD":"bigTV.png","DD":"dishTV.png","SD":"Sun_direct.png","VD":"videocon.png"};
var opr_dth =  {"AD":"Airtel_digiTV.png","BD":"bigTV.png","DD":"dishTV.png","SD":"Sun_direct.png","TD":"tata_sky.png","VD":"videocon_d2h.png"}
var opr_data =  {"AC":"aircel.png","AT":"airtel_mo.png","CG":"BSNL.png","DC":"videocon.png","DP":"mtnl.png","ID":"idea.png","LM":"default.png","MT":"mts.png","RC":"Reliance.png","TI":"tata_indicom.png","VF":"vodafone.png"}
b2c.core = {
     init:function(){
     
     },
     browseplan :function(plandetails,operator,circletype,operatortype){
         var plandetails = "getPlanDetails";
	 var serviceurl = panelUrl+"/apis/receiveWeb/mindsarray/mindsarray/json?method?";
         var operatorid = operator;
         var circle = circletype;
         var html = "";
         var associativeArray = new Array();
         var plantype = new Array();
         $.ajax({
            url: serviceurl,
            type:"POST",
            dataType: "jsonp",
            data:{method:plandetails,operator:operatorid,circle:circle},
			jsonp: 'root',
            success:function(data)
            {
             $.each(data, function( key, value) {
                    $.each(value, function(k,v){
                    if(typeof v == "object")
                     {
                          $.each(v.circles, function(pkey,pval){
                              i = 0;
                              $.each(pval.plans, function(a,b){
                                  if(a!="None"){
                                  var classname = (i==0)?"active":"";
                                  if(a=='Data/2G'){
                                      a = '2G'
                                  }
                                  html+='<li class='+classname+'><a href=#'+a+' role="tab" data-toggle="tab">'+a+'</a></li>';
                                  }
                                  var newElement = {};
                                  newElement[a] = b;
                                  associativeArray.push(newElement);
                                  plantype.push(a);
                                  i++;
                              });

                          });
                      }

                     });

                      });
                $("#"+operatortype+"_plan").html('');
               $("#"+operatortype+"_plan").append(html);
               var htmlbody = '';
               var htmlcontent = '';
                $("#"+operatortype+"_plandetails").html('');
               var tabclass = '';
               j = 0;
               $.each(plantype,function(plankey,planval){
               if(planval!="None"){
               var tabclass = (j== 0)? "tab-pane active ":"tab-pane ";
               htmlcontent +='<div class="'+tabclass+'"  height="100" width="300" id='+planval+'><div class="table_scroll"><table class="table table-responsive text-center"><tbody><tr class="thead2"><td style="width:10%">Price</td><td style="width:10%">Validity</td><td>description</td></tr>'
               $.each(associativeArray,function(k,v){
                    $.each(v,function(key,val){
                        if(key == planval ){ 
                             $.each(val,function(datakey,dataval){ 
                                 var planamt = "\'" + dataval.plan_amt + "\'";
                                 var planvalidity = "\'"+dataval.plan_validity +"\'";
                                 var plandesc = "\'"+dataval.plan_desc+"\'";
                                 var type = "\'"+operatortype+"\'";
                                 htmlcontent+='<tr onclick="getplanDetails('+planamt+','+planvalidity+','+plandesc+','+type+')"><td><i class="fa fa-inr"></i> '+dataval.plan_amt+'</td><td>'+dataval.plan_validity+'</td><td style="text-align:left;padding-right: 15px;">'+dataval.plan_desc+'</td></tr>'
                                 });
                                 htmlcontent+='</tbody></table></div></div>';
                                 $("#"+operatortype+"_plandetails").append(htmlcontent);
                                 htmlcontent = '';
                        }
                     });
                    });
                     j++;
                     }
                     else
                     {
                      htmlcontent+="<table><tr><td colspan='3' class='tab-content mT20' width='670' align='center'><b>No Plans Found</b></td></tr></table>";
                      $("#"+operatortype+"_plandetails").append(htmlcontent);
                     }
					 $("."+operatortype).show();
                    
                  });
            }
        });
     },
     getPlandescription:function(amt,validity,desc,type){
         
          var mobnumber = $("#"+type+"_number").val();
          var wallet_bal = $("#walletbal").val();
          var specialrchg = 0;
          if($("#spl_rchg").css('display')!='none'){
              specialrchg = $('input:radio[name=special_recharge]:checked').val();
          }
          var maxlimit = $("#"+type+"_max").val();
          var minlimit = $("#"+type+"_min").val();
          if(type=="post"){
           var charge = $("#"+type+"_charge").val();
           var servicechrg = charge.split(",");
           var mincharge = servicechrg[0].split(":");
           var maxcharge = servicechrg[1].split(":");
           var totalamt = parseInt(amt)+parseInt(mincharge[1]);
           $("#trans_amount").html(totalamt);
           if(parseInt(wallet_bal)<parseInt(totalamt)){
              $("#wallet").hide();
              $("#walletmsg").html("Insufficient balance in wallet");
           }
           else{
                $("#walletmsg").html("");
                $("#wallet").show();
              }
            }
           else {
               if(parseInt(wallet_bal)<parseInt(amt)){
                $("#wallet").hide();
                $("#walletmsg").html("Insufficient balance in wallet");
               } 
                else {
                $("#walletmsg").html("");
                $("#wallet").show();
                 }
                $("#trans_amount").html(amt);
            }
         
           $("#"+type+"_amount").val(amt);
           $("#number").html(mobnumber);
           if(desc!=''){
           $("#description").html(desc);
           }
           if(type =="mob"){
               $("#service_msg").hide();
               $("#recharge_type").html("Prepaid Mobile Number");
           }
           else if(type=="post"){
               $("#recharge_type").html("Postpaid Mobile Number");
               $("#service_msg").show();
                $("#service_msg").html("("+parseInt(totalamt)+" ="+parseInt(amt)+  "+"  +$("#service_charge").val()  +  "    service charge inclusive of all taxes)");
           }
           else if(type=="data"){
               $("#service_msg").hide();
                $("#recharge_type").html("Data Card Number");
           
           }
           else if(type=="dth"){
                $("#service_msg").hide();
                $("#recharge_type").html("DTH Number");
           }
           //$("#provider_name").html($("#"+type+"_provider option:selected").text());
           $("#rec_amount").val(amt);
           $("#rec_flag").val($("#"+type+"_flag").val());
           $("#rec_number").val(mobnumber);
           $("#stv").val(specialrchg);
           var rechargeoperator = $("#"+type+"_id").val();
           $("#rec_operator").val(rechargeoperator); 
                if(desc!=""){
                 // $("#show"+type+"plan").hide();
            }
         
     },
     checkcircle:function(number,plandetails,productid,circle,type){
         var serviceurl = panelUrl+"/apis/receiveWeb/mindsarray/mindsarray/json?";
	     var methodtype = "getMobileDetails";
             $.ajax({
		           url:serviceurl,
					data:{method :methodtype,mobile: number},
					type: "POST",
					dataType: 'jsonp',
					jsonp: 'root',
             success:function(data){
					 if(data[0].status=="success"){
					 var circlevalue =  data[0].details.area;
					 if(circlevalue==''){ 
					 if(type=="mob"){    
					 $("#mobile_circle").show();
					 }
					 else if(type=="data"){
					  $("#dat_circle").show();
					 }
					 b2c.core.browseplan(plandetails,productid,circle,type);
					 }
					 else{
						 $("#"+type+"_circle").val(circlevalue);
						 $("#"+type+"_checkplan").show();
					     b2c.core.browseplan(plandetails,productid,circlevalue,type);
					 }
					}
					else {
					 if(type=="mob"){  
					 $("."+type).show();
					 $("#mobile_circle").show();
					 if(circle!=''){
					 b2c.core.browseplan(plandetails,productid,circle,type);
					}
					 }
					 else if(type=="data"){
						$("."+type).show();
						$("#dat_circle").show();
						if(circle!=''){
						b2c.core.browseplan(plandetails,productid,circle,type);
					}
					 }
					}

				  },error: function (xhr,error) {
						console.log(error);
					},
				  });
       
     },
     validate_dth : function(opr_id,subsciber_id){
         var len = subsciber_id.length
         var sub1 = subsciber_id.substring(0,1)
         var sub2 = subsciber_id.substring(0,2)
        
        if(opr_id == 1 && (len != 10 || sub2 != "30")){//Airtel DTH
            return false;
        }
        else if(opr_id == 2 && (len != 12 || sub2 != "20")){//Big TV
             return false;
        }
        else if(opr_id == 3 && (len != 11)){//Dish TV
             return false;
        }
        else if(opr_id == 4 && (len != 11 || $.inArray(sub1,['1','4'])==-1)){//Sun TV
             return false;
        }
        else if(opr_id == 5 && (len != 10 || $.inArray(sub2,['10','11'])==-1)){//Tata Sky
             return false;
        }
        return true;
     },
     payment:function(amt,opt,flag,number){
           var amt = $("#"+amt).val();
           var operator = $("#"+opt).val();
           var flag  = $("#"+flag).val();
           var number = $("#"+number).val();
           var cb_code = $("#user_voucher_code").val();
           var paymenturl = host+"/home/success/";
           var provider_name = $("#provider_name").html();
           var type = $("#recharge_type").html();
           var specialrecharge = $("#stv").val();
           $("#orderconfirmation").hide();
            var paymentmode = 'wallet';
              if($("#usewallet").is(":checked") && $("#usewallet").css("display") != "none"){
                  paymentmode = 'wallet';
              }else{
                  paymentmode = 'online';
              }
              $('.loader').show();
        if(flag=="1" || flag=="3"){  
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/recharge/?";
             $.ajax({
                    url: url,
                    type:"GET",
                    data:{mobile_number:number,
                         operator:operator,
                         flag:flag,
                         amount:amt,
                         recharge:"1",
                         paymentopt:paymentmode,
                         payment : "1",
                         cb_code : cb_code,
                         partial:"1",
                         stv:specialrecharge,
                         return_url: paymenturl,
                         api_version:3,
                         res_format : "jsonp"
                        },
                   timeout: 50000,
                   dataType: "jsonp",
                   async:false,
                   //jsonpCallback: 'callback',
                  // crossDomain: true,
                    success:function(data){ 
                       if(data.status=="success"){
                        localStorage.setItem("amount",amt);
                        localStorage.setItem("number",number);
                        localStorage.setItem("provider",provider_name);
                        localStorage.setItem("type",type);
                        if(data.description.online_form==false){
                             window.location ="/home/success?status="+data.status+"&txnid="+data.description.transaction_id+"&balance="+data.description.closing_balance
                             }
                           else{
                               $("#transaction_form").append(data.description.form_content)
                               $("#pg_payu_form").submit();
                               $("#transaction_form").html('');
                          }
                    }
                    else if(data.status=="failure" && data.errCode==201) {
                        alert("Youre session has been time out");
                        window.location="/home/logout/";  
                     }
                     else if(data.status=="failure") {
                          alert(data.description);
                          window.location="/home/";
                     }
                   },
                    beforeSend: function(){
                       $("#orderconfirmation").hide();
                       $('.loader').show();
                    },
                    complete: function(){
                       $('.loader').hide();
                     },
                    error: function (xhr,status,error) {
                        console.log(xhr);
                        console.log(error);
                        console.log(status);
                    }

               });
            }
            else if(flag=="2"){ 
            
                var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/recharge/?";
                $.ajax({
                url: url,
                type:"GET",
                data:{subscriber_id:number,
                      operator:operator,
                      flag:flag,
                      amount:amt,
                      recharge:"1",
                      paymentopt:paymentmode,
                      payment : "1",
                      return_url: paymenturl,
                      res_format : "jsonp",
                      cb_code : cb_code,
                      partial:'1',
                      api_version:'3',
                      request_via : 'web',
                     },
                timeout: 50000,
                dataType: "jsonp",
                jsonpCallback: 'callback',
                crossDomain: true,
                success:function(data){
                 if(data.status=="success"){
                        localStorage.setItem("amount",amt);
                        localStorage.setItem("number",number);
                        localStorage.setItem("provider",provider_name);
                        localStorage.setItem("type",type);
                        if(data.description.online_form==false){
                         window.location ="/home/success?status="+data.status+"&txnid="+data.description.transaction_id+"&balance="+data.description.closing_balance
                         }
                       else{
                           $("#transaction_form").append(data.description.form_content)
                           $("#pg_payu_form").submit();
                           $("#transaction_form").html('');
                           }
                 } 
                 else if(data.status=="failure" && data.errCode==201) {
                            alert("Youre session has been time out");
                            window.location="/home/logout/";  
               }
                else {
                    alert(data.description);
                    window.location="/home/";

                 }
              },
               beforeSend: function(){
                   $("#orderconfirmation").hide();
                   $('.loader').show();
                   },
               complete: function(){
                 $('.loader').hide();
                 },
                error: function (xhr,status,error) {
                            console.log(xhr);
                            console.log(error);
                            console.log(status);
                        }
                });
            
            }
            else if(flag=="4"){
                var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/recharge/?";
                var service_charge = $("#service_charge").val();
                var totalamt = $("#total_amount").val();
                var service_charge_percent = $("#service_charge_percent").val();
                $.ajax({
                url: url,
                type:"GET",
                data:{mobile_number:number,
                      operator:operator,
                      flag:flag,
                      base_amount:amt,
                      service_charge:service_charge,
                      amount:totalamt,
                      payment_flag:"1",
                      service_tax:service_charge_percent,
                      billpayment : "1",
                      paymentopt:paymentmode,
                      payment:"1",
                      return_url: paymenturl,
                      cb_code : cb_code,
                      res_format : "jsonp",
                      partial:'1',
                      api_version:'3',
                      request_via : 'web',
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
                if(data.status=="success"){
                    localStorage.setItem("amount",totalamt);
                    localStorage.setItem("number",number);
                    localStorage.setItem("provider",provider_name);
                    localStorage.setItem("type",type);
                    if(data.description.online_form==false){
                     window.location ="/home/success?status="+data.status+"&txnid="+data.description.transaction_id+"&balance="+data.description.closing_balance
                    } else {
                      $("#transaction_form").append(data.description.form_content)
                      $("#pg_payu_form").submit();
                      $("#transaction_form").html('');
                     } 
               }
              else if(data.status=="failure" && data.errCode==201) {
                     alert("Youre session has been time out");
                        window.location="/home/logout/";  
               }else{
                   alert(data.description);
                    window.location="/home/";
               }
            },
             beforeSend: function(){
               $("#orderconfirmation").hide();
               $('.loader').show();
               },
           complete: function(){
             $('.loader').hide();
             },
             error: function (xhr,status,error) {
                        console.log(xhr);
                        console.log(error);
                        console.log(status);
                    }
         
            });
            }
     },
     setUserData:function(data){
        var mobileNo = data.description["mobile"];
        var name = data.description["name"];
        var email = data.description["email"];
        var dob = data.description["date_of_birth"];
        var longitude =  data.description["longitude"]; 
        var latitude =  data.description["latitude"]; 
        var walletbal = data.description["wallet_balance"];
        var device_type = data.description["device_type"];
        var gender = data.description["gender"];
        var loyaltypoints = data.description["loyalty_points"];
        var totalgifts = data.description["total_gifts"];
        var mylikes = data.description["total_mylikes"];
        var myreview = data.description["total_myreviews"];
        var redeem = data.description["total_redeem"];
        //var password = localStorage.getItem('passflag');
        var user_id = data.description["user_id"];
        $.ajax({
            url: "/home/setUserInfo",
            type:"POST",
            data:{
                 name:name,
                 email:email,
                 dob:dob,
                 mobile:mobileNo,
                 device_type:device_type,
                 gender:gender,
                 walletbal : walletbal,
                loyaltypoints : loyaltypoints,
                totalgifts : totalgifts,
                mylikes : mylikes,
                myreview : myreview,
                redeem : redeem,
                 user_id : user_id
                 },
            timeout: 50000,
            dataType: "json",
            success:function(data){
                // alert(name+'||'+mobileNo);
			if(data.status=='success'){	
			$("#isloggedin").val('1'); 
                        $('#basicModal').modal('hide');
			 //location.reload();
                         $('#balance').html(walletbal);
                         $('#walletbal').val(walletbal);
                         if(parseInt(walletbal)!=0){
                            $('#walletbal_val').html(parseInt(walletbal));
                            $('.walletbal_text').css("display","");
                         }
                         $('#signin').css("display","none");
                         $('#welcome').css("display","block");
                         $('#redeem_giftcoin').css("display","none");
                         $('#gift_store_button').css("display","block");
                         if(name && name!="")
                         $('#wlecomeUsername').html(name);
                        else
                         $('#wlecomeUsername').html(mobileNo);
                         if(localStorage.getItem("redeem") == "1"){ 
                                           // location.href = "/giftstore";
                                            localStorage.removeItem("redeem");
//                                            window.location="/giftstore";
                         }
		    }
            },
            beforeSend: function(){
               $('.loader').show();
              },
            complete: function(){
             $('.loader').hide();
             },
            error: function (error) {
            }
        });
        
     },
     orderinformation:function(type){
          $("#provider_name").html($("#"+type+"_provider"+" option:selected" ).text());
         var amt = $("#"+type+"_amount").val();
          var number = $("#"+type+"_number").val();
          var operator = $("#"+type+"_id").val();
          var type = type;
          var maxlimit = $("#"+type+"_max").val();
          var minlimit = $("#"+type+"_min").val();
          var flag = $("#isloggedin").val();
          var placeholder = $("#"+type+"_number").attr("placeholder");
          var  serviceprovider = $("#"+type+"_provider").val();
          if(type=='dth'){
              $("#operator_logo").attr("src", '/assets/images/operator/'+opr_dth[serviceprovider]);
          }
          else if(type=='data'){
              $("#operator_logo").attr("src", '/assets/images/operator/'+opr_data[serviceprovider]);
          }
          else{
              $("#operator_logo").attr("src", '/assets/images/operator/'+opr[serviceprovider]);
          }
          
          
           if(flag!='1'){
			  $("#basicModal").modal('toggle');
              return false;
          }
          $("#orderconfirmation" ).removeClass("modal fade orderconfirmation");
           $("#orderconfirmation").hide();
           
          if(type=="post"){
               var charge = $("#"+type+"_charge").val();
               if(charge!=''){
               var servicechrg = charge.split(",");
               var mincharge = servicechrg[0].split(":");
               var maxcharge = servicechrg[1].split(":");
               }
               var service_charge_percent = $("#service_charge_percent").val();
               
               if(number.length!=10){
                  alert("Please Enter Valid Mobile Number");
                   $("#"+type+"_number").focus();
                  return false;
               }
              else if(parseInt(amt)>parseInt(mincharge[0])){
                   var totalamt = (parseInt(amt)+parseInt(mincharge[1]));
                   $("#service_charge").val(parseInt(mincharge[1]));
                   $("#total_amount").val(parseInt(totalamt));
               }
               else {
                   var totalamt = (parseInt(amt)+parseInt(maxcharge[1]));
                   $("#service_charge").val(parseInt(maxcharge[1]));
                   $("#total_amount").val(parseInt(totalamt));
               }
          }
           if(type=="mob" || type=="data"){ 
              if(number.length!=10){
                 $("#incorrect_mobile_num").html("<small>Please enter a valid 10 digits Mobile Number.</small>");
                 $("#"+type+"_number").focus();
                 return false;
              }
			}
           if(number=='' && type=='mob'){
               alert("Please Enter Prepaid Mobile Number");
               $("#"+type+"_number").focus();
               return false;
           }
           else if(number=='' && type=='data'){
               alert("Please Enter Data card Number");
               $("#"+type+"_number").focus();
               return false;
           }
           else if(number=='' && type=='post'){
               alert("Please Enter PostPaid Mobile Number");
               $("#"+type+"_number").focus();
               return false;
           }
           else if(operator==''){
               alert("Service Provider can not be blank");
               $("#"+type+"_provider").focus();
               return false;
           }
           else if(number=='' && type=='dth'){
                $("#dth_error_msg").html("Please  "+placeholder);
               $("#"+type+"_number").focus();
               $("#dth_error_msg").show();
               return false;
           }else if( type=='dth' && !b2c.core.validate_dth(operator,number)){
               
                    $("#dth_error_msg").html('wrong subcriber id');
                    $("#dth_error_msg").show();
                    return false;
             
           }
           else if(amt==''){
               alert("Please Enter Amount");
               $("#"+type+"_amount").focus();
               return false;
           }
            else if(parseInt(amt)==0){
               alert("Please enter amount between range "+minlimit+" to "+ maxlimit);
               $("#"+type+"_amount").val('');
               $("#"+type+"_amount").focus();
               return false;
           }
           else if((parseInt(amt)!='') && (parseInt(amt)>parseInt(maxlimit))){
               
               alert("Please enter amount between range "+minlimit+" to "+ maxlimit);
                $("#"+type+"_amount").val('');
                $("#"+type+"_amount").focus();
               return false;
           }
           else if((parseInt(amt)!='') && (parseInt(amt)<parseInt(minlimit))){
               
               alert("Please enter amount between range "+minlimit+" to "+ maxlimit);
                $("#"+type+"_amount").val('');
                $("#"+type+"_amount").focus();
               return false;
           }           
           else{ 
               $("#dth_error_msg").hide();
              $("#orderconfirmation" ).addClass("modal fade orderconfirmation");
			  $("#orderconfirmation").show();
              b2c.core.getPlandescription(amt,operator,"",type);
           }
         setTimeout(function(){
           $("#coupon_code").focus();
          
        }, 1);
         
     },
     checkotp:function(event){
        var mobileno = localStorage.getItem("mob_number");
        var gender = localStorage.getItem("gender");
        var otp = $("#otp").val();
        var resonseotp = localStorage.getItem("otp");
        var confirmotp = md5(mobileno)+md5(otp);
        if(otp=='') {
            $("#sign_otp_msg").html("<small>Please enter a one time password.</small>");
            event.preventDefault();
            return false;
        }
        else if(confirmotp != resonseotp){
            event.preventDefault();
            $("#sign_otp_msg").html("<small>Invalid Otp</small>");
            return false;
        }
        else
        {
            var loginurl = b2cUrl+"/index.php/api_new/action/actiontype/signin/api/true/?";
            $.ajax({
            url: loginurl,
            type:"GET",
            data:{username:mobileno,
                  password:otp,
                  uuid:"",
                  latitude:"",
                  logitude:"",
                  device_type:"Web",
                  res_format : "jsonp",
		  api_version : "3"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
               if(data.status=="success"){
                   $("#sign_success").html("<small>Signup successfully !!! </small>");
                   b2c.core.setUserData(data);
               }
                else if(data.status=='failure' && data.errCode==201){
                    $("#basicModal").modal('toggle');
                  }
              },
              beforeSend: function(){
              $('.loader').show();
              },
             complete: function(){
             $('.loader').hide();
             },
             error: function (xhr,error) {
             },  
         
            });
             
        }
     },
     register:function(){
         var mobileNo = $("#mobnumber").val();
	   if(mobileNo =='' || !(IsMobileNumber(mobileNo))){
            $('#mobnumber').val("");   
            $("#valid_mobile_num").html("<small>Please enter a valid Mobile Number</small>");
            return false;
          }
          else {
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/register_user/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{mobile_number:mobileNo,
                  device_type:"Web",
                  api_version : 4,
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
                if(data.status=="success" && data.errCode=='0'){
					localStorage.setItem("mob_number",mobileNo);
					localStorage.setItem("otp",data.description.otp);
					$(".mobno").hide();
                                        $("#valid_mobile_num").empty();
                                        //$("#sign_otp_sent").html("<small>Please enter the OTP sent on your registered Mobile No.</small>");
					$(".otp").show();
					$("#otp").attr("placeholder", "Enter One Time Password").blur();
                                        $("#login_signup").hide();
                                        $("#register_user").hide();
					//$("#check_otp").show();
                }
                else if(data.status=="success" && data.errCode=='202'){
                    $(".mobno").hide();
                    $(".login_font").hide();
                    $("#sign_msg").html("<small><strong>"+data.description+"</strong></small>");
                    localStorage.setItem("mob_number",mobileNo);
                    $("#register_user").hide();
                    $("#login_signup").show();
                }
                else { 
					$(".mobno").hide();
					$(".otp").show();
					$("#sign_msg").html("<small><strong>"+data.description+"</strong></small>");
					$("#register_user").hide();
                                        $("#login_signup").hide();
					//$("#check_otp").show();
					$("#otp").attr("placeholder", "Enter Password").blur();
					localStorage.setItem("mob_number",mobileNo);
					localStorage.setItem("otp",data.description.otp);
				}
            },
            error: function (xhr,error) {
             },  
         
            });
	   
   }
     },
     logout:function(){
       var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/signout/?";
	   $.ajax({
            url: url,
            type: "GET",
            data: { 
				    api_version:"3",
					res_format: "jsonp"
                  },
            timeout: 50000,
            dataType: "jsonp",
            crossDomain: true,
			success: function(data) {
				if(data.status=="success"){
                                    localStorage.clear();
				  window.location = '/home/logout/'			
				}
            },
            beforeSend: function(){
             $('.loader').show();
             },
             complete: function(){
             $('.loader').hide();
             },
            error: function(xhr, error) {
				
          },
        });
     },
     validateQuickpayamount : function(){
        var amt = $("#postpaid_amount").val();
        var maxlimit = $("#max_amount").val();
        var minlimit = $("#min_amount").val();
        var charge = $("#charge_slab").val();
        var servicechrg = charge.split(",");
        var mincharge = servicechrg[0].split(":");
        var maxcharge = servicechrg[1].split(":");
        var service_charge_percent = $("#service_charge_percent").val();
        if (amt == ''){
            alert("Amount can not be blank");
            $("#postpaid_amount").focus();
            return false;
        }
        else if ((amt != '') && (amt > parseInt(maxlimit))) {
            alert("Amount can not be greater than  " + maxlimit);
            $("#postpaid_amount").focus();
            $("#postpaid_amount").val('');
            return false;
        }
        else if ((amt != '') && (amt < parseInt(minlimit))) {
            alert("Amount can not be less than  " + minlimit);
            $("#postpaid_amount").focus();
            $("#postpaid_amount").val('');
            return false;
        }
        else if ((amt) > (parseInt(mincharge[0]))) {
            var totalamt = (parseInt(amt) + parseInt(mincharge[1]) + parseInt(service_charge_percent));
            $("#service_charge").val(parseInt((mincharge[1])));
            $("#total_amount").val(totalamt);
            $("#rec_amount").val(amt);
            $("#quickpay_amount").hide();
            $("#trans_amount").show();
            $("#trans_amount").html(totalamt);
            $("#service_msg").show();
            $("#service_msg").html("(" + parseInt(totalamt) + " =" + parseInt(amt) + "+" + $("#service_charge").val() + "    service charge inclusive of all taxes)");
        }
        else {
            var totalamt = (parseInt(amt) + parseInt(maxcharge[1]) + parseInt(service_charge_percent));
            $("#service_charge").val((maxcharge[1]));
            $("#total_amount").val(totalamt);
            $("#rec_amount").val(amt);
            $("#quickpay_amount").hide();
            $("#trans_amount").show();
            $("#trans_amount").html(totalamt);
            $("#service_msg").show();
            $("#service_msg").html("(" + parseInt(totalamt) + " =" + parseInt(amt) + "+" + $("#service_charge").val() + "    service charge inclusive of all taxes)");
        }
      },
      getQuickPaylist : function(cnt){
       $("#quickpaydata").html("");
        var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/get_quickpaylist/?";
        var html = "";
        var i = 0
        $.ajax({
				url: url,
				type: "GET",
				timeout: 50000,
				dataType: "jsonp",
				jsonpCallback: 'callback',
           data: {
                res_format: "jsonp"
                 },
            crossDomain: true,
            success: function(data) { 
                if (data.status == "success"){ 
                    if (data.description.length > 0) {
                        $.each(data.description, function(key, val) {
                            
                                var number = "\'" + val.number + "\'";
                                var amount = "\'" + val.amount + "\'";
                                var flag = "\'" + val.flag + "\'";
                                var name = "\'" + val.name + "\'";
                                var opreator = "\'" + val.operator_id + "\'";
                                var delegate_flag = "\'" + val.delegate_flag + "\'";
                                var confirmation = "\'" + val.confirmation + "\'";
                                var confirmation = "\'" + val.confirmation + "\'";
                                var id = "\'" + val.id + "\'";
                                var stv = "\'" + val.stv + "\'";
                                var missedcall = "\'" + val.missed_number + "\'";
                                var operatorname = "\'" + val.operator_name + "\'";
                                var operatorcode = "\'" + val.operator_code + "\'";
                                var datetime = "\'" + val.datetime + "\'";
                                var date_str = datetime.split(" ");
                                var date = convertDate(date_str[0]);
                                                 html+= '<div class="col-sm-6 col-md-6 margin-top-25">'
								 html+= '<table class="recharge table-bordered offer_tab" style="width:250px">'
								 html+= '<tr>'
								 if (val.flag == "4") {
								 html+= '<td style="width:90px" class="amount" rowspan="2" data-toggle="modal"  data-target=".orderconfirmation" onclick="showquickpay(' + number + ',' + name + ',' + amount + ',' + opreator + ',' + delegate_flag + ',' + confirmation + ',' + flag + ',' + id + ',' + stv + ',' + missedcall + ',' + operatorname + ',\'postpaid\',' + operatorcode + ');">'+date+'</td>'
							     } else {
									  html+= '<td style="width:90px" class="amount" rowspan="2" data-toggle="modal"  data-target=".orderconfirmation" onclick="showquickpay(' + number + ',' + name + ',' + amount + ',' + opreator + ',' + delegate_flag + ',' + confirmation + ',' + flag + ',' + id + ',' + stv + ',' + missedcall + ',' + operatorname + ',\'other\',' + operatorcode + ');"><i class="fa fa-inr"></i>'+val.amount+'</td>'
								 }
                                                                 html+= '<td class="lasttd">'+val.number+'</td>'
								 html+= '</tr>'
								 html+= '<tr>'
                                                                 html+= '<td class="lasttd" >'+val.operator_name+'<button class="remove-offer"  onclick="deletequickpay_quick(' + id + ')">× </button></td>'
								 html+='</tr></table>';
							     html+='</div>';
                                 });

                    }
                    $("#quickpaydata").append(html);
                   
                }
                else if(data.status=='failure' && data.errCode==201){
                    $("#basicModal").modal('toggle');
                  }
                else if (data.status == "failure") {
                    window.location = "/home/logout/"
                }
            },
            beforeSend: function() {
                $('.loader').show();
            },
            complete: function() {
                $('.loader').hide();
            },
            error: function(xhr, error) {
            }

        });
          
      },
         Wallettopup :function(){
          var amount = $("#amount").val();
          if(amount > MAX_WALLET_BALANCE_ALLOWED){
              $("#addmoney_amount").html("Please Enter Amount less than "+MAX_WALLET_BALANCE_ALLOWED);
              $("#addmoney_amount").show();
              return false;
          }
          if(amount==""){
              $("#addmoney_amount").html("Please Enter Amount ");
              $("#addmoney_amount").show();
              return false;
          }
          $("#addmoney_amount").hide();
          var paymenturl = host+"/home/success/";
                if (amount == ""){
                    alert("Please Enter Amount");
                    $("#amount").focus();
                    return false;
                }
                else {
                    var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/online_walletrefill/?";
                    
                    $.ajax({
                        url: url,
                        type: "GET",
                        timeout: 50000,
                        dataType: "jsonp",
                        jsonpCallback: 'callback',
                        data: {
                            amount: amount,
                            return_url: paymenturl,
                            pay_by : 'web',
                            res_format: "jsonp"
                        },
                        crossDomain: true,
                        success: function(data) {
                            if (data.status == "success") {
                                localStorage.setItem("amount",amount);
                                localStorage.setItem("paymentmode",'online');
                                $("#transaction_form").append(data.description.form_content);
                                $("#pg_payu_form").submit();
                                $("#transaction_form").html('');
                            }
                            else if(data.status=='failure' && data.errCode==201){
                                $("#basicModal").modal('toggle');
                              }
                        },
                        beforeSend: function() {
                            $('.loader').show();
                        },
                        complete: function() {
                            $('.loader').hide();
                        }
                    });

                }
          
      },
      redeemvoucher : function(){
                var couponcode = $("#voucher_code").val();
                if (couponcode == ""){
                    //alert("Please Enter Coupon code");
                    $("#couponcode").html("Please Enter Coupon code");
                    $("#voucher_code").focus();
                    return false;
                }
                else {
                    var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/redeem_voucher/?";
                    $.ajax({
                        url: url,
                        type: "GET",
                        data: {
                            vcode: couponcode,
                            res_format: "jsonp"
                        },
                        timeout: 50000,
                        dataType: "jsonp",
                        jsonpCallback: 'callback',
                        crossDomain: true,
                        success: function(data) {
                            if (data.status == "success") {
                                b2c.core.checkbalance();
                                $("#couponcode").html(data.description);
                            }else if(data.status=='failure' && data.errCode==201){
                                $("#basicModal").modal('toggle');
                              }
                            else {
                                $("#couponcode").html(data.description);
                            }
                        },
                        beforeSend: function() {
                            $('.loader').show();
                        },
                        complete: function() {
                            $('.loader').hide();
                        }
                    });

                }

      },
      checkbalance : function(){
          var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/check_bal";
                $.ajax({
                    url: url,
                    type: "GET",
                    data: {
                        res_format: "jsonp"
                    },
                    timeout: 50000,
                    dataType: "jsonp",
                    jsonpCallback: 'callback',
                    crossDomain: true,
                    success: function(data) {
                        if (data.status == "success") {
                            $("#couponcode").html(data.description);
                            var balance = {description:{'wallet_balance' :data.description.account_balance }};
	                        b2c.core.setUserData(balance);
                        }else if(data.status=='failure' && data.errCode==201){
                            $("#basicModal").modal('toggle');
                          }
                        else {
                            $("#couponcode").html(data.description);
                        }
                    }
                });
      },
      updateprofile:function(data){
          
            var mobileNo = data.mobile;
            var name = data.name;
            var email = data.email;
            var dob = data.date_of_birth;
            var longitude =  data.longitude; 
            var latitude =  data.latitude; 
            var walletbal = data.wallet_balance;
            var device_type = data.device_type;
            var gender = data.gender;
            var loyaltypoints = data.loyalty_points;
            var totalgifts = data.total_gifts;
            var mylikes = data.total_mylikes;
            var myreview = data.total_myreviews;
            var redeem = data.total_redeem;
            var password = localStorage.getItem('passflag');
            $.ajax({
                url: "/home/setUserInfo",
                type:"POST",
                data:{
                     name:name,
                     email:email,
                     dob:dob,
                     mobile:mobileNo,
                     device_type:device_type,
                     gender:gender,
                     walletbal : walletbal,
                     loyaltypoints : loyaltypoints,
                     totalgifts : totalgifts,
                     mylikes : mylikes,
                     myreview : myreview,
                     redeem : redeem,
                     password : password
                     },
                timeout: 50000,
                dataType: "json",
                success:function(data){
                 if(data.status=='success'){	
                  
                    }
                },
                beforeSend: function(){
                   $('.loader').show();
                  },
                complete: function(){
                 $('.loader').hide();
                 },
                error: function (error) {
                    console.log(error);
                }
            });

     }
     ,purchase_deal: function(offer_id,deal_id,offer_price,amount,by_voucher){
          var url =b2cUrl+"/index.php/api_new/action/api/true/actiontype/purchase_deal/?";
          var paymentmode = 'wallet';
            if(by_voucher==1){
                if(typeof $("#usewallet_voucher") !== "undefined" && $("#usewallet_voucher")){
                    if($("#usewallet_voucher").is(":checked")){
                     paymentmode = 'wallet';
                    }else{
                        paymentmode = 'online';
                    }
                }
                var balance = parseInt($("#loyalty_balance").val());
                var actual_price = parseInt($("#actual_price").val());
                if(balance < amount){
                    offer_price = actual_price;
                    amount = 0;
                }
             }
             $(".loader").show();
                $.ajax({
                url: url,
                type:"GET",
                data:{
			offer_id : offer_id,
			deal_id : deal_id,
			coupon_id : "",
			paymentopt : paymentmode,
			amount : amount,
			of_price : offer_price,
			quantity : 1,
			trans_category : 'deal',
			freebie : 'true',
			partial : 1,
			api_version : 3,
			ref_id : '',
			request_via : 'web',
                      	res_format : "jsonp",
                        pay_by:"web",
                        return_url: encodeURIComponent(web_url),
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){ 
                if(data.description.online_form==true && data.status=="success"){
                        $("#transaction_form").append(data.description.form_content)
                        $("#pg_payu_form").submit();
                        $("#transaction_form").html('');
                    }
                else if(data.status=="success"){
                        var code = data.description.deal_coupon_code;
                        b2c.core.update_profile();
                       // document.getElementById("voucher_code").innerHTML=code;
                        
                        $("#expiry_date").html(data.description.expiry);$("#voucher").html(code);
                        $("#coupon_details").show(); 
                         $('#confirm_congratulation').modal({
                            show: true
                        });
                    }
                    else if(data.status=='failure' && data.errCode==201){
                        $("#basicModal").modal('toggle');
                      }
                    else if(data.status=="failure"){
                            $('#gift_grab_error').html(data.description)
                            $('#confirm_gift_error').modal({
                            show: true
                            });
                     // $("#transaction_form").append(data.description.form_content)
                    
                      
                     } 
                },
             beforeSend: function(){
                 if(by_voucher==1){
                    $("#orderconfirmation").hide();
                }else{
                    
                }
               $('.loader').show();
               },
           complete: function(){
             $('.loader').hide();
             },
            error: function (xhr,error) {
              
          },  
         
            });
     },update_profile : function(){
          var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/MyLikesCount/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
                if(data.status=='success'){
                     b2c.core.updateprofile(data.description);
                }
            },
            error: function (xhr,error) {

             },  

            });
     },cancel_transaction : function(amount,trans_id){
              var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/cancel_payment/?";
		     $.ajax({
                    url: url,
                    type:"GET",
                    data:{
                         amount : amount,
                         trans_id: trans_id,
                         api_version:'3',
                  
                        },
            timeout: 50000,
            dataType: "jsonp",
            crossDomain: true,
            success:function(data){ 
           },
            beforeSend: function(){
               $('.loader').show();
               },
               complete: function(){
                  $('.loader').hide();
                },
               error: function (xhr,error) {
              
               },  
         
            });
     },edit_user_details : function(){
         var name = $("#name").val();
         var email = $("#email").val();
         var mobile = $("#mobile").val();
         var gender = $('input[name=gender]:checked').val()
         var dob = $("#dob").val();
         
         var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/Update_profile/?";
            $.ajax({
           url: url,
           type:"GET",
           data:{
                name : name,
                date_of_birth: dob,
                email : email,
                mobile : mobile,
                gender : gender,
                fb_data : 2,
                api_version:'3',
                res_format : "jsonp"
               },
            timeout: 50000,
            dataType: "jsonp",
            crossDomain: true,
            success:function(data){ 
                if(data.status=='success'){
                    b2c.core.updateprofile(data.profileData);
                    $("#name").val(data.profileData.name);
                    $("#email").val(data.profileData.email);
                    $("#mobile").val(data.profileData.mobile);
                    $("#gender").val(data.profileData.gender);
                    $("#dob").val(data.profileData.date_of_birth);
                    // give values to label
                     $("#email_label").html(data.profileData.email);
                     $("#mobile_label").html(data.profileData.mobile);
                     $("#gender_label").html(data.profileData.gender);
                     $("#dob_label").html(data.profileData.date_of_birth);
                     $("#name_label").html(data.profileData.name);
                     
                    
                     $('#profile_editsuccess').modal({
                        show: true
                    });
                }
                else if(data.status=='failure' && data.errCode==201){
                    $("#basicModal").modal('toggle');
                  }
                else{
                    $('#profile_editerro').modal({
                        show: true
                    });
                }
           },
            beforeSend: function(){
               $('.loading-image').show();
               },
               complete: function(){
                  $('.loading-image').hide();
                  $(".eprofile").hide();
                  $('.profile').show();
                },
               error: function (xhr,error) {
              
               },  
         
            });
     },
     send_sms : function(){
         var user = $("#user").val();
         var mobile = $("#user_mobile").val();
         var txn_id = $("#txn_id").val();
          var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/Send_coupon_code/?";
            $.ajax({
           url: url,
           type:"GET",
           data:{
                user_id : user,
                mobile: mobile,
                txn_id : txn_id,
                api_version:'3',
                res_format : "jsonp"
               },
            timeout: 50000,
            dataType: "jsonp",
            crossDomain: true,
            success:function(data){ 
                if(data.status=='success'){
                    $('.loader').hide();
                    $('#confirm_SMS_success').modal({
                        show: true
                    });
                   
                  
                }
                else if(data.status=='failure' && data.errCode==201){
                    $("#basicModal").modal('toggle');
                  }
                else{
                    $('#confirm_SMS_error').modal({
                        show: true
                    });
                }
           },
            beforeSend: function(){
               $('.loader').show();
               },
               complete: function(){
                  $('.loader').hide();
                  
                },
               error: function (xhr,error) {
              
               },  
         
            });
     },prepaid_recharge: function(amt,opt,flag,number){
           var amt = $("#"+amt).val();
           var operator = $("#"+opt).val();
           var flag  = $("#"+flag).val();
           var number = $("#"+number).val();
           var paymenturl = host+"/home/success/";
           var provider_name = $("#provider_name").html();
           var type = $("#recharge_type").html();
           var specialrecharge = $("#stv").val();
           $("#orderconfirmation").hide();
            var paymentmode = 'wallet';
              if($("#usewallet").is(":checked")){
                  paymentmode = 'wallet';
              }else{
                  paymentmode = 'online';
              }
              $('.loader').show();
        	  if(flag=="1" || flag=="3"){  
		  var getData=$.ajax({
                    url:proto+b2cUrl+'/index.php/api_new/action/api/true/actiontype/recharge',
                    data:    { 
                            mobile_number:number,
                            operator:operator,
                            flag:'1',
                            amount:amt,
                            recharge:'1',
                            paymentopt:'wallet',
                            payment:'1',
                            stv:specialrecharge,
                            return_url:paymenturl,
                            res_format:'jsonp',
                            partial:'1',
                            api_version:'3',
                            pay_by:'web',
                            request_via:'web',
                                                  },
                    crossDomain: true,
                    dataType:'JSONP',
                   });
        
                getData.done(function(res){
                     if(res.status=="success"){
                        localStorage.setItem("amount",amt);
                        localStorage.setItem("number",number);
                        localStorage.setItem("paymentmode",paymentmode);
                        localStorage.setItem("provider",provider_name);
                        localStorage.setItem("type",type);
                        if(res.description.online_form==false){
                            window.location ="/home/success?status="+res.status+"&txnid="+res.description.transaction_id+"&balance="+res.description.closing_balance
                        }
                       else{
                           $("#transaction_form").append(res.description.form_content)
                           $("#pg_payu_form").submit();
                           $("#transaction_form").html('');
                        }
                              }
                     else if(res.status=="failure") {
                         alert(res.description);
                          window.location="/home/";
                     }
                },function(err){
                    console.log(err);
                });
            }
            else if(flag=="2"){
                
              var getData=$.ajax({
                    url:proto+b2cUrl+'/index.php/api_new/action/api/true/actiontype/recharge',
                    data:    { 
                                subscriber_id:number,
                                operator:operator,
                                flag:flag,
                                amount:amt,
                                recharge:"1",
                                paymentopt:paymentmode,
                                payment : "1",
                                return_url: paymenturl,
                                res_format : "jsonp",
                                partial:'1',
                                api_version:'3',
                                request_via : 'web',
                              },
                    crossDomain: true,
                    dataType:'JSONP',
                   });
        
                getData.done(function(res){
                    $('.loader').hide();
                     if(data.status=="success"){
                        localStorage.setItem("amount",amt);
                        localStorage.setItem("number",number);
                        localStorage.setItem("provider",provider_name);
                        localStorage.setItem("type",type);
                        if(data.description.online_form==false){
                            window.location ="/home/success?status="+data.status+"&txnid="+data.description.transaction_id+"&balance="+data.description.closing_balance
                        }
                        else{
                            $("#transaction_form").append(data.description.form_content)
                            $("#pg_payu_form").submit();
                            $("#transaction_form").html('');
                        }
                    } else if(data.status=="failure") {
                     alert(data.description);
                     window.location="/home/";  
                    }
                   else {
                       alert(data.description);
                      window.location="/home/logout/";
                    }
                },function(err){
                    console.log(err);
                });
                
           
            }
            else if(flag=="4"){
                 var service_charge = $("#service_charge").val();
                var totalamt = $("#total_amount").val();
                var service_charge_percent = $("#service_charge_percent").val();
                
                var getData=$.ajax({
                    url:proto+b2cUrl+'/index.php/api_new/action/api/true/actiontype/recharge',
                    data:    { 
                                mobile_number:number,
                                operator:operator,
                                flag:flag,
                                base_amount:amt,
                                service_charge:service_charge,
                                amount:totalamt,
                                payment_flag:"1",
                                service_tax:service_charge_percent,
                                billpayment : "1",
                                paymentopt:paymentmode,
                                payment:"1",
                                return_url: paymenturl,
                                res_format : "jsonp",
                                partial:'1',
                                api_version:'3',
                                request_via : 'web',
                              },
                    crossDomain: true,
                    dataType:'JSONP',
                   });
        
                getData.done(function(res){
                    $('.loader').hide();
                     if(data.status=="success"){
                        localStorage.setItem("amount",totalamt);
                        localStorage.setItem("number",number);
                        localStorage.setItem("provider",provider_name);
                        localStorage.setItem("type",type);
                        if(data.description.online_form==false){
                         window.location ="/home/success?status="+data.status+"&txnid="+data.description.transaction_id+"&balance="+data.description.closing_balance
                        } 
                        else {
                          $("#transaction_form").append(data.description.form_content)
                          $("#pg_payu_form").submit();
                          $("#transaction_form").html('');
                         } 
                  }
                  else if(data.errorCode=="201") {
                     alert(data.description);
                     window.location="/home/";
                    }
                },function(err){
                    console.log(err);
                });
            }
     }
 }
  function getplanDetails(amt,validity,desc,type){
          b2c.core.getPlandescription(amt,validity,desc,type);
   }
   
   
   function deletequickpay(){
        var id = $("#quickpay_id").val();
        if (id != "") {
            $("#remove-offer-modal").modal('hide');
            var quick_pay_id = id;
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/del_quickpay/?";
            $.ajax({
                url: url,
                type: "GET",                
                data: {
                    id: quick_pay_id,
                    res_format: "jsonp"
                },
                timeout: 50000,
                jsonpCallback: "callback",
                dataType: "jsonp",
                crossDomain: true,
                success: function(data) {
                    if (data.status == "success")
                    {
                        b2c.core.getQuickPaylist('1');
                        $("#quickmsg").html(data.description);
                    }
                    else if(data.status=='failure' && data.errCode==201){
                        $("#basicModal").modal('toggle');
                      }
                    else if (data.status == "failure") {
                        window.location = '/home/logout/'
                    }
                },
                beforeSend: function() {
                    $('.loader').show();
                },
                complete: function() {
                    $('.loader').hide();
                },
                error: function(xhr, error) {
                    console.log(error);
                },
            });
        }
    }

	
   function IsMobileNumber(txtMobile) {
    var mob = /^[789][0-9]{9}$/;
    if (mob.test(txtMobile) == false) {
         $('#mobnumber').val(""); 
         $("#valid_mobile_num").html("<small><strong>Please enter a valid Mobile Number</strong></small>");
//        alert("Please enter valid mobile number.");
        txtMobile.focus();
        return false;
    }
    return true;
   }
   
   
        function forgetPasswordClick(one,two,ev) {
         var nameORnumber = document.getElementById("mob-number").value;
           if(nameORnumber == ''){
             $("#login_mobile_num_error").html("<small>Please enter mobile number to send otp.</small>"); 
             ev.preventDefault();
            }else{
             $('#login_mobile_num_error').html("");    
             document.getElementById(one).style.display = 'block';
             document.getElementById(two).style.display = 'none';
             send_OTP();
            } 
        }
            
        function send_OTP(edit_profile_MobNum){ 
          var nameORnumber = document.getElementById("mob-number").value;
          nameORnumber = (nameORnumber) ? nameORnumber : edit_profile_MobNum;
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/forgotpwd/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{user_name:nameORnumber,
                  vtype:1,
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
                if(data.status=='success'){
                    $("#sent_otp").html("<small> OTP sent !!! </small>");
                }
                else if(data.status=='failure' && data.errCode==201){
                    $("#basicModal").modal('toggle');
                  }
            },
            error: function (xhr,error) {
            },  
         
            });
            
        }
        
        function send_SignupOTP(mobilenum,msg){ 
          var nameORnumber = document.getElementById(mobilenum).value;
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/forgotpwd/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{user_name:nameORnumber,
                  vtype:1,
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
                if(data.status=='success'){
                    $("#"+msg).html("<small> OTP sent !!! </small>");
                }
                else if(data.status=='failure' && data.errCode==201){
                    //$("#basicModal").modal('toggle');
                  }
            },
            error: function (xhr,error) {
            },  
         
            });
            
        }
        
        function signup_password_match() {
          var pass1 = document.getElementById("signup_password").value;
          var pass2 = document.getElementById("signup_confirm_password").value;
          var ok = true;
          if (pass1 != pass2) {
            $('#signup_password').val(""); 
            $('#signup_confirm_password').val("");
            document.getElementById("signup_password").style.borderColor = "#E34234";
            document.getElementById("signup_confirm_password").style.borderColor = "#E34234";
            $("#signupError").html("<small> Password are not matched please enter password again. </small>");     
            ok = false;
          }
         
         return ok;
        }
    
        function password_match() {
          var pass1 = document.getElementById("forget_password").value;
          var pass2 = document.getElementById("confirm_password").value;
          var ok = true;
          if (pass1 != pass2) {
            $('#forget_password').val(""); 
            $('#confirm_password').val("");
            document.getElementById("forget_password").style.borderColor = "#E34234";
            document.getElementById("confirm_password").style.borderColor = "#E34234";
            $("#wrong_password").html("<small> Password are not matched please enter password again. </small>");     
            ok = false;
          }
         else {
           $("#correct_password").html("<small> Password are matched. </small>");
         }
         return ok;
        }
       
        function clearsentOTP(){
            /*for login password update*/
            $('#sent_otp').html('');   
            $('#correct_password').html('');  
            $('#correct_otp').html('');  
            $('#wrong_password').html('');  
            $('#wrong_otp').html('');
            $('#invalid_otp').html('');
            
            /*For profile password update*/
            $('#edit_sent_otp').html('');   
            $('#edit_correct_password').html('');  
            $('#edit_correct_otp').html('');  
            $('#edit_wrong_password').html('');  
            $('#edit_wrong_otp').html('');  
            $('#edit_invalid_otp').html('');
            
            /*For signin modal*/
            $('#valid_mobile_num').html('');
            $('#sign_msg').html('');
            $('#sign_otp_msg').html('');
            $('#sign_otp_sent').html('');
            $('#sign_success').html('');
        }
    

    function checkOTP_Password(ev){
          var nameORnumber = document.getElementById("mob-number").value;
            var pass1 = document.getElementById("forget_password").value;
            var pass2 = document.getElementById("confirm_password").value;
            ev.preventDefault();
            if(password_match()){
             clearsentOTP(); 
             var new_otp = document.getElementById("forget_otp").value;
             var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/update_password/?";
                $.ajax({
                url: url,
                type:"GET",
                data:{mobile_number:nameORnumber,
                      password:pass1,
                      confirm_password:pass2,
                      code:new_otp,
                      current_password:new_otp,
                      vtype:1,
                      api_version:3,
                      api:true,
                      res_format : "jsonp"
                     },
                 timeout: 50000,
                 dataType: "jsonp",
                 jsonpCallback: 'callback',
                 crossDomain: true,
                 success:function(data){
                    if(data.status=='success'){
//                                      console.log(data);   
                                        $("#correct_otp").html("<small> Password updated successfully. </small>");  
                                        localStorage.setItem('passflag',pass1);
                                        b2c.core.setUserData(data);
                    }else if(data.description == "Invalid OTP"){
                //                      console.log(data.description);
                                        $("#invalid_otp").html("<small>Invalid OTP</small>");   
                    }
                    else if(data.status=='failure' && data.errCode==291){
                        var msg = data.description.split("\\n");
                        msg = msg.join("<br>");
                    $("#invalid_otp").html("<small><p style='text-align:left'>"+msg+"</p></small>"); 
                  }
                    else if(data.status=='failure' && data.errCode==201){
                    $("#basicModal").modal('toggle');
                  }
                 },
                 error: function (xhr,error) {
                    console.log(data);
                 },  
                });
            }
        } 
        
        function checksignup_Otp(ev){
          var nameORnumber = document.getElementById("mobnumber").value;
            var pass1 = document.getElementById("signup_password").value;
            var pass2 = document.getElementById("signup_confirm_password").value;
            ev.preventDefault();
            if(signup_password_match()){
             clearsentOTP(); 
             var new_otp = document.getElementById("signup_otp").value;
             var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/update_password/?";
                $.ajax({
                url: url,
                type:"GET",
                data:{mobile_number:nameORnumber,
                      password:pass1,
                      confirm_password:pass2,
                      code:new_otp,
                      current_password:new_otp,
                      vtype:1,
                      api_version:3,
                      api:true,
                      res_format : "jsonp"
                     },
                 timeout: 50000,
                 dataType: "jsonp",
                 jsonpCallback: 'callback',
                 crossDomain: true,
                 success:function(data){
                    if(data.status=='success'){
//                                      console.log(data);   
                                        $("#signupError").html("<small> Password updated successfully. </small>");  
                                        localStorage.setItem('passflag',pass1);
                                        b2c.core.setUserData(data);
                                        $('#sign_up_Modal').modal('hide');
                    }else if(data.description == "Invalid OTP"){
                //                      console.log(data.description);
                                        $("#signupError").html("<small>Invalid OTP</small>");   
                    }
                    else if(data.status=='failure' && data.errCode==291){
                        var msg = data.description.split("\\n");
                        msg = msg.join("<br>");
                    $("#signupError").html("<small><p style='text-align:left'>"+msg+"</p></small>"); 
                  }
                    else if(data.status=='failure' && data.errCode==201){
                    //$("#basicModal").modal('toggle');
                  }
                 },
                 error: function (xhr,error) {
                    console.log(data);
                 },  
                });
            }
        }
        
        function checkProfile_OTP_Password(ev,numberORname){
            var pass1 = document.getElementById("edit_forget_password").value;
            var pass2 = document.getElementById("edit_confirm_password").value;
            ev.preventDefault();
            if(profile_password_match()){
             clearsentOTP(); 
             var new_otp = document.getElementById("edit_forget_otp").value;
             var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/update_password/?";
                $.ajax({
                url: url,
                type:"GET",
                data:{mobile_number:numberORname,
                      password:pass1,
                      confirm_password:pass2,
                      code:new_otp,
                      current_password:new_otp,
                      vtype:1,
                      api_version:3,
                      api:true,
                      res_format : "jsonp"
                     },
                 timeout: 50000,
                 dataType: "jsonp",
                 jsonpCallback: 'callback',
                 crossDomain: true,
                 success:function(data){
                    if(data.status=='success'){
//                                      console.log(data);   
                                        $("#edit_correct_otp").html("<small> Password updated successfully. </small>");  
                                        $('#edit_password_modal').modal('hide');
                                        $('#profile_password_update_success').modal({
                                            show: true
                                        });
                    }else if(data.description == "Invalid OTP"){
//                                      console.log(data.description);
                                        $("#edit_invalid_otp").html("<small>Invalid OTP</small>");   
                    }
                     else if(data.status=='failure' && data.errCode==291){
                        var msg = data.description.split("\\n");
                        msg = msg.join("<br>");
                    $("#edit_invalid_otp").html("<small><p style='text-align:left'>"+msg+"</p></small>"); 
                  }
                  else if(data.status=='failure' && data.errCode==201){
                        $("#basicModal").modal('toggle');
                    }    
                 },
                 error: function (xhr,error) {
                 },  
                });
            }
           
        }
        
        function profile_password_match() {
          var pass1 = document.getElementById("edit_forget_password").value;
          var pass2 = document.getElementById("edit_confirm_password").value;
          var ok = true;
          if (pass1 != pass2) {
            $('#edit_forget_password').val(""); 
            $('#edit_confirm_password').val("");
            document.getElementById("edit_forget_password").style.borderColor = "#E34234";
            document.getElementById("edit_confirm_password").style.borderColor = "#E34234";
            $("#edit_wrong_password").html("<small> Password are not matched please enter password again. </small>");     
            ok = false;
          }
         else {
           $("#edit_correct_password").html("<small> Password are matched. </small>");
         }
         return ok;
        }
        
        function profile_send_OTP(numberORname){ 
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/forgotpwd/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{user_name:numberORname,
                  vtype:1,
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
                if(data.status=='success'){
                    $("#edit_sent_otp").html("<small> OTP sent !!! </small>");
                }
                else if(data.status=='failure' && data.errCode==201){
                    $("#basicModal").modal('toggle');
                  }
            },
            error: function (xhr,error) {
            },  
         
            });
            
        }
    
        months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        function convertDate(date_str) {
        temp_date = date_str.split("-");
        return temp_date[2] + "-" + months[Number(temp_date[1])-1];
        }
       
        function show_service_msg(amt){
                amt = (amt == 0 )? amt : $("#postpaid_amt_textbox").val();
                var charge = $("#charge_slab").val();
                var servicechrg = charge.split(",");
                var mincharge = servicechrg[0].split(":");
                var maxcharge = servicechrg[1].split(":");
                var service_charge_percent = $("#service_charge_percent").val();
                amt = (amt)?amt:0;
                var totalamt = (parseInt(amt) + parseInt(mincharge[1]) + parseInt(service_charge_percent));
                $("#service_charge").val(parseInt((mincharge[1])));
                $("#total_amount").val(totalamt);
                $("#service_msg").show();
                $("#service_msg").html("(" + parseInt(totalamt) + " =" + parseInt(amt) + "+" + $("#service_charge").val() + "    service charge inclusive of all taxes)");
                $("#rec_amount").val(amt);
        }    
