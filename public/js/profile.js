host = window.location.host;

    function getTransactionHistory(count)
    {
        var page = parseInt(count);
        if (page == "0") {
            $("#purchasehistory tbody").html('');
        }
        $("#purchasehistory tfoot").html('');
        var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/purchase_history/?";
        var html = "";
        $.ajax({
            url: url,
            type: "GET",
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            data: {
                page: page,
                res_format: "jsonp"
            },
            crossDomain: true,
            success: function(data) {
                if (data.status == "success" && data.description.length>0){
                    $.each(data.description, function(key, val) {
                        if (val.operator_name == null) {
                            var opt_name = "";
                        }
                        else{
                            var opt_name = val.operator_name;
                         }
                        var transamount = "\'" + val.transaction_amount + "\'";
                        var operatorname = "\'" + val.operator_name + "\'";
                        var status = "\'" + val.status + "\'";
                        var transid = "\'" + val.transaction_id + "\'";
                        var transactionmode = "\'" + val.transaction_mode + "\'";
                        var transdatetime = "\'" + val.trans_datetime + "\'";
                        var number = "\'" + val.number + "\'";
                        var transstatus = (val.status == "2") ? "Successful" : "failed";
                        if (val.service_id == "1") {
                                var rectype = "Prepaid Mobile";
                        }
                         else if (val.service_id == "2") {
                            var rectype = "Postpaid Mobile";
                        }
                        else if (val.service_id == "3") {
                            var rectype = "Dth Recharge";
                         }
                        else if (val.service_id == "4") {
                            var rectype = "Postpaid Recharge";
                        } else {
                                var rectype = "";
                        }
			var transclass = (val.status == "2") ? "greentxt" : "redtxt";
                        var img_src = (val.trans_category === "recharge") ? "/assets/images/icons/recharge_red.png" : "/assets/images/icons/recharpop.png";
                        var offer_name = (val.offer_name != null) ? val.offer_name : val.number;
//                        var deal_name = (val.deal_name != null) ? val.deal_name : val.number;
                        var deal_name = (val.deal_name != null) ? val.deal_name : '';
                        var offer_deal = (val.trans_category == "refill") ? val.number : offer_name+'<br>'+deal_name ;
                        if(val.trans_category == "refill"){
                          var transaction_mode = "Wallet Topup";
                        }else if((val.trans_category == "deal")){
                            transaction_mode = "Deal Purchase"; 
                        }else if((val.trans_category == "recharge")){
                            transaction_mode = "Recharge"; 
                        }else{
                           transaction_mode = "Postpaid Bill"; 
                        }
                        
//                        var transaction_mode = (val.trans_category == "refill") ? "Wallet Topup" : val.transaction_mode ;

                        html += '<tbody>'
                                    +'<tr>'
                                       +'<td><img src='+img_src+' alt="" class="img-circle img-responsive" width="30"></td>'   
                                       +'<td>'+offer_deal+'</td>'
                                       +'<td>'+transaction_mode+'</td>'
                                       +'<td> <i class="fa fa-inr"></i>  '+val.transaction_amount+'</td>'
                                       +'<td class='+transclass+'>'+transstatus+'</td>'

//						    +'<td class="bluetxt">'+val.number+'<br><span class="smtxt">'+rectype+'</span></td>'
//				                 +'<td><img src="/images/rupee_sm.png">'+val.transaction_amount+'</td>'
//			                     +'<td>'+val.trans_datetime+'</td>'
//						         +'<td class='+transclass+'>'+transstatus+'</td>'
                                    +'<tr>'
                                +'</tbody>'
                    });
                    page++;
                    if (data.description.length > 0 && data.description.length >= 5) {
			html+= '<tfoot><tr><td>&nbsp;</td><td>&nbsp;</td><td><span onclick="getTransactionHistory(' + page + ')"  ><b><a>Load more History</a></b></span></td><td>&nbsp;</td><td>&nbsp;</td></tr></tfoot>';
                    }
                    $("#purchasehistory").append(html);
                }
                else if( data.status == "success" && data.description.length===0){
                    var html = "<tbody style='width:100%'><tr><td style='text-align:center'>No Record Found</td></tr></tbody>";
                     $("#purchasehistory").append(html);
                }
                else if (data.status == "failure") {
                    window.location = "/home/logout/"
                }
            },
            beforeSend: function() {
                $('.loader').show();
            },
            complete: function() {
                $('.loader').hide();
            },
            error: function(xhr, error) {
            },
        });

    }
	
    function getWalletHistory(count) {
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/wallet_history/?";
            var page = parseInt(count);
            if (page == "0") {
                $("#wallethistory tbody").html('');
            }
            $("#wallethistory tfoot").html('');
            var html = "";
            $.ajax({
                url: url,
                type: "GET",
                timeout: 50000,
                dataType: "jsonp",
                data: {
                    page: page,
                    res_format: "jsonp"
                },
                crossDomain: true,
                success: function(data) {
                    if (data.status == "success" && data.description.length>0){
                        $.each(data.description, function(key, val) {
                            var transclass = (val.status == "2") ? "greentxt" : "redtxt";
                            var transstatus = (val.status == "2") ? "Successful" : "failed";
                             var offer_name = (val.offer_name != null) ? val.offer_name : val.number;
//                             var deal_name = (val.deal_name != null) ? val.deal_name : val.number;
                             var deal_name = (val.deal_name != null) ? val.deal_name : '';
                             var offer_deal = (val.trans_category == "refill") ? val.number : offer_name+'<br>'+deal_name ;
                             
                            if(val.trans_category == "refill"){
                              var transaction_mode = "Wallet Topup";
                            }else if((val.trans_category == "deal")){
                                transaction_mode = "Deal Purchase"; 
                            }else if((val.trans_category == "recharge")){
                                transaction_mode = "Recharge"; 
                            }else{
                                transaction_mode = "Postpaid Bill";  
                            }
                             
//                             var transaction_mode = (val.trans_category == "refill") ? "Wallet Topup" : val.transaction_mode ;
                            html+='<tbody>'
                                      +'<tr>'
                                        +'<td><img src="/assets/images/icons/wallet.svg" class="img-circle img-responsive" width="30" alt=""></td>'
                                        +'<td>' + offer_deal + '</td>'
                                        +'<td>' + transaction_mode + '</td>'
                                        +'<td class="text-center">' + val.trans_datetime + '</td>'
                                        +'<td> <i class="fa fa-inr"></i>  '+val.transaction_amount+'</td>'
                                        +'<td class="green text-center" style="width:120px;">Closing Balance <br> Rs.'+val.closing_bal+'</td>'
                                      +'</tr>'
                                    +'</tbody>'
                        });
                        page++;
                        if (data.description.length > 0 && data.description.length >= 5) {
                          html += '<tfoot><tr><td>&nbsp;</td><td>&nbsp;</td><td><span onclick="getWalletHistory(' + page + ')"><b><a>Load more History</a></b></span></td><td>&nbsp;</td><td>&nbsp;</td></tr></tfoot>';
                        }
                         $("#wallethistory").append(html);
                      }
                       else if( data.status == "success" && data.description.length===0){
                            var html = "<tbody style='width:100%'><tr><td style='text-align:center'>No Record Found</td></tr></tbody>";
                             $("#wallethistory").append(html);
                        }
                      else if (data.status == "failure") {
                      window.location = "/home/logout/"
                     }
                },
                beforeSend: function() {
                    $('.loader').show();
                },
                complete: function() {
                    $('.loader').hide();
                },
                error: function(xhr, error) {

                },
            });
        }
        function getGiftcoinHistory(){
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/LoyaltyPointsHistory/?";
            var html = "";
            $.ajax({
                url: url,
                type: "GET",
                timeout: 50000,
                dataType: "jsonp",
                data: {
                    res_format: "jsonp"
                },
                crossDomain: true,
                success: function(data) {
                    if (data.status == "success" && data.description.length>0){
                        $.each(data.description, function(key, val) {
                            html+='<tbody>'
                                    +'<tr>'
                                if((val.redeem_status == 3) || (val.redeem_status == 5)){
                                    html+='<td><img src="/assets/images/icons/gift_coin_plus.svg"></td><td>'+ val.coins +' Coins <br>Added</td>'
                                } else  if((val.redeem_status == 4) || (val.redeem_status == 1)){
                                    html+='<td><img src="/assets/images/icons/gift_coin_minus.svg"></td><td>'+ val.coins +' Coins <br>'
                                           if(val.redeem_status == 4){
                                        html+='Reversed'
                                            }else{
                                        html+='Redeemed'
                                            }
                                        html+='</td>'
                                }
                                if(val.trans_category === 'deal'){
                                    html+="<td> Grabbed gift '" + val.Name +"'</td>"
                                } else if(val.trans_category === 'recharge'){
                                    if(val.redeem_status == 3){
                                      html+='<td> Mobile recharge of Rs.' + val.transaction_amount +'<br>'+'('+ val.Name +')'+'</td>'
                                    } else  if(val.redeem_status == 4){
                                      html+='<td>'+ val.msg +'Rs. '+ val.transaction_amount +'<br>'+'('+ val.Name +')'+'</td>'
                                    }
                                }else if(val.trans_category === 'billing'){
                                    if(val.redeem_status == 3){
                                      html+='<td> Bill payment of Rs.' + val.transaction_amount +'<br>'+'('+ val.Name +')'+'</td>'
                                    } else  if(val.redeem_status == 4){
                                      html+='<td>'+ val.msg +'Rs. '+ val.transaction_amount +'<br>'+'('+ val.Name +')'+'</td>'
                                    }
                                }else if(val.trans_category === 'pay1shop'){
                                    if(val.redeem_status == 3){
                                      html+='<td> Pay1shop of Rs.' + val.transaction_amount +'<br>'+'('+ val.Name +')'+'</td>'
                                    } else  if(val.redeem_status == 4){
                                      html+='<td>'+ val.msg +'Rs. '+ val.transaction_amount +'<br>'+'('+ val.Name +')'+'</td>'
                                    }
                                }else if(val.trans_category === 'signup'){
                                      html+='<td>'+ val.msg +'Rs. '+ val.transaction_amount +'</td>'
                                }    
                                    html+='<td class="text-center">'+ val.trans_datetime +'</td>'
                                    html+='<td style="width:110px; ">Transaction ID<br>' + val.trans_id + '</td>'
                                    html+='</tr>'
                            html+='</tbody>'
                        });
                        $("#gifthistory").html(html);
                      }
                      else if( data.status == "success" && data.description.length===0){
                            var html = "<tbody style='width:100%'><tr><td style='text-align:center'>No Record Found</td></tr></tbody>";
                             $("#gifthistory").append(html);
                        }
                        else if (data.status == "failure") {
                      window.location = "/home/logout/"
                     }
                },
                beforeSend: function() {
                    $('.loader').show();
                },
                complete: function() {
                    $('.loader').hide();
                },
                error: function(xhr, error) {

                },
            });


        }
            
        function getfaq(type){
        var type = type;
       // alert(type);
        var serviceurl = b2cUrl+"/index.php/api_new/action/api/true/actiontype/get_faq/?";
        var html = "";

        $.ajax({
            url: serviceurl,
            type: "POST",
            dataType: "jsonp",
            data: {
                req: type,
                res_format: "jsonp"
            },
            success: function(data) {
                if (data.status == "success"){
                    $("#" + type).append(data.description[0]);
                }
                else if (data.status == "failure") {
                    window.location = '/home/logout/'
                }
            },
            beforeSend: function() {
                $('.loader').show();
            },
            complete: function() {
                $('.loader').hide();
            },
            error: function(xhr, error) {
                console.log(xhr);
                console.log(error);
            }
        });


    }
    function updateprofile()
    {
        var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/update_profile/?";
        var name = $("#name").val();
        var dob = $("#datepicker").val();
        var gender = $('input:radio[name=options]:checked').val();
        dob = dob.split("/");
        dob = dob[0]+"-"+dob[1]+"-"+dob[2];
        var mobno = $("#mnumber").val();
        var accpin = $("#pin").val();
        var email = $("#email_id").val();
        var sessionemail = $("#profile_email").val();
        if (sessionemail != email) {
              enterpassword();
        }
        else {
            var data = {
                name: name,
                gender: gender,
                date_of_birth: dob,
                password: accpin,
                email: sessionemail,
                res_format: "jsonp"
                };
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success: function(data) {
               
                if (data.status == "success") {
                    $("#msg").html(data.description);
                    $("#update_msg").html('');
                    b2c.core.updateprofile(data.profileData);
                    $("#basicModal2").modal('hide');
                }
                else {
                   $("#msg").html('');
                   $("#errormsg").html(data.description);
                   $("#basicModal2").modal('hide');
                   alert(data.description);
                }

                return false;


            },
            beforeSend: function() {
                $('.loader').show();
            },
            complete: function() {
                // Code to hide spinner.
                $('.loader').hide();
            },
            error: function(xhr, error) {
            },
        });
        }
    }
    
    function enterpassword(){
        $("#basicModal2").modal('toggle');
    }
    
    function forgotpassword(mobno){
          
          var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/forgotpwd/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{
                  user_name:mobno,
                  vtype:"1",
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
               if(data.status=="success"){
               localStorage.setItem("mob_number",mobno);
			   $("#change_mob").hide();
			   $("#change_pin").show();
               }
              else {
                    //$("#update_msg").html(data.description);
                   }
            },
            error: function (xhr,error) {
             },  
         
            });
			
		
    }
    
    function updatepassword(){
        var mobileno = localStorage.getItem("mob_number");
        var otp = $("#new_otp").val();
        var newpin = $("#new_pin").val();
        var confirmpin = $("#confirm_pin").val();
        if (otp == ''){
            alert("Please enter a OTP.");
            return false;
        }
        else if (newpin == ''){
            alert("Please enter new pin");
            return false;
        }
        else if (confirmpin == ''){
            alert("please enter confirm pin");
            return false;
        }
        else if (newpin != confirmpin){
        
            alert("Password does not match");
            return false;
        }

        else {
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/update_password/?";
            $.ajax({
                url: url,
                type: "GET",
                timeout: 50000,
                dataType: "jsonp",
                jsonpCallback: 'callback',
                crossDomain: true,
                data:{mobile_number:mobileno,
                    password:newpin,
                    confirm_password:confirmpin,
                    code:otp,
                    vtype:"1",
                    res_format : "jsonp"
                 },
                success: function(data) {
                    if (data.status == "success"){
                    
                        $("#update_msg").html("Password Reset Successfully!!");
                        $("#pin").val(newpin);
                    } else {
                        
                        $("#update_msg").html(data.description);
                    }

                }
            });
        }
		
    }
    
    function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
        
    } else { 
          alert("Geolocation is not supported by this browser.");
    }
     }

function showPosition(position) {
       var latitude = position.coords.latitude;
       var longitude = position.coords.longitude;
       localStorage.setItem("latitude",latitude);
       localStorage.setItem("longitude",longitude);
       
  }
      