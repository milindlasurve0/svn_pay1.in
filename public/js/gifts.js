var domain = b2cUrl;
var param = "?1=1";
$(document).ready(function() {

    var domain = window.domain;
    var param = window.param;
    var display_param = null;
    var string_param = window.location.href.split('/')[5];
    if (typeof (string_param) != "undefined" && string_param !== null) {
        display_param = true;
    }
    if ($(".homeList").length) {
        ajax_url = domain + "/index.php/api_new/action/api/true/actiontype/GetAllGifts_WEB/" + param;        
        display_store(ajax_url, display_param);
    }
    if ($(".storeCategory").length) {
        var cat_handler = category_list_handler(string_param);
        if (!cat_handler) {
            param += "&type=category";
            ajax_url = domain + "/index.php/api_new/action/api/true/actiontype/GetAllGifts_WEB/" + param;
        } else {
            ajax_url = domain + cat_handler.url;
        }        
        display_store(ajax_url, display_param);
    }else if ($(".showCategory").length) {
        var cat_handler = category_list_handler("cat-"+string_param);
        ajax_url = domain + cat_handler.url;        
        display_store(ajax_url, display_param);
    }
});

var fill_Category = function(categorydata) {
    var categoryHTML = "";
    $.each(categorydata, function(index, element) {
        if ((element.img_url).length < 1) {
            element.img_url = "http://placehold.it/247x225";
        }
        categoryHTML += '<li><a href="categories/' + (element.name).toLowerCase() + '/' + element.id + '"><img  class="sliderimages"  src="' + element.img_url + '" alt="Image"><p class="text-center">' + element.name + '</p></a></li>';
    });
    $("#categoryList .bxslidercat").html(categoryHTML);
    attach_Slider('bxslidercat', 3, 3);
}

var display_Giftstore = function(giftstoredata) {
    var gift_type_HTML = "";

    $.each(giftstoredata, function(index, element) {
        gift_type_HTML += '<hr/>';
        gift_type_HTML += '<div class="row top30" >';
        gift_type_HTML += '<div class="row" >';
        gift_type_HTML += '<div class="col-lg-12 text-center" ><h1>' + element.name + ' </h1></div >';
        var page = (element.name).split(" ");
        gift_type_HTML += '<div class="col-lg-10 col-lg-offset-1" >';
        gift_type_HTML += '<div align="right"><a href="/store/' + element.url+ '/'+element.id+'">See All <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></div>';
        gift_type_HTML += '<ul class="bxslider" >';
        $.each(element.details, function(inner_index, inner_element) {
            if ((inner_element.img_url).length < 1) {
                inner_element.img_url = "http://placehold.it/300x150";
            }
            gift_type_HTML += '<li><a href="/gifts/detail/'+inner_element.offer_id+'" ><img class="sliderimages"  height="150px" width="300px" src="' + inner_element.L_img_url + '" alt="Image" style="max-width:100%;" > <p class="text-center" >' + inner_element.offer + '</p></a></li>';
        });
        gift_type_HTML += '</ul>';
        gift_type_HTML += '</div>';
        gift_type_HTML += '</div>';
        gift_type_HTML += '</div>';
    });
    $("#gift_types").html(gift_type_HTML);
}

var display_store_page = function(categorydata) {
    $.each(categorydata, function(index, element) {
        if (typeof (element.category_details) != "undefined" && element.category_details !== null) {
            $.each(element.category_details, function(inner_index, inner_element) {
              //  console.log(inner_element);
            });
        }
    });
}

var display_store_category = function(categorydata) {
    //console.log(categorydata);
    //console.log(" in type");
    
    menuObj = {};
    var category_data = "";
    category_data += '<div class="row">';
    category_data += '<div class="col-lg-12 col-md-12 col-sm-12">';
    $.each(categorydata, function(index, element) {
        if ((element.img_url).length < 1 || (element.img_url).length < 4) {
            element.img_url = "http://placehold.it/250x150";
        }
        menuObj[element.id] = element.name;
        category_data += '<div class="col-md-4">';
        category_data += '    <a href="categories/' + (element.name).toLowerCase() + '/' + element.id + '" class="thumbnail" id="' + element.id + '" )">';
        category_data += '       <img  title="' + element.name + '" width="250px" class="img-responsive" src="' + element.img_url + '" alt="">';
        category_data += '    <div id="description" class="categTitle">' + element.name + '</div>';
        category_data += '    </a>';
        category_data += '</div>';
        
        $('ul#overlayUL').append("<li><a href='categories/"+ (element.name).toLowerCase() + "/" + element.id +" '>"+element.name+"</a></li>");
    });
    category_data += '</div>';
    category_data += '</div>';
   // console.log(menuObj);
    $.jStorage.set('pay1_type_list', menuObj);
    $("#gift_types").html(category_data);
}

var category_list_handler = function(cat_type_value) {
    console.log('cat_type_value--');
    console.log(cat_type_value);
    if ( typeof(cat_type_value) != "undefined" &&   cat_type_value !== null) {
        var cat_type = cat_type_value.split('-')[1];
    } else {
        return false;
    }

    if (cat_type == "undefined" || cat_type == null) {
        return false;
    }
    var url = "/index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?type=category&type_id=" + cat_type;
    var catObj = {
        url: url,
    }
    return catObj;
}

var show_cat_list = function(obj) {
    domain = window.domain;
    catobj = category_list_handler(obj.id);
    display_store(domain + catobj.url, true);
}


var display_store = function(ajax_url, display_list) {
    console.log(display_list);
    console.log(" ajax url : " + ajax_url);
    
    if ($("#gift_types").length) {
       // console.log("in gift");
        $.ajax({
            url: ajax_url + '&res_format=jsonp&callback=',
            method: 'GET',
            dataType: 'jsonp',
            success: function(data) {
                //console.log(" in success ");
                //console.log(data);
                if (typeof (data.description.category) != "undefined" && data.description.category !== null) {
                    if ($("#categoryList .bxslidercat").length) {
                        fill_Category(data.description.category);
                    }
                    if ($(".storeCategory").length) {
                        getMenuList('Categories');
                        if (display_list != "undefined" && display_list !== null) {
                            display_offer_list(data);
                        } else {
                            display_store_category(data.description.category);
                        }
                    }
                }
                if (typeof (data.description.data) != "undefined" && data.description.data !== null) {
                   // console.log("  in home 22 ");
                    if ($("#gift_types").length) {
                        console.log("  in home 33 ");
                        if ($("#gift_types").hasClass("homeList")) {
                           // console.log("  in home  ");
                            display_Giftstore(data.description.data);
                            attach_Slider_with_caption('bxslider', 5, 5);
                        }
                    }
                }
                if (display_list != "undefined" && display_list !== null) {
                    if ($(".showCategory").length) {
                        display_offer_list(data.description);
                    }
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    }
}

var display_offer_list = function(offerList) {
    //console.log(" in offer list ");
   // console.log(offerList);
   
    var offerlistHTML = "";
    $.each(offerList, function(index, element) {
  
        console.log('---Display ---')
        console.log(element);
        console.log("name : " + element.deal);
        console.log("Image : " + element.img_url);
        
        offerlistHTML += '<div class="col-lg-3 col-sm-3  text-center"  data-gift-points="'+element.min_amount+'" >';
        offerlistHTML += '<div class="thumbnail tileheight">';
        if (typeof (element.mylikes) != "undefined" && element.mylikes !== null) {
            if (element.mylikes == 1) {
                offerlistHTML += '<div class="addtofavdiv"><img data-swap="/images/addtofavorities.png" src="/images/addtofavorities_red.png" value="' + element.mylikes + '" style="cursor: pointer"/></div>';
            } else {
                offerlistHTML += '<div class="addtofavdiv"><img src="/images/addtofavorities.png" data-swap="/images/addtofavorities_red.png" value="' + element.mylikes + '" style="cursor: pointer"/></div>';
            }
        } else {
            offerlistHTML += '<div class="addtofavdiv"><img src="/images/addtofavorities.png" data-swap="/images/addtofavorities_red.png" value="0" style="cursor: pointer"/></div>';
        }
        if ((element.img_url) == null || (element.img_url).length < 1) {
            element.img_url = "http://placehold.it/150x100";
        }
        offerlistHTML += '<a class="">';
        offerlistHTML += '<img alt="" src="' + element.img_url + '"  height="100px" width="150px">';
        offerlistHTML += '</a>';
        offerlistHTML += '<div class="card_infolabel pull-right showpoints">';
        offerlistHTML += '<img src="/images/gift_ic.png"  />';
        offerlistHTML += '<span class="card_infolabel">' + element.min_amount + '</span>';
        offerlistHTML += '</div>';
        offerlistHTML += '<div class="caption prod-caption top15">';
        offerlistHTML += '<h5><a href="/gifts/detail/'+element.id+'">' + element.offer + '</a></h5>';
        offerlistHTML += '<span class="listingspan">' + element.deal + '</span>';
        offerlistHTML += '</div>';
        offerlistHTML += '</div>';
        offerlistHTML += '</div>';
    });
    $("#gift_types").html(offerlistHTML);
}

//var addTofav = function(obj) {
//    var flag = $("#isloggedin").val();
//   // alert(flag);
//    if(flag !=1){
//        $("#basicModal").modal('toggle');
//        
//    } else {
//       //alert("hello");
//    var _this = $(obj);
//    var current = _this.attr("src");
//    var swap = _this.attr("data-swap");
//    _this.attr('src', swap).attr("data-swap", current);
//    if (_this.attr('value') == 0) {
//        _this.attr('value', 1);
//    } else {
//        _this.attr('value', 0);
//    }
//    }
//
//}

var getMenuList = function(higlighted){
    console.log("I am in GetMenuList");
    var domain = window.domain;
    var ajax_url = domain + "/index.php/api_new/action/api/true/actiontype/GetGiftInfo_WEB/?type=";
    $.ajax({            
            url: ajax_url + '&res_format=jsonp&callback=',
            method: 'GET',
            dataType: 'jsonp',
            success: function(new_data) {
                var vmenu = "";
                var overlaymenu = "";
                $.each(new_data.description.gift, function(index, element) {
                    vmenu += '<a   class="list-group-item list-group-item" href="/store/'+element.url+'/'+element.id+'"><b>'+element.name+'Store===</b></a>';
                    overlaymenu += '<li><a href="/store/'+element.url+'/'+element.id+'">'+element.name+'</a></li>';
                });
                
                
                vmenu += '<a  class="list-group-item list-group-item" href="/store/nearyou"><b>Near You</b></a>';
         
         if(higlighted=="Near You"){
                overlaymenu += '<li><a href="/store/nearyou" class="addclassunderline">Near You</a></li>';
            }
            else{
                      overlaymenu += '<li><a href="/store/nearyou">Near You</a></li>';
            }    
                vmenu += '<a   class="list-group-item list-group-item" href="#" onclick="javascript:showOverlay();"><b>Categories</b><i class="glyphicon glyphicon-chevron-down arrowDown"></i></a>';
            
             if(higlighted=="Categories"){
                overlaymenu += '<li><a href="/categories" class="addclassunderline">Categories</a><ul id="overlayUL"></ul>';
            }else{
                    overlaymenu += '<li><a href="/categories">Categories</a><ul id="overlayUL"></ul>';
            }  
                $(".verticaldropdown").html(vmenu);
                
                $(".overlaymenu").html(overlaymenu);
                
                var overlay_cat ="";
                
                $.each(new_data.description.categories, function(inner_index, inner_element) {
                    overlay_cat += "<li><a href='/categories/"+ (inner_element.name).toLowerCase() + "/" + inner_element.id +" '>"+inner_element.name+"</a></li>";
                });                
                
                $('ul#overlayUL').append(overlay_cat);
            },
            error: function(err) {
                console.log(err);
            }
        });
}



