	$(document).ready(function () {

		    $('#myform input').on('blur', function () {
		        if ($("#myform").valid()) {
		            $('#cpayment1').prop('disabled', false);
		        } else {
		            $('#cpayment1').prop('disabled', 'disabled');
		            $('#cpayment1').addClass('disabled'); 
		        }
		    });

		    $("#myform").validate({
		        rules: {
		            cardtype: {
		                required: true,

		            },
		            cardno: {
		                required: true,
		                creditcard: true
		            },
		            month:{
		            	required:true
		            },
		            year:{
		            	required:true
		            },
		            cvv:{
		            	required:true,
		            	digits: true
		            }
		    

		        }
		    });


  			$('#test input').on('blur', function () {
		        if ($("#test").valid()) {
		            $('#cpayment2').prop('disabled', false);
		        } else {
		            $('#cpayment2').prop('disabled', 'disabled');
		            $('#cpayment2').addClass('disabled'); 
		        }
		    });

		     $("#test").validate({
		        rules: {
		            cardtype: {
		                required: true,
		               
		            },
		            debit: {
		                required: true,
		                creditcard: true
		            },
		            month:{
		            	required:true
		            },
		            year:{
		            	required:true
		            },
		            cvv:{
		            	required:true,
		            	digits: true
		            }
		           
		        }
		    });
		  

		$('#test1 input').on('blur', function () {
		        if ($("#test1").valid()) {
		            $('#cpayment3').prop('disabled', false);
		        } else {
		            $('#cpayment3').prop('disabled', 'disabled');
		            $('#cpayment3').addClass('disabled'); 
		        }
		    });

		     $("#test1").validate({
		        rules: {
		        	
		            bank: {
		                required: true
		            },
		             other: {
		                required: true
		            }		  
		                    
		        }
		    });


			$('#test2 input').on('blur', function () {
		        if ($("#test2").valid()) {
		            $('#cpayment4').prop('disabled', false);
		        } else {
		            $('#cpayment4').prop('disabled', 'disabled');
		            $('#cpayment4').addClass('disabled'); 
		        }
		    });

		     $("#test2").validate({
		        rules: {
		            first: {
		                required: true
		               
		            },
		            second: {
		                required: true
		            }		           
		        }
		    });



		});

