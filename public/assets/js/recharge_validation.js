  $(document).ready(function () {

        $('#prepaid input').on('blur', function () {
            if ($("#prepaid").valid()) {
                $('#prepaid_btn').prop('disabled', false);
            } else {
                $('#prepaid_btn').prop('disabled', 'disabled');
            }
        });

        $("#prepaid").validate({
            rules: {
                phone: {
                    required: true,
                    minlength: 8,
                    digits:true

                },
                sim:{
                  required:true
                },
            
                text:{
                  required:true
                }
            }
        });




         $('#postpaid input').on('blur', function () {
            if ($("#postpaid").valid()) {
                $('#postpaid_btn').prop('disabled', false);
            } else {
                $('#postpaid_btn').prop('disabled', 'disabled');
            }
        });

        $("#postpaid").validate({
            rules: {
                phone: {
                    required: true,
                    minlength: 8,
                    digits:true

                },
                sim:{
                  required:true
                },
            
                text:{
                  required:true
                }
            }
        });

         $('#dth input').on('blur', function () {
            if ($("#dth").valid()) {
                $('#dth_btn').prop('disabled', false);
            } else {
                $('#dth_btn').prop('disabled', 'disabled');
            }
        });

        $("#dth").validate({
            rules: {
                phone: {
                    required: true,
                    minlength: 8,
                    digits:true

                },
                sim:{
                  required:true
                },
            
                text:{
                  required:true
                }
            }
        });
         $('#datacard input').on('blur', function () {
            if ($("#datacard").valid()) {
                $('#datacard_btn').prop('disabled', false);
            } else {
                $('#datacard_btn').prop('disabled', 'disabled');
            }
        });

        $("#datacard").validate({
            rules: {
                phone: {
                    required: true,
                    minlength: 8,
                    digits:true

                },
                sim:{
                  required:true
                },
            
                text:{
                  required:true
                }
            }
        });





         $('#wallet input').on('blur', function () {
            if ($("#wallet").valid()) {
                $('#wallet_btn').prop('disabled', false);
            } else {
                $('#wallet_btn').prop('disabled', 'disabled');
            }
        });

        $("#wallet").validate({
            rules: {
                amount: {
                    required: true
                  
                }
            }
        });
        

        $('#redeem1 input').on('blur', function () {
            if ($("#redeem1").valid()) {
                $('#redeem_btn').prop('disabled', false);
            } else {
                $('#redeem_btn').prop('disabled', 'disabled');
            }
        });

        $("#redeem1").validate({
            rules: {
                voucher_code: {
                    required: true
                  
                }
            }
        });


});