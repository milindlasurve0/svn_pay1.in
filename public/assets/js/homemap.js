function initialize() {
  var mapOptions = {
    zoom: 4,
	  
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    draggable: false,
	scrollwheel: false,
    center: new google.maps.LatLng(40.9, 77.2)
  }
  
    
  var map = new google.maps.Map(document.getElementById('map-canvas'),
                                mapOptions);

  var image = 'assets/images/map/1.png';
  var myLatLng = new google.maps.LatLng(28.890542, 40.274856);
  var beachMarker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: image
  });
   var image1 = 'assets/images/map/2.png';
  var myLatLng = new google.maps.LatLng(23.890542, 75.274856);
  var beachMarker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: image1
  });
  var image2 = 'assets/images/map/3.png';
  var myLatLng = new google.maps.LatLng(26.890542, 108.274856);
  var beachMarker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: image2
  });
  var image3 = 'assets/images/map/0.png';
        var myLatLng = new google.maps.LatLng(19.1836574,72.831691);
        var beachMarker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: image3
   });     
}

google.maps.event.addDomListener(window, 'load', initialize);