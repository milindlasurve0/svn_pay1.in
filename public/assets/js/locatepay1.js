        var geocoder;   
        var default_rad = 6;
        var zoom_flag = true;
        var old_zoom = 13;
        var dist;
        var NE_lat;
        var NE_lng;
        var map_lat ;
        var map_long ;
        var center_lat;
        var center_lng;
        var old_center_lat;
        var old_center_lng;
        var serviceurl;
        var drag_map;
        var map;
        var marker;
        var markers = [];
        var drag_zoom_count = 0 ;
        var init_lat;
        var init_lng;
        var current_center_lat ;
        var current_center_lng ;
        var infowindow;
        
        $('a[href="#add_money"]').on('shown.bs.tab', function (e) {
           showMap();
        });
        
        
        function showMap(){
            map_lat = localStorage.getItem('latitude');
            map_long = localStorage.getItem('longitude');
            init_lat = map_lat;
            init_lng = map_long;
            if(map_lat==null && map_long == null ){
                getLocation();
            } else {
            var map_lat = localStorage.getItem('latitude');
            var map_long = localStorage.getItem('longitude');
            }
                var myOptions = {
                    zoom: old_zoom,
                    center: new google.maps.LatLng(map_lat,map_long),
                    panControl:false,
                    mapTypeControl: false,
                    streetViewControl: false,
                    navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                };
                map = new google.maps.Map($('#map-canvas1')[0], myOptions);

                //always keep blue dot marker at user's current geographical location. 
                var latlng = new google.maps.LatLng(init_lat,init_lng);
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon:'/assets/images/map/0.png',
                });
                urlCall(map_lat,map_long,default_rad,map);

            
                center_lat_lng  = map.getCenter();
//            var center_lat_lng_arr = Object.keys(center_lat_lng).map(function(k) { return center_lat_lng[k] });
//                center_lat = center_lat_lng_arr[0];
//                center_lng = center_lat_lng_arr[1];

                var res = String(center_lat_lng);
                var array = res.split(",");
                center_lat = array[0].replace(/["'()]/g,"");
                center_lng = array[1].replace(/["'()]/g,"");

                old_center_lat = center_lat;
                old_center_lng = center_lng;
                
            //map dragend event func+ "<br/> (Contact no. +91-"+ v.t.mobile + ")"+ tion.    
            google.maps.event.addListener(map, 'dragend', function() {
                    // 3 seconds after the center of the map has changed, pan back to the marker.
            window.setTimeout(function() {
                u = "K";
                center_lat_lng  = map.getCenter();
//            var center_lat_lng_arr = Object.keys(center_lat_lng).map(function(k) { return center_lat_lng[k] });

            var res = String(center_lat_lng);
            var array = res.split(",");
            center_lat = array[0].replace(/["'()]/g,"");
            center_lng = array[1].replace(/["'()]/g,""); 

//                center_lat = center_lat_lng_arr[0];
//                center_lng = center_lat_lng_arr[1];

                NE_lat_lng  = map.getBounds().getNorthEast();
//            var NE_lat_lng_arr = Object.keys(NE_lat_lng).map(function(k) { return NE_lat_lng[k] });

            var res = String(NE_lat_lng);
            var array = res.split(",");
            NE_lat = array[0].replace(/["'()]/g,"");
            NE_lng = array[1].replace(/["'()]/g,""); 
            
//                NE_lat     = NE_lat_lng_arr[0];
//                NE_lng     = NE_lat_lng_arr[1];
                
                C2N_diff = LatLongdistance(center_lat,center_lng,NE_lat,NE_lng,u);
                C2C_diff = LatLongdistance(center_lat,center_lng,old_center_lat,old_center_lng,u);
                old_center_lat = center_lat;
                old_center_lng = center_lng;
                
                if((C2C_diff>(C2N_diff/2))&&(C2N_diff>0)){
                    map_lat = center_lat;
                    map_long = center_lng;
                    dist  = Math.ceil(C2N_diff);
                    drag_zoom_count++;
                urlCall(map_lat,map_long,dist,map);    
                }
                  }, 3000);
            });
            
            //map zoom_changed event function.    
            google.maps.event.addListener(map, 'zoom_changed', function() {
                    var zoomLevel = map.getZoom();
                    if((Math.abs(zoomLevel - old_zoom))>=1 && zoom_flag){
                        
                    center_lat_lng  = map.getCenter();
//                var center_lat_lng_arr = Object.keys(center_lat_lng).map(function(k) { return center_lat_lng[k] });

                var res = String(center_lat_lng);
                var array = res.split(",");
                center_lat = array[0].replace(/["'()]/g,"");
                center_lng = array[1].replace(/["'()]/g,"");  

//                    center_lat = center_lat_lng_arr[0];
//                    center_lng = center_lat_lng_arr[1];
                        
                    NE_lat_lng  = map.getBounds().getNorthEast();
//                var NE_lat_lng_arr = Object.keys(NE_lat_lng).map(function(k) { return NE_lat_lng[k] });

                var res = String(NE_lat_lng);
                var array = res.split(",");
                NE_lat = array[0].replace(/["'()]/g,"");
                NE_lng = array[1].replace(/["'()]/g,"");  

//                    NE_lat     = NE_lat_lng_arr[0];
//                    NE_lng     = NE_lat_lng_arr[1];    
                        
                        C2N_diff = LatLongdistance(center_lat,center_lng,NE_lat,NE_lng,"K"); 
                        map_lat = center_lat;
                        map_long = center_lng;
                        dist = Math.ceil(C2N_diff);
                        drag_zoom_count++;
                        urlCall(map_lat,map_long,default_rad,map);    
                    }
                    zoom_flag = true;
            });    
            
            //map center_changed event function.    
            google.maps.event.addListener(map, 'center_changed', function(event){
               window.setTimeout(function() { 
                  geocoder = new google.maps.Geocoder();
                  
                current_center_lat_lng  = map.getCenter();
//            var current_center_lat_lng_arr = Object.keys(current_center_lat_lng).map(function(k) { return current_center_lat_lng[k] });
//                current_center_lat = current_center_lat_lng_arr[0];
//                current_center_lng = current_center_lat_lng_arr[1];
                
                var res = String(current_center_lat_lng);
                var array = res.split(",");
                current_center_lat = array[0].replace(/["'()]/g,"");
                current_center_lng = array[1].replace(/["'()]/g,""); 
                
                   }, 3000);
            });
            
              //getting lat long on map click
//              google.maps.event.addListener(map, 'click', function(event){
//                 var drag_map = event.latLng;
//                 document.getElementById('lat').value = drag_map.lat().toFixed(6);
//                 document.getElementById('lng').value = drag_map.lng().toFixed(6);
//              });    
                
        }
        
        //calling getNearRetailer Api with user's current geographical location's latitude and longitude   
          urlCall.api_limit = 0; 
        function urlCall(map_lat,map_long,dist,map){
          urlCall.api_limit++;
          if(urlCall.api_limit >= '10'){
            deleteMarkers();  
            $('#locatepay1_maplimit_exceed').modal({
                        show: true
            });
            return;
          }
            var token = $("#token").val();
             infowindow = new google.maps.InfoWindow();
            serviceurl = "https://cashpg.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?method=getNearByRetailer&lat="+map_lat+"&lng="+map_long+"&distance="+dist+"&tokenvalue="+token+"&check=false";
            $.ajax({
                url: serviceurl,
                type: "POST",
                dataType: "jsonp",
                jsonp: 'root',
                success: function (data) {
                    console.log(data);
                    $("p").empty();
                    if(data[0]['errorcode'] == 'E030'){
                     alert(data[0]['msg']);
                      $('.loader').show(); 
                      $("p").empty();
                      $('#TokenExpire').html("Your token had been expired.<br>Loading......  ");
                     window.location.reload();  
                    }else if(typeof data[0][0] === 'undefined' || data[0][0] === null){
                     $("p").empty();
                     $('#NoRetailer').html("Sorry !! No Retailer found at this area.");
                    }
                    $('.loader').hide();
                    $.each(data, function (key, value) {
                        $.each(value, function (k, v) { 
                            var latlng = new google.maps.LatLng(v.t.latitude, v.t.longitude);
                            marker = new google.maps.Marker({
                              position: latlng,
                              map: map,
                              icon:'/assets/images/map/Map_pin.png',
                              title: v.t.address
                            });
                            if(v.t.shopname == null){
                                v.t.shopname = "";
                            }
                            google.maps.event.addListener(marker, 'click', function (){
                                infowindow.close();
                                infowindow.setContent(v.t.shopname + "<br/>" + v.t.address + "<br/>" + v.t.area_name + "<br/>" + v.t.city_name);
                                infowindow.open(map, this);
                            });
                     });
                    });
                },
                beforeSend: function () {
                    $('.loader').show();
                },
                complete: function () {
                    $('.loader').hide();
                },
                error: function (xhr, error) {
                    console.log(xhr);
                    console.log(error);
                 }
            });
       }
       //calcultaing distance in KM btw two latitude and longitude 
        function LatLongdistance(lat1, lon1, lat2, lon2,unit) {
            //delete previous markers after 2 times drag_zoom_count
            if(drag_zoom_count >= 2){
             deleteMarkers();    
             drag_zoom_count = 0 ;   
            }
            var radlat1 = Math.PI * lat1/180;
            var radlat2 = Math.PI * lat2/180;
            var radlon1 = Math.PI * lon1/180;
            var radlon2 = Math.PI * lon2/180;
            var theta = lon1-lon2;
            var radtheta = Math.PI * theta/180;
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            dist = Math.acos(dist);
            dist = dist * 180/Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit=="K") { dist = dist * 1.609344 }
            if (unit=="N") { dist = dist * 0.8684 }
            return dist.toFixed(2);
        }                                                                           
       
        // Sets the map on all markers in the array.
        function setAllMap(map) {
          for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
          }
        }
        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
          setAllMap(null);
        }
        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
          clearMarkers();
          markers = [];
        }
        