<?php
function getUserDetails()
{
   $CI = & get_instance();
   $data=$CI->session->userdata('user');
   return $data;
   
}

function getPayuDetails(){
    $domain_url = "http://b2c.pay1.loc/";
   $payu_config =  array(
                    'KEY'=> 'gtKFFx', //WhmK3z
                    'SALT'=> 'eCwWELxi', //JkFnrJ9P
                    'SUBMIT_URL'=>'https://test.payu.in/_payment',
                    'B2C_SUBMIT_URL'=>$domain_url."index.php/panel/payu",
                    'SUCCESS_URL'=>$domain_url.'index.php/api_new/action/api/true/actiontype/payu_update/page/success',
                    'FAILURE_URL'=>$domain_url.'index.php/api_new/action/api/true/actiontype/payu_update/page/failure',
                    'TIMEOUT_URL'=>$domain_url.'index.php/api_new/action/api/true/actiontype/payu_update/page/timeout',
                    'CANCEL_URL'=>$domain_url.'index.php/api_new/action/api/true/actiontype/payu_update/page/cancel',
                    'SERVICE_URL'=>'https://test.payu.in/merchant/postservice.php',
            );
    return $payu_config;
}
 

    function getImageDetails(){
        // Get a reference to the controller object
        $CI = get_instance();

        // You may need to load the model if it hasn't been pre-loaded
        $CI->load->model('Home_Gift_Image');

        // Call a function of the model
        $result = $CI->Home_Gift_Image->getImageDetails();

        return $result;
    }
    
    function getMetaTagDetails(){
        $CI = get_instance();
        $CI->load->model('Home_Gift_Image');
        $result = $CI->Home_Gift_Image->getMetaTagDetails();
        return $result;
    }
    
    function getAllJobDetails(){
        $CI = get_instance();
        $CI->load->model('Home_Gift_Image');
        $result = $CI->Home_Gift_Image->getAllJobDetails();
        return $result;
    }
    
    