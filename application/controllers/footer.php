<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Footer extends CI_Controller
{
    public function index()
    {
        $this->load->view('about_us');
    }
    
    public function about_us()
    {  
        $this->load->view('footer/about_us');
    }
    public function privacy_policy()
    {  
        $this->load->view('footer/privacy_policy');
    }
    public function sitemap()
    {  
        $data['sidebarCategories']=parent::setSideBarCategories();
        $this->load->view('footer/sitemap',$data);
    }
    
}
