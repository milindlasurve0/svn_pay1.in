<?php

class Categories extends CI_Controller
{
    
    public function __construct() 
     {
        
        parent::__construct();
        $this->load->library('curl'); 
    }
    
    public function index($category="",$cat_id="")
    {

        $data['CategoryName'] = $category;
        $data['cat_type'] = $category;
        if($category == ""){
            $data['cat_type'] = "all";
        }
            $this->load->view('categories/index',$data);
        
    }
    
    public function showCategory($cat_name,$cat_id){
        
        $data['CategoryName'] = $cat_name;
        $data['CategoryId'] = $cat_id;
        
        /*
         * Get sidebar categories from cookie is found or else set them from parent controller 
         */
        $data['sidebarCategories']=json_decode(!empty($_COOKIE['sidebarCategories'])?$_COOKIE['sidebarCategories']:parent::setSideBarCategories());
        
   
        $this->load->view('categories/show_category',$data);
        
    }
    
    
}