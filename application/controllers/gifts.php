<?php

class Gifts extends CI_Controller
{
    public function detail($id)
    {
      
        $data = array();
        
        if(!empty($id))
         {

            $detail_url = CDEV_URL."/index.php/api_new/action/api/true/actiontype/GetGiftDetails_WEB/?id=".$id;
            
            $response = $this->curl->get($detail_url);

            $data['response'] = json_decode($response,true);
        
        }
        


        $this->load->view('gifts/index',$data);
        
    }
}
