<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Payment extends CI_Controller
{
    
    public function __construct() 
     {
        
        parent::__construct();
        $this->load->library('curl'); 
        $this->load->model('payment_model');
    }
    
    public function index()
    { $this->load->library('curl'); 
        $data = $_REQUEST;
        
        /*$data['key'] = "gtKFFx" ;
        $data['txnid'] = "100018350" ;
        $data['amount'] = "53" ;
        $data['productinfo'] = "recharge" ;
        $data['firstname'] = "swapnilT" ;
        $data['email'] = "umeshrathod01@gmail.com" ;
        $data['phone'] = "9975629244" ;
        $data['surl'] = "http://b2c.pay1.loc/index.php/api_new/action/api/true/actiontype/payu_update/page/success" ;
        $data['furl'] = "http://b2c.pay1.loc/index.php/api_new/action/api/true/actiontype/payu_update/page/failure" ;
        $data['curl'] = "http://b2c.pay1.loc/index.php/api_new/action/api/true/actiontype/payu_update/page/cancel" ;
        $data['touturl'] = "http://b2c.pay1.loc/index.php/api_new/action/api/true/actiontype/payu_update/page/timeout" ;
        $data['hash'] = "ab9a62676cb21f67de6a5b38511c1dc360f9fd87a40aa2c10dd189a520b7b225fcf8a9d156d8f56e1aa1f4b47c40a3c51ef41eae0277c722a1c8920d76b3e266" ;
        $data['user_credentials'] = "gtKFFx:99" ;
        $data['user_sess_id'] = "6a6a4ggdjqe5jlg3brgi0vofb3" ;
        $data['user_sess_val'] = "PHPSESSID" ;
        $data['user_credentials'] = 'gtKFFx:99';*/
        
        if(isset($data['api'])) unset($data['api']);
        if(isset($data['actiontype'])) unset($data['actiontype']);
        if(isset($data['request_via'])) unset($data['request_via']);        
       
       
        $passData = array('var1'=>$data['user_credentials'],'command'=>'get_user_cards');
         $payu_detail = getPayuDetails($passData);
        $service_api_data = $data;
        $hash = hash("sha512", $payu_detail['KEY'] . "|".$passData['command']."|" . $passData['var1'] . "|" . $payu_detail['SALT']);
        $service_api_data = array_merge($passData, array('hash' => $hash,'key' => $payu_detail['KEY']));
        $data['UserCardDetails']= unserialize($this->curl->post($payu_detail['SERVICE_URL'], $service_api_data));
         $data['B2C_SUBMIT_URL'] = 'https://test.payu.in/_payment'; 
         
         $data['banklist'] = $this->payment_model->get_banklist();  
         
        $this->load->view('payment/index',$data);
        
    }
}