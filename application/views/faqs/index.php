<?php $this->load->view('common/doctype_html');  

    $current_page_url =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $meteTagData = getMetaTagDetails();
    foreach ($meteTagData as $row){
      
      if($row->page_url == $current_page_url){
        $new_page_tittle =  $row->page_tittle;
        $new_page_meta_description =  $row->page_meta_description;  
        $new_page_meta_tag =  $row->page_meta_tag;  
      }
      
    }

?>
<meta name="description" content="<?php if($new_page_meta_description){ echo $new_page_meta_description; }else{ ?>These FAQs are designed to provide a better understanding of PAY1. For more information about PAY1 or if you need support please contact us. <?php } ?> ">
<meta name="keywords" content="<?php if($new_page_meta_tag){ echo $new_page_meta_tag; }else{ ?>FAQs , Pay1  <?php } ?>" />
<title><?php if($new_page_tittle){ echo $new_page_tittle; }else{ ?>FAQs | Pay1 <?php } ?> </title>
<?php $this->load->view('common/header');  ?>
   <link rel="stylesheet" href="/assets/css/style.css">
   
   
      <div class="clearfix" style="clear:both;"></div>
      <!-- ======================== Header Image ========================== -->
      <input type="hidden" id="faqs_img_hdn" value="/assets/images/background/FAQs.jpg">
      <div class="career_page1 margin-top-50">
         <img src="/assets/images/default_img/pay1_loader.jpg" class="img-responsive" alt="" id="faqs_img">
      </div>
      <div class="container-fluid">
         <div class="container border-both">
            <div class="row">
               <div class="col-md-3 col-sm-3 ">
                  <div class=''>
                     <div id="contact_navigation">
                        <ul class="tabs">
                           <li><a href="/about-us">About us</a></li>
                           <li><a href="/contact-us">Contact Us</a></li>
                           <li><a href="/terms-and-conditions">Terms & Conditions</a></li>
                           <!--<li><a href="#security" data-toggle="tab">Security Policy</a></li>-->
                           <li class="active"><a href="#faqs" data-toggle="tab">FAQs</a></li>
                            <li><a href="/privacy-policy">Privacy Policy</a></li>
                           <li><a href="/careers">Careers</a></li>
                           <li><a target="_blank" href="/partners-blog/" >Blog</a></li>
                           <li><a href="/sitemap">Sitemap</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-sm-9">
                  <div class='padding-both margin-top-30'>
                                  
                  </div>

               <div class="row">
                  <div id="accordion" class="panel-group">       
                    <div class="panel panel-default">
                      <div class="panel-heading active">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                          <h4 class="panel-title">General & Account Related Queries: <b class="opened"></b>
                          </h4>
                        </a>
                      </div>
                      <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">
                          <p class="title">

                           <p class="faghead">What is Pay1? Why should I use it?</p>
                              <p>Pay1 is an online wallet from where you can recharge your prepaid, postpaid, DTH & Data card. It's one stop shop for almost all the prepaid needs. Pay1 makes your experience easier and better. You don't have to worry for low balance - it's just matter of few clicks now!</p>

                                <p class="faghead">Wallet? How does it work?</p>
                                <p>Pay1 wallet is a secure online wallet where you can store your money for making any transaction on Pay1.in. Pay1 wallet balance is the amount in Indian rupees that can be increased by making a payment via different payment options available.</p>
                        
                                 <p class="faghead">How can I make a Pay1 account?</p>
                                 <p>You just have to add your email Id and create a password. You also need to add your mobile number which will be the primary number for your Pay1 account. You will receive all the information regarding transaction on this unique number.</p>
                        
                                 <p class="faghead">How to add money to the wallet?</p>
                                  <p>It's simple! You just need to login to the Pay1 account and click on 'Add Money'. You will be redirected to the 'Payment Options' page. Pay1 offers payment by credit card, debit card, net banking, and even cash pay.</p>

                                  <p class="faghead">How to get a cash topup done?</p>
                                 <p>It's simple again! Just visit your nearest Pay1 retailer, pay him cash and get your wallet loaded with money. You can locate a nearby Pay1 retailer in the application by clicking on the cash top-up icon. Make sure your GPS is ON.</p>
                        
                                  <p class="faghead">Is there any registration fee?</p>
                                  <p>It's absolutely free! You don't need to spend your hard earned money to become a Pay1 user.</p>
                        
                                  <p class="faghead">Is my account information safe?</p>
                                  <p>Don't worry! All information transmitted between you and Pay1 is done through SSL, a secure and safety protocol available on the web. Pay1 will never release any of your bank details to anyone and we only use your email address for all your records.</p>
                           
                           <p class="faghead">How quickly does the top up get to the phone?</p>
                           <p>It's just a matter of few seconds! That's why it is the "A-One" way to mobile freedom.</p>
                           
                           <p class="faghead">What is debit to wallet?</p>
                           <p>Whenever you use your wallet money for recharges, pay bills or buy offers, it is termed as debit to wallet. The amount is deducted from your wallet for respective recharge value.</p>
                           
                           <p class="faghead">I want to create a new password! How can I do that?</p>
                           <p>Just Login to Pay1.in. Next, you will see "View Profile" on the top right. Just click on it and enter your new password using an OTP you receive.</p>
                           
                           <p class="faghead">Forgot my password! What next?</p>
                           <p>Don't worry! We have got your back. You have to click on "forgot password" link. Enter your email Id and a password recovery link mail will be send to you. Further, click on the link provided to you and you will be redirected to create a new password.</p>
                           
                           <p class="faghead">I want to change the registered email id! Is it possible?</p>
                        <p>Yes, you can go to your profile and make necessary changes.</p>
                        
                           <p class="faghead">How can I change my primary number?</p>
                        <p>Sorry, we are afraid we have no such facility at the moment on Pay1.</p>
                        
                           <p class="faghead">Cancellation</p>
                           <p>Once you use your Pay1 balance to purchase a prepaid product, it cannot be reverted back. Pay1 does not take any responsibility for failure of product delivery at the operator's end.</p>
                        
                           <p class="faghead">Terms and conditions</p>
                           <p>Always make sure you read terms and conditions, be it purchasing a voucher or grabbing a gift. </p>
                        
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                          <h4 class="panel-title">Recharge/Bill Payment Related Queries <b class="closed"></b>
                          </h4>
                        </a>
                      </div>
                      <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                         <p class="faghead">Recharge unsuccessful! Where is my money?</p>
                              <p>Here are some possible reasons: </p>
                              <p>1- You have just added money to the wallet and have not processed a recharge transaction.</p>
                              <p>2- There was a delay from the operator's end.</p>
                              <p>3- Probably, it could be a denomination issue.</p>
                              <p>4- Connectivity issue due to which the recharge was stalled.</p>
                              <p>5- Payment to wallet is not successful and there is no money in wallet</p>
                              <p>No problem! We are here for you. The money will be refunded back to your wallet if the recharge was unsuccessful.</p>
                        
                                <p class="faghead">My wallet has money! Now, how can I make recharge or pay bill?</p>
                                <p>You are just a one step back! In your Pay1 account, you can see different sections. Now, click on "Mobile Recharge" or other service you want to choose. Enter your details and click on 'Recharge' to proceed. It's done!</p>
                                  
                                 <p class="faghead">My friend needs a recharge! Can I use my wallet for other prepaid mobile numbers?</p>
                                 <p>Go on to help your friend! You need to add the mobile number to your Pay1 account. Wallet money can be used for your loved ones too.</p>
  
                                 <p class="faghead">How can I do a recharge without an internet access?</p>
                                  <p>Pay1 takes care of all your needs. You can avail Missed Call Recharge service. Just make sure that you have sufficient amount in your Pay1 wallet.</p>
  
                                  <p class="faghead">I do not have access to computer all the time! Is there an alternative?</p>
                                 <p>Of course yes! You can download Pay1 App for your mobile phone. It's more fun when you can make a recharge without worrying about the availability of computer.</p>
                        
                                  <p class="faghead">How long will it take for the bill payment confirmation?</p>
                                  <p>Generally, it takes two - three business days for postpaid bill confirmation. In case, you do not get confirmation after 48 hours, contact the operator or send us the details.</p>

                                  <p class="faghead">While doing a recharge on pay1 got an error. Why?</p>
                                  <p>You can face errors if:</p>
									 <p>1- Entered a wrong mobile number</p>
									 <p>2- Entered wrong operator or circle</p>
									 <p>3- Trying to recharge with an invalid amount</p>
									 <p>4- The service is not available on Pay1</p>
									 <p>5- Wrongly entered your bill details</p>
									In such a scenario, we suggest you to double check the details you have entered.</p>
                           </p>

                           <p class="faghead">I have not received the desired plan!</p>
                           <p>We always try to provide details of the plan available on Pay1. However, you should check with the mobile operator for the exact details. It so happens that sometimes you receive zero talk time. We have no control over it as the mobile operator defines amount credited in the recharge. You must check if special recharge like data usage, SMS pack, or rate cutter plan is activated.</p>
                        
                           <p class="faghead">I mistakenly recharged on wrong number! Can I get my money back?</p>
                           <p>That's sad! The money can't be refunded back to you if the operator has already successfully processed the recharge. However, if you are fortunate, there may be a possibility that recharge got failed. Just give a missed call to pay1 customer care, so that we can help you out!</p>
                        
                           <p class="faghead">I got a successful message from you. But I still have not received the recharge!</p>
                           <p>In case you have not received any message from operator; first check your current balance. Still if you don’t get any benefit, call up the operator and provide them the details sent to you through mail.</p>

                           <p class="faghead">What is a missed call recharge?</p>
                           <p>Well it is an amazing feature and something which you have probably not heard earlier. By Activating the Missed-Call Service for A Mobile No., You can facilitate the Self-Recharge Service to that User. You do not need to visit any store whether it is an online or offline store. Just by giving a Missed-Call to a number, you can recharge your phone.</p>

                           <p class="faghead">Who pays for the missed call recharge?</p>
                           <p>Well, such transactions would consider deductions from the primary user’s wallet.</p>
                        
                           <p class="faghead">What is quick pay for?</p>
                           <p>All the Transactions you make related to recharge & Bill Payment gets saved in the Quick Pay Log. It helps you to process the same transaction next time with just a click.</p>
                        
                                  
                        </div>
                      </div>
                    </div>


                  <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                          <h4 class="panel-title">Payment & Wallet Related Queries <b class="closed"></b>
                          </h4>
                        </a>
                      </div>
                      <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p class="faghead">What is credit to wallet?</p>
                        <p>It simply means adding money to the wallet. Whenever you add money the term is known as "credit to wallet" and you will receive a confirmation message for successful payment transaction.</p>

                        <p class="faghead">I did not receive the cashback</p>
                        <p>Cash back will be credited to your Pay1 wallet within 24 hours of making a successful transaction.</p>

                         <p class="faghead">My money has been debited from bank account! Where has it gone?</p>
                        <p>Check your wallet balance! If the wallet balance is not increased that means your payment transaction was not successful in real time. Sometimes, due to technical reasons, we may not receive a response from the bank for your payment request. In such case, we assume it as a payment attempt. It is possible that the money has been debited from your bank and not added to the wallet.</p>
                        <p>Here are some reasons which can be responsible for the error you are facing while trying to make a payment to wallet:</p>
                        <p>1. The connection with your Internet Service provider is terminating every time you are trying to make the payment.</p>
                        <p>2. The payment gateway is unable to receive approval from your Bank for your payment.</p>
                        <p>3. Your bank account does not have sufficient balance.</p>
                        <p>4. Your bank account has not been activated for online payments.</p>
                        <p>5. There was an issue of connectivity between Bank and the payment gateway.</p>
                        <p>Don't worry! You just have to give a missed call to know what exactly happened.</p>

                         <p class="faghead">Is it necessary to add money to the wallet?</p>
                        <p>Well, we do have an option of direct recharge through your debit card and credit card but we always suggest to keep your wallet loaded to experience a faster transaction.</p>
                        
                         <p class="faghead">How can I check my wallet balance?</p>
                        <p>You can find your wallet balance at the top right corner of the home screen.</p>
                        
                         <p class="faghead">Is it secure to make a payment on pay1.in?</p>
                        <p>It's absolutely secure! Payu Money is the payment gateway partners. All the transactions are executed safely and no personal information regarding your payment is ever accessible by any employee or third party.</p>
                        
                         <p class="faghead">I do not have a debit or credit card! How can I make a payment?</p>
                        <p>You can make a payment through net banking. You just have to activate the Net banking services with the issuing bank. Alternatively, you may also use Cash Pay by procuring a topup from your nearby Pay1 retailer.</p> 
                        <p class="faghead">I can't see my bank in the net banking list. How can I add money to the wallet?</p>
                        <p>In case, you are not able to find your bank in the net banking list. Please browse the other payment options available:</p>
                        <p>- Credit cards (MasterCard or Visa cards)</p>
                        <p>- Debit cards (MasterCard or Visa cards)</p>
                        <p>- ATM-cum-Debit cards (Maestro cards)</p>
                        <p>We will surely add more banks to ease your payment process.</p>
                                  
                        <p class="faghead">I want wallet balance connected to my bank?</p>
                        <p>That’s sad, we are afraid we have no such facility at the moment on Pay1.</p>

                        <p class="faghead">Some error prompted while I was a making a payment to the wallet.</p>
                        <p>Due to technical issues, sometimes you may see an error. There is no need to worry about it. If your amount has been deducted from the bank, all you have to do is give a missed call to our support number.</p>
                        <p class="faghead">I was redirected to a page! What is a payment gateway?</p>
                        <p>It's completely safe. Payment gateway is an online payment service for e-commerce portals to provide you the internet payment options.</p>

                        <p class="faghead">Is there forfeiture of my wallet balance?</p>
                        <p>Your money is safe with us! However, make sure you use wallet balance at least once in six months to avoid such scenario. Grab the maximum benefit out of your wallet!</p>
                        </div>
                      </div>
                    </div>


   <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                          <h4 class="panel-title">Deal And Voucher Related Queries <b class="closed"></b>
                          </h4>
                        </a>
                      </div>
                      <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">
                        <p class="faghead">How do I reedem a deal I have bought?</p>
                        <p>Go ahead! A description about redeeming your deal is mentioned in the Validity & Recommendations section. You need to take a prior appointment if advised or else show the voucher print or Voucher on mobile as stated to the merchant before redeeming it.</p>

                        <p class="faghead">What is a voucher code?</p>
                        <p>A voucher code is a unique no. given to you for the respective deal you buy. You need to show/provide the same voucher code to the merchant while redeeming the deal.</p>

                         <p class="faghead">How soon can I redeem a deal?</p>
                        <p>Your voucher gets activated the moment you buy a deal. You can redeem a deal until it expires, stated in the validity & recommendation section.</p>

                         <p class="faghead">Is it necessary to take prior appointment?</p>
                        <p>We want to make things hassle free for you. Hence, Prior appointment is necessary unless otherwise stated.</p>

                         <p class="faghead">While booking/redeeming a deal, do I need to mention pay1 to the merchant?</p>
                        <p>Yes, the first thing you need to mention, you are a PAY1 customer. That makes things easy for the merchant.</p>
                        
                        <p class="faghead">How about redeeming multiple deals?</p>
                        <p>No. If the fine print says, you can only redeem one. If you buy more than one then you have to give them away as gifts to other people. In such case a printout is mandatory to show while redeeming a deal.</p>

                         <p class="faghead">What if I get late for an appointment?</p>
                        <p>In most cases the bookings are allocated the amount of time required for the treatment and will usually have appointments back to back so they are likely to be unable to accommodate you if you arrive late. In most cases a minimum of 24 hours-notice is required to amend/cancel a booking. Failing to do so may cause you to forfeit the full or partial value of your voucher.</p>

                         <p class="faghead">I want to purchase an e-voucher!</p>
                        <p>Go ahead! Click on "Offers" on the home page and select the offer you want to purchase. You would be asked for the required “Amount & Coins” For making a payment, select the payment source (wallet/Netbanking) and then click on "proceed to payment".</p>

                         <p class="faghead">I have an e-voucher, however the product I want to buy is worth less than the value of the voucher. What happens to the balance money?</p>
                        <p>That’s a serious matter, it depends on voucher to voucher as some brands may allow a balance value credited to their respective wallet. However, we suggest you to utilize the voucher in a complete manner to avoid such scenarios. Read all the terms and conditions before grabbing an E-voucher.</p>
                        
                         <p class="faghead">I want to cancel/return an unused e-voucher?</p>
                        <p>Sorry, voucher once grabbed cannot be returned or cancelled. We suggest you utilize it while making a purchase from the respective brands.</p>
                        
                        <p class="faghead">My unused e-voucher is valid for how many months?</p>
                        <p>You would receive a text message when you grab an E-voucher stating the expiry date and other information.</p>
                  </div>
                      </div>
                    </div>
                  



                  </div>
               </div>
 				  <div class="margin-top-30 clearfix"></div>
               </div>


            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="modal fade" role="dialog" id="failure">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="cls" data-dismiss="modal">&times;</button>
                  <h3>Payment Failure</h3>
               </div>
               <div class="modal-body">
                  <p>Sorry! Your previous payment attempt failed.</p>
                  <div class="pull-right">
                     <button class="modal_button" id="try-again">Try Again</button>
                     <button class="modal_button"id="cancel-button"data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>

      <script src="assets/js/jquery-scrollToTop.js"></script>
      <script type="text/javascript">
         $(document).ready(function($) {
             $('body').scrollToTop({skin: 'cycle'});
             $("#faqs_img").attr("src",$("#faqs_img_hdn").val());
         });
     
        $('#accordion .panel-heading').click(function () {
          if (!$(this).hasClass('active')){
            // the element clicked was not active, but now is - 
            $('.panel-heading').removeClass('active');
            $(this).addClass('active'); 
            setIconOpened(this);
          }else{    
        // active panel was reclicked
          if ($(this).find('b').hasClass('opened')){
           setIconOpened(null);
          }else{
          setIconOpened(this);
           }
         }
        });

        // create a function to set the open icon for the given panel
        // clearing out all the rest (activePanel CAN be null if nothing is open)
        function setIconOpened(activePanel) {
          $('.panel-heading').find('b').addClass('closed').removeClass('opened'); 
          if (activePanel){
            $(activePanel).find('b').addClass('opened').removeClass('closed'); 
          }
        }
      </script>
   <?php echo $this->load->view('common/footer'); ?>
