      <link rel="shortcut icon" href="../../../public/assets/images/img/pay1_favic.png">
      <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="/assets/css/homepage.css">
<!--    <link rel="stylesheet" href="/assets/css/style.css">-->
      <link rel="stylesheet" href="/assets/css/navbar.css">
      <link rel="stylesheet" href="/assets/css/footer.css">
      <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
      <link rel="stylesheet" href="/assets/css/scrollToTop.css">
      <link rel="stylesheet" href="/assets/css/easing.css">
      <link rel="stylesheet" href="/assets/css/normalize.css">
      <link rel="stylesheet" href="/assets/css/media_query.css">
      <link rel="stylesheet" href="/assets/css/payment.css">
      <link rel="stylesheet" href="/assets/validation/validationEngine.jquery.css">
<!--      <link rel="stylesheet" href="/assets/css/profile.css">-->
<!--      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">-->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
<!--        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>-->
      
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

        
         <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
         <script type="text/javascript" src="<?php echo base_url('js/common.js'); ?>"></script>
         <script type="text/javascript" src="<?php echo base_url('js/jquery.cookie.js'); ?>"></script>

         <script type="text/javascript" src="<?php echo base_url('js/bootstrap-validator.js'); ?>"></script>
         <script type="text/javascript" src="<?php echo base_url('js/bootstrap-datepicker.js'); ?>"></script>
         <script type="text/javascript" src="<?php echo base_url('js/jquery.bxslider.js'); ?>"></script>
         <script type="text/javascript" src="<?php echo base_url('js/bootstrap-slider.js'); ?>"></script>
         <script type="text/javascript" src="<?php echo base_url('js/recharge.js'); ?>"></script>
         <!-- Files containing custom Javascript code  -->
         <!-- Start -->
         
     </head>
    <body >
           <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-54897637-1', 'auto');
            ga('send', 'pageview');

          </script>
 
    <?php $this->load->view('common/headermenu');  ?>
        
    <?php $this->load->view('common/rechargemenu');  ?>