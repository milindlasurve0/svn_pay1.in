<?php

$userData = getUserDetails();

$isLoggedIn = $this->session->userdata('isLoggedIn');
$data = getOperatorDetails();
$operatorData = json_decode($data, true);

$parameters = $this->uri->uri_to_assoc();

if(is_array($operatorData)){
foreach ($operatorData as $operatorkey => $operatorval) {
	if (is_array($operatorval)) {
		foreach ($operatorval as $operatorkey => $operatorvalue) {
			if ($operatorkey == "mobile") {
				$arrayval["Mobile"] = $operatorvalue;
			} elseif ($operatorkey == "data") {
				$arrayval["data"] = $operatorvalue;
			} elseif ($operatorkey == "dth") {
				$arrayval["dth"] = $operatorvalue;
			} elseif ($operatorkey == "postpaid") {
				$arrayval["postpaid"] = $operatorvalue;
			}
		}
	}
}
}
$operatorarray = array();
$datalimit = array();
$array = array();
if(isset($arrayval["data"]))
foreach ($arrayval["data"] as $key => $val) {
	$operatorarray[$val["opr_code"]] = $val["id"];
	$datalimit[$val["opr_code"]] = array($val["max"], $val["min"], $val["stv"], $val["charges_slab"], $val["product_id"]);
	$array[strtolower(str_replace(" ","-",$val["name"]))] = $val["opr_code"] ;
}

$data = json_encode($operatorarray);
$datalimit = json_encode($datalimit);

$postpaidoperatorarray = array();
$postpaidlimit = array();
if(isset($arrayval["postpaid"]))
foreach ($arrayval["postpaid"] as $key => $val) {
	$postpaidoperatorarray[$val["opr_code"]] = $val["id"];
	$postpaidlimit[$val["opr_code"]] = array($val["max"], $val["min"], $val["stv"], $val["charges_slab"], $val["service_tax_percent"], $val["product_id"]);
	$array[strtolower(str_replace(" ","-",$val["name"]))] = $val["opr_code"] ;
}

$postpaiddata = json_encode($postpaidoperatorarray);
$postpaidlimit = json_encode($postpaidlimit);

$prepaidoperatorarray = array();
$prepaidlimit = array();
if(isset($arrayval["Mobile"]))
foreach ($arrayval["Mobile"] as $key => $val) {
	$prepaidoperatorarray[$val["opr_code"]] = $val["id"];
	$prepaidlimit[$val["opr_code"]] = array($val["max"], $val["min"], $val["stv"], $val["charges_slab"], $val["product_id"]);
	$array[strtolower(str_replace(" ","-",$val["name"]))] = $val["opr_code"] ;
}

$prepaiddata = json_encode($prepaidoperatorarray);
$prepaidlimit = json_encode($prepaidlimit);

$dthoperatorarray = array();
$dthlimit = array();
if(isset($arrayval["dth"]))
foreach ($arrayval["dth"] as $key => $val) {
	$dthoperatorarray[$val["opr_code"]] = $val["id"];
	$dthlimit[$val["opr_code"]] = array($val["max"], $val["min"], $val["stv"], $val["charges_slab"], $val["product_id"]);
	$array[strtolower(str_replace(" ","-",$val["name"]))] = $val["opr_code"] ;
}

$array = json_encode($array);

$dthdata = json_encode($dthoperatorarray);
$dthlimit = json_encode($dthlimit);
$getcircle = json_decode(CIRCLE, TRUE);
//echo "<pre>";
//print_r($arrayval);
//echo "</pre>";
 
?>
<?php if(isset($parameters['status']) && ($parameters['status']== 'success')){ ?>
<div class="modal fade" id="ordersuccess"  tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    		<div class="modal-dialog">
      		  <div class="modal-content">
          		  <div class="modal-header order_bottomgreenline">
 					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           		    <h4 class="modal-title" id="myModalLabel">Success</h4>
           			</div>
            <div class="modal-body">
				 
				<h4 class="modal-title login_font_title" id="myModalLabel" style="color:green;">Transactions Completed Successfully!!</h4>
            <div class="row">
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12">Mobile Number</td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8">Amount</td>
						<td class="order_font col-sm-6 col-md-6 col-xs-12">Wallet Balance</td>
					</tr>
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12" id="trans_mobile"></td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8" id="transaction_amount"></td>
						<td class="order_font col-sm-6 col-md-6 col-xs-12" id="trans_balance"></td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12">Service Provider</td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8" colspan="2">Transaction Id</td>
						
					</tr>
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12" id="service_provider"></td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8" colspan="2" id="trans_id"></td>
						
					</tr>
				</table>
             </div>
        
				<!--row ends-->
              <div class="modal-header order_bottomgreenline">
             <h4>If Your Transaction does not succeed</h4>         
             </div>
               <div class="row pay_via">
             	<div class="col-md-6 col-sm-6 col-xs-12">                             
                <div class="row">                
                <div class="col-md-4 col-sm-4 col-xs-12">
                <img src="/images/offer_callic.png" height="68" width="68">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">         
                <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">Call Us</div>
                <div class="col-md-12 col-sm-12 col-xs-12"><h4 class="h4bold" id="balance"><?php echo ($userData["walletbal"]>0)?$userData["walletbal"]:0 ?></h4></div>                                          
                </div>
                </div>
                 
                </div>
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">

                <div class="col-md-4 col-sm-4 col-xs-12"><img src="/images/ic1.png" height="68" width="68"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">Ask Us <br/> <b>listen@pay1.in</b></div>              
                </div>
                 
                </div>
<!--				   <div class="col-md-6 col-sm-6 col-xs-12">
					   <div class="row">
						   <div class="col-md-6 col-sm-6 col-xs-12"><button class="btn btn-lg btn-primary btn-block" type="button">check out our deals</button></div>              
					   </div>
                 
                </div>-->
             </div>
             </div>
              </div>
			
			</div>
		    </div>
<?php } else if(isset($parameters['status']) && ($parameters['status']== 'error')){ ?>

<div class="modal fade" id="ordererror" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    		<div class="modal-dialog">
      		  <div class="modal-content">
          		  <div class="modal-header order_bottomgreenline">
 					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel" style="color:red;">Failed!!!!</h4>
           			</div>
            <div class="modal-body">
				 
				<h4 class="modal-title login_font_title" id="myModalLabel" style="color:red;">Transactions failed!!</h4>
            <div class="row">
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12">Mobile Number</td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8">Amount</td>
						<td class="order_font col-sm-6 col-md-6 col-xs-12">Wallet Balance</td>
					</tr>
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12" id="trans_mobile"></td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8" id="transaction_amount"></td>
						<td class="order_font col-sm-6 col-md-6 col-xs-12" id="trans_balance"></td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12">Service Provider</td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8" colspan="2">Transaction Id</td>
						
					</tr>
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12" id="service_provider"></td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8" colspan="2" id="trans_id"></td>
						
					</tr>
				</table>
             </div>
        
				<!--row ends-->
              <div class="modal-header order_bottomgreenline">
             <h4>If Your Transaction does not succeed</h4>         
             </div>
               <div class="row pay_via">
             	<div class="col-md-6 col-sm-6 col-xs-12">                             
                <div class="row">                
                <div class="col-md-4 col-sm-4 col-xs-12">
                <img src="/images/offer_callic.png" height="68" width="68">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">         
                <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">Call Us</div>
                <div class="col-md-12 col-sm-12 col-xs-12"><h4 class="h4bold" id="balance"><?php echo ($userData["walletbal"]>0)?$userData["walletbal"]:0 ?></h4></div>                                          
                </div>
                </div>
                 
                </div>
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">

                <div class="col-md-4 col-sm-4 col-xs-12"><img src="/images/ic1.png" height="68" width="68"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">Ask Us <br/> <b>listen@pay1.in</b></div>              
                </div>
                 
                </div>

             </div>
             </div>
              </div>
			
			</div>
		    </div>
<?php } ?>

				
<div id="transaction_form">
	
</div>
 <input type='hidden' id='user' value='<?php echo $userData['user_id']; ?>'>
 <!--- ********************************** Login with pay1 model *************************** -->
 
 <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div id="LOGIN-MODAL" class="modal-body clearfix">
             <button class="model_disbtn" data-dismiss="modal">× </button>
              <h4 class="modal-title" id="loginmsg" align="center" style="color:blue;"></h4>
            <div class="col-sm-12">
              <h4 class="text-center">LOGIN WITH PAY1</h4>
            </div>
            <div class="col-sm-6 border-right" style="border-right:1px solid gray;">
              <div class="col-sm-12">
               <form method="post" id="login_modal" action = ''>  
                    <h5 >Login using mobile number</h5>
                    <h6 >Fill in the required information below</h6>
                    <span id ="login_mobile_num_error" style="color:red;"></span> 
                    <div class="input-group form-group">
                      <span class="input-group-addon" id="basic-addon1">+91</span>
                      <input  id ="mob-number" pattern="[789][0-9]{9}" maxlength="10" onkeypress="return isNumberKey(event)" onchange="return isNumberKey(event)" name="mob-number" type="tel" placeholder="Mobile Number" autocomplete="off" class="form-control" aria-describedby="basic-addon1" required>
                    </div>
                    <span id ="login_password_error" style="color:red;"></span>
                    <div class="input-group form-group">
                       <span class="input-group-addon" style="padding-left: 18px;"><i class="fa fa-key fa-fw"></i> </span> 
                       <!--<span class="input-group-addon glyphicon glyphicon-lock"></span>-->  
                      <input name="password" id="password"  placeholder="Password" type="password" class="form-control">
                    </div>
                    <span id ="invalid_user_name_password" style="color:red;"></span>
                <div id="forgot-password-container">
                    <div><a onclick="forgetPasswordClick('forgot-password-div','LOGIN-MODAL',event)" style="cursor:pointer;">Forgot Password?</a></div>
                    <span class="forgot-password-text"><small>(Create a new password)</small></span>
                </div>
                <div class="form-group">
                  <!--<input type="submit" Value="Login" class="btn btn-save center-block">-->
                    <input id="login_button" type="submit" Value="LOGIN" class="btn btn-save center" style="border-radius:5px">
                </div>
               </form>
<!--               <div class="terms-condition-wrapper">
                 <div class="margin-bottom-20">
                     <i class="terms-condition-message-txt">By entering your mobile number you agree to our <a href="/terms">Terms & Conditions </a> and <a href="/privacy-policy">Privacy Policy </a>.</i>
                  </div>
              </div>    -->
                <!--<div>OR</div>-->  
                  <!--<button class="btn btn-primary center-block topsignup" id="sign_up" data-dismiss="modal" type="submit">Sign up</button>-->
<!--                  <button class="btn btn-primary center" id="sign_up" data-dismiss="modal" type="submit">Sign up</button>-->
              </div>
            </div>
            
            <div class="col-sm-6 ">
              <!--<h5 class="text-center">Login Using</h5>-->
              <div class="" style="margin-top:30px;">
                <!--<a class="facebook-login" href="#"><img src="assets/images/icons/fb.png" alt=""></a>-->
                <h5>Singup using mobile number</h5>     
              </div><br>
               
              <div class="margin-top-20">
                <button class="btn btn-primary center" id="sign_up" data-dismiss="modal" type="submit">Sign up</button>   
                <!--<a class="google-login margin-top-50" href="#"><img src="assets/images/icons/gogle.png" alt=""></a>-->
              </div>
              <br>
              <div class="terms-condition-wrapper">
                 <div class="margin-bottom-20">
                     <i class="terms-condition-message-txt">By entering your mobile number you agree to our <a href="/terms">Terms & Conditions </a> and <a href="/privacy-policy">Privacy Policy </a>.</i>
                  </div>
              </div>    
<!--              <div >OR</div>-->
            </div>
          </div>
         <div id='forgot-password-div' style="display: none" class="modal-body clearfix">
             <button class="model_disbtn" data-dismiss="modal">× </button>
              <h4 class="modal-title" id="loginmsg" align="center" style="color:blue;"></h4>
            <div class="col-sm-12">
              <h4 class="text-center">RESET PASSWORD</h4>
            </div>
            <div class="col-sm-6 border" style="border-right:1px solid gray;">
              <div class="col-sm-12">
               <form method="post" id="forgot-password-form" action = '' onsubmit="return checkOTP_Password(event)" >        
                <h5 >You will receive an OTP</h5>
                <h6 >Fill in the required information below</h6>
                <span id ="sent_otp" style="color:blue;"></span>
                <div class="form-group">
                    <input  id ="forget_otp"  name="forget_otp" type="text" placeholder="OTP" autocomplete="off" class="form-control" aria-describedby="basic-addon1" onfocus="clearsentOTP()" required>
                </div>
                <div class="form-group">
                    <input name="forget_password" id="forget_password"  placeholder="New Password" type="password" class="form-control" required>
                </div>
                <div class="form-group">
                    <input name="confirm_password" id="confirm_password"  placeholder="Confirm Password" type="password" class="form-control" onblur="password_match()" required>
                </div>
                <span id ="wrong_password" style="color:red;"></span> 
                <span id ="wrong_otp" style="color:red;"></span> 
                <span id ="invalid_otp" style="color:red;"></span>
                <div class="form-group">
                    <!--<input type="submit" Value="Confirm" class="btn btn-save center">-->
                    <input type="submit" class="btn btn-danger" value="Confirm">
                    <input id="resend_otp" type="button" class="btn btn-danger" value="Resend OTP" onclick="send_OTP()">
<!--                    <input id="resend_otp" type="button" Value="Resend OTP" class="btn center" onclick="send_OTP()" style="margin-top: 10px;">-->
                </div>
                <span id ="correct_otp" style="color:blue;"></span> 
                 <span id ="correct_password" style="color:blue;"></span> 
               </form>
              </div>
            </div>
              <div class="otp-message-wrapper col-sm-6 ">
                 <div class="margin-top-40">
                 <i class="otp-message-txt">We will send One Time Password (OTP) on your mobile to reset your password.</i>
                  </div>
              </div>
              <div style="float:right; bottom: 0;"><span><a class="back-link" onclick="forgetPasswordClick('LOGIN-MODAL','forgot-password-div')" style="cursor:pointer;">Back</a></span></div>
          </div>
            
        </div>
      </div>
     
     <div >
      </div> 
    </div>


  <!--- **********************************End Login with pay1 model *************************** -->


 <!--- ********************************** Signin with pay1 model *************************** -->
        <div class="modal fade" id="sign_up_Modal" tabindex="-1" role="dialog" aria-labelledby="sign_up_Modal" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body clearfix">
                    <button class="model_disbtn" data-dismiss="modal">× </button>
                    <div class="col-sm-12">
                      <h4 class="text-center" id="myModalLabel">SIGNUP WITH PAY1</h4>
                    </div>
                    <form method="post" id="signin_modal" action = ''>
                    <div class="col-sm-6 border-right" style="border-right:1px solid gray;">
                      <div class="col-sm-12">
                          <span id ="sign_success" style="color:red;"></span>
                          <span id ="sign_msg" style="color:red;"></span>
                          <br>
                          <strong class="login_font signup_header">Signup using mobile number</strong>
                          <p></p>
                        <span id ="sign_mobile_num_msg" style="color:red;"></span>
                        <div class="input-group form-group mobno">
                          <span class="input-group-addon" id="basic-addon1">+91</span>
                          <input id="mobnumber" name="mobnumber" type="tel" maxlength="10" class="form-control" onkeypress="return isNumberKey(event)" onfocus="clearsentOTP()" aria-describedby="basic-addon1">
                        </div>
                        <span id ="sign_otp_msg" style="color:red;"></span>
                        <span id ="sign_otp_sent" style="color:blue;"></span>
                        <div class="form-group otp signup_otp" style="display: none;">
                          <div class="col-sm-12">
                             <h5 >You will receive an OTP</h5>

                             <span id ="sent_otp" style="color:blue;"></span>
                             <div class="form-group">
                                 <input  id ="signup_otp"  name="signup_otp" type="text" placeholder="OTP" autocomplete="off" class="form-control" aria-describedby="basic-addon1" onfocus="clearsentOTP()" required>
                             </div>
                             <div class="form-group">
                                 <input name="signup_password" id="signup_password"  autocomplete="off" placeholder="New Password" type="password" class="form-control" required>
                             </div>
                             <div class="form-group">
                                 <input name="signup_confirm_password" id="signup_confirm_password" autocomplete="off"  placeholder="Confirm Password" type="password" class="form-control" onblur="signup_password_match()" required>
                             </div>
                             <span id ="signupError" style="color:red;"></span>
                           </div>  
                            <div class="form-group otp_button">
                                 <!--<input type="submit" Value="Confirm" class="btn btn-save center">-->
                                 <input type="button" data-toggle="modal" class="btn btn-danger" value="Confirm" onclick="checksignup_Otp(event)">
                                 <input id="resend_otp" type="button" class="btn btn-danger" value="Resend OTP" onclick="send_SignupOTP('mobnumber','signupError')">
             <!--                    <input id="resend_otp" type="button" Value="Resend OTP" class="btn center" onclick="send_OTP()" style="margin-top: 10px;">-->
                             </div>
                          <!--input type="password" id="otp" name="otp" placeholder="OTP" class="form-control" onfocus="clearsentOTP()"-->
                        </div>
                      <span id ="valid_mobile_num" style="color:red;"></span>  
                <button class="btn btn-lg btn-primary btn-block" type="button" id="register_user">Create an account</button>
                <button class="btn btn-primary center" data-toggle="modal" data-target="#basicModal" data-dismiss="modal" id="login_signup" type="buton" style="display:none">Log in</button>
                 <!--input type="button" Value="Submit" class="btn btn-save btn-lg btn-primary btn-block" id="login_signup"  style="display: none;" -->
                <!--<button class="btn btn-save btn-lg btn-primary btn-block" id="check_otp"  style="display: none;" type="button">Submit</button>-->
                      </div>
                    </div>
                    </form>        
                        
                    <div class="col-sm-6 text-center ">
                        <div class="terms-condition-wrapper">
                            <div class="margin-top-40">
                                <i class="terms-condition-message-txt">By entering your mobile number you agree to our <a href="/terms">Terms & Conditions </a> and <a href="/privacy-policy">Privacy Policy </a>.</i>
                             </div>
                        </div>  
<!--                      <h5 class="text-center">Signin Using</h5>
                      <div class="margin-top-50" style="margin-top:30px;">
                        <a class="facebook-login" href="#"><img src="assets/images/icons/fb.png" alt=""></a>
                      </div><br>
                      <div class="margin-top-20">
                        <a class="google-login margin-top-50" href="#"><img src="assets/images/icons/gogle.png" alt=""></a>
                      </div>
                      <div class="or">OR</div>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>

  
<!--- ********************************** End Signin with pay1 model *************************** -->  
  
<div class="row collapse" id="rechargemenu" >
    <div class="col-md-12">
<!--        <div class="tabbable">
            <div class="tabbable-line">-->
                <div class="clearfix"></div>
                <div class="container_fluid bottom_nav nav nav-pills nav-stacked" style="height:76px;margin-top: 50px;">
                    <div class="container">
                           <ul class="header_bottom_menu">
                                  <li class="active mob"><a data-toggle="pill"  href="#prepaid-mobile">Prepaid mobile</a></li>
                                  <li class="post"><a data-toggle="pill" href="#postpaid-mobile">postpaid mobile</a></li>
                                  <li class="dth"><a data-toggle="pill"  href="#dth">dth</a></li>
                                  <li class="data"><a data-toggle="pill" href="#data">datacard</a></li>
                                  <li><a data-toggle="pill" href="#quick_pay" id="getQuickPaylist">quick recharge</a></li>
                                  <li><a data-toggle="pill" href="#add_money">add money</a></li>
                       </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="tab-content" >
                    <!-- Prepaid mobile -->
                    <div class="tab-pane active " id="prepaid-mobile">
                        <div class="menu_content">
                            <div class="container">
                                <div class="row margin-top-25 minheight-500">
                                    <div class="col-md-4 col-sm-4 col-xs-12" id="mobile">
                                        <form method="post" id="mob_form" action="">
                                        <input type="hidden" id="mob_flag" name="mob_flag" value="1">
                                        <input type="hidden" id="mob_circle" name="mob_circle" value="">
                                        <input type="hidden" id="mob_max" name="mob_max" value="">
                                        <input type="hidden" id="mob_min" name="mob_min" value="">
                                        <input type="hidden" id="mob_id" name="mob_id" value="">
                                        <input type="hidden" id="mob_product" name="mob_product" value="">

                                        <h4 class="recharge_header">Recharge Your Prepaid Mobile</h4>
                                        <span id ="incorrect_mobile_num" style="color:red;"></span>
                                            <div class="form-group" style="width:280px;">
                                              <input type="text" class="form-control" placeholder="Enter your mobile no" name="mob_number" value="" id="mob_number" maxlength="10" onkeypress="getnumberDetails(this.value,'mob_provider','mob_amount');return isNumberKey(event)" onchange="getnumberDetails(this.value,'mob_provider','mob_amount');return isNumberKey(event)" onblur="checkValidMobNum('prepaid_num')" >
                                            </div>           
                                        <br/>
                                        <div class="form-group">
                                            <div class="form-group" style="width:280px;"> 
                                                <select class="form-control" name="mob_provider" id="mob_provider" onchange="getplanoperatorchange('mob')">
                                                    <option value="">Select Service Provider</option>
                                                    <?php foreach($arrayval["Mobile"] as $key => $val){?>
                                                    <option value="<?php echo $val["opr_code"] ?>"><?php echo $val["name"] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <span id="spl_rchg" style="display:none"><input type="radio" name="special_recharge" value="1" > Special &nbsp;&nbsp;&nbsp;<input type="radio" name="special_recharge" value="0" checked> Normal</span>
                                        </div>
                                        <br/>
                                        <div class="form-group">            
                                             <div class="form-group" style="width:280px;"><input type="text" class="form-control" placeholder="Enter Amount" value="" name="mob_amount" id="mob_amount"  onkeypress="return isNumberKey(event)"  maxlength="5"></div>                 
                                        </div>
                                        <br/>
                                        
                                        <div class="form-group">           
                                        <div class="form-group">
                                            <button class="btn btn-success orderinfo" style="width:280px;" value="mob"  data-toggle="modal" type="button" data-target=".orderconfirmation">Continue to Payment
                                            </button> 
                                        </div>
                                     </div>
                                     </form>

                                    </div>
                                
                                <div class="margin-top-25">
                                    <div class="col-md-8 col-sm-8 col-xs-12 mob " style="display: none;">
                                        <div class="checkplans" id="showmobplan" >
                                          <div class="sliderowbar">
                                            <ul class="nav nav-tabs" role="tablist" id ="mob_plan">

                                            </ul>
                                          </div>
                                            <table class="form-control" id="mobile_circle"  style="display:none;">
                                                <tr width="670">
                                               <!-- <th>Select Circle</th>-->

                                                <td><select class="form-control" name="mob_cir" id="mob_cir" onchange="getplanoperatorchange('mob')">
                                                <option value="">Select Circle</option>
                                                <?php foreach($getcircle as $key => $val){ if($val["code"]!="SC"){?>
                                                <option value="<?php echo $val["code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php }} ?>

                                                </select></td>
                                                </tr>
                                            </table>

                                        <!-- Tab panes -->
                                        <div class="tab-content mT20"  id ="mob_plandetails" style="">

                                        </div>
                                         <div class="text-right">
                                            <a href="javascript:void(0);" class="hidecheckplans"  title="Close">Close &nbsp;</a>
                                         </div>	
                                     </div>
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <!-- Prepaid mobile END-->
                    <!-- Postpaid mobile -->
                    <div class="tab-pane" id="postpaid-mobile">
                         <div class="menu_content">
                             <div class="container">
                                 <div class="row margin-top-25 minheight-500">
                                 
                                     <div class="col-md-4 col-sm-4 col-xs-12">
                                        <form method="post" id="post_form" action="login.php">  
                                            <input type="hidden" name="post_flag" id="post_flag" value="4">
                                             <input type="hidden" name="post_id" id="post_id" value="">
                                             <input type="hidden" name="post_min" id="post_min" value="">
                                             <input type="hidden" name="post_max" id="post_max" value="">
                                             <input type="hidden" name="post_charge" id="post_charge" value="">
                                             <input type="hidden" name="post_servicecharge" id="post_servicecharge" value="">

                                            <h4 class="recharge_header">Recharge Your Postpaid Mobile</h4>
                                            <span id ="incorrect_postpaid_mobile_num" style="color:red;"></span>
                                            <div class="form-group">           
                                                <div class="form-group" style="width:280px;"><input type="text" style="background:" size="30" class="form-control" placeholder="Enter  PostPaid Number" value="" maxlength="10"  id="post_number" name="post_number" onkeypress="getdataoperatorvalue(this.value,'post_provider','post_amount');return isNumberKey(event)" onblur="checkValidMobNum('postpaid_num')"></div>           
                                            </div>
                                            <br/>
                                            <div class="form-group">
                                            <div class="form-group" style="width:280px;"> 
                                            <select class="form-control" name="post_provider" id="post_provider" onchange="getplanoperatorchange('post');">
                                                <option value="">Select Service Provider</option>
                                                            <?php foreach($arrayval["postpaid"] as $key => $val){?>
                                                            <option value="<?php echo $val["opr_code"] ?>"><?php echo $val["name"] ?></option>
                                                            <?php } ?>
                                                 </select>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="form-group">            
                                            <div class="form-group" style="width:280px;"><input type="text"   value="" class="form-control" name="post_amount" id="post_amount" onkeyup="showservicecharge();" onkeypress="return isNumberKey(event);" placeholder="Enter Amount"  maxlength="5"></div> 
                                            <span><small id ="post_charge_msg" style="display:none"></small></span>
                                        </div>
                                        <br/>
                                        <div class="form-group">           
                                            <div class="form-group"><button class="btn btn-success orderinfo" value="post" type="button" data-toggle="modal" data-target=".orderconfirmation" class="btn btn-primary btn-lg btn-block" style="width:280px;">Continue to Payment</button>  </div>
                                         </div>
                                     </form>
                                 </div>
                                 
                            </div>
                          </div>
                     </div>
                             
                        
		</div>
                     <!-- Postpaid mobile END-->
                      <!-- Data card dtart-->
               <div class="tab-pane" id="dth">
                   <div class="menu_content">
                        <div class="container">
                            <div class="row margin-top-25 minheight-500">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                <form method="post" id="dth_form" action="login.php">
                                     <input type="hidden" name="dth_flag" id="dth_flag" value="2">
                                     <input type="hidden" name="dth_max" id="dth_max" value="">
                                     <input type="hidden" name="dth_min" id="dth_min" value="">
                                     <input type="hidden" name="dth_id" id="dth_id" value="">
                                     <input type="hidden" name="dth_product" id="dth_product" value="">
                                     <input type="hidden" name="dth_circle" id="dth_circle" value="">

                                     <h4 class="recharge_header">Recharge Your DTH</h4>
                                     <div class="form-group">
                                             <div class="form-group" style="width:280px;"> 
                                                     <select class="form-control" name="dth_provider" id="dth_provider" onchange="getplanoperatorchange('dth');">
                                                             <option value="">Select Operator</option>
                                                             <?php foreach ($arrayval["dth"] as $key => $val) { ?>
                                                                     <option value="<?php echo $val["opr_code"] ?>" ><?php echo $val["name"] ?></option>
                                                             <?php } ?>
                                                     </select></div>
                                     </div>
                                     <br/>
                                     <div class="form-group">           
                                             <div class="form-group" style="width:280px;"><input type="text" size="30" class="form-control" value="" onkeypress="return isNumberKey(event)" name="dth_number" id="dth_number" maxlength="25">
                                             <span id="dth_error_msg" style="color:red;display:none"></span>
                                             </div>           
                                     </div>


                                     <br/>
                                     <div class="form-group">            
                                             <div class="form-group" style="width:280px;"><input type="text" class="form-control" placeholder="Enter Amount" name="dth_amount" placeholder="Enter Amount" id="dth_amount" value="" onkeypress="return isNumberKey(event)" class="form-control"  maxlength="5"></div>                 
                                     </div>
                                     <br/>
                                     <div class="form-group">           
                                             <div class="form-group"><button class="btn btn-success orderinfo" type="button" value="dth" data-toggle="modal"  data-target=".orderconfirmation"  style="width:280px;">Continue to Payment</button>  </div>
                                     </div>
                                </form>
                             </div>
                                <div class="col-md-8 col-sm-8 col-xs-12 dth" style="display:none;">
                                    <div class="checkplans" id="showdthplan">
                                            <div class="sliderowbar">
                                                    <ul class="nav nav-tabs" id="dth_plan" role="tablist">

                                                    </ul>
                                            </div>
                                            <div class="tab-content mT20" id="dth_plandetails">

                                            </div>

                                            <div class="text-right">
                                                    <a href="javascript:void(0);" class="hidecheckplans"  title="Close">Close &nbsp;</a>
                                            </div>						
                                    </div>
                                </div>
                            </div>
                            <div class="margin-top-25">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Data card END-->
		<!-- Add money start-->			
		<div class="tab-pane "  id="add_money">
                    <div class="menu_content">
                        <div class="container">
                            <div class="row margin-top-25 minheight-500">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-3 ">
                                       <div class="addmoney borderright">
                                          <ul class="tabs add_mon margin-top-25">
                                              <li class="active"><a href="#locate" data-toggle="tab" aria-expanded="true">Locate Pay1 Store</a></li>
                                              <li class=""><a href="#online" onclick="clearNoRetailerMsg()" data-toggle="tab" aria-expanded="true">Online Payment</a></li>
                                             <li class=""><a href="#redeem" onclick="clearNoRetailerMsg()" data-toggle="tab" aria-expanded="true">Reedeem Voucher</a></li>
                                          </ul>
                                       </div>
                                    <div class="margin-top-25"></div>
                                 </div> 
                                 <div class="col-md-9">

                                    <div class="tab-content">
                                        <span id = "TokenExpire" style="color:red; margin-left: 225px;"></span>
                                        <span id = "NoRetailer" style="color:red; margin-left: 225px;"></span>
                                       <!--Locate Pay1 store -->
                                       <div class="col-sm-12 tab-pane active" id="locate" >
                                            
                                            <div class="locatemap">
                                              <div id="map-canvas1"></div>
                                            </div>
                                      </div>
                                      <!--locate -->


                                       <!--Online Payment -->
                                       <div class="tab-pane" id="online">
                                           <div class="opay form_container" style="margin-top: 6px;">use your debit/credit card or <br>netbanking to topup pay1 wallet</div>
                                           <div class="col-sm-4" style="padding-left:0px">
                                                      <div>
<!--                                                          <span class="fa fa-inr"></span>-->
                                                          <input type="text" class="form-control" id="amount" name="amount" placeholder="ENTER AMOUNT" maxlength="5">
                                                      </div>
                                                        <span  style="color:red"><small id="addmoney_amount"></small></span>
                                                        <br>
                                                      <br>
                                                   <input type="button" value="CONFIRM" disabled="disabled" id="wallet_btn" class="btn btn-success" style="border-radius:5px;margin-right: 15px">
                                                   <input type="button" value="CANCEL" class="btn btn-default" onclick="cancel_voucher('amount','addmoney_amount')" style="border-radius:5px">
                                            
                                          </div>
                                       </div>

                                       <!--Online Payment End -->

                                       <!--Redeem start-->
                                       <div class=" tab-pane" id="redeem">
                                          <div class="opay form_container"  style="margin-top: 6px;">Redeem your pay1 voucher here</div><br>
                                          <div class="col-sm-4" style="padding-left:0px">
                                                <form action="" method="" id="redeem1" novalidate="validate">
                                                     <div> 
                                                         <input type="text" class="form-control" placeholder="VOUCHER CODE" name="voucher_code" id="voucher_code">
                                                     </div><span style="color:red;padding-left:15px" id="couponcode"></span>
                                                         <br><br>
                                                   <input type="button" value="CONFIRM" disabled="disabled" id="redeem_btn" class="btn btn-success" style="border-radius:5px;margin-top: 0px;margin-right: 15px">
                                                   <input type="button" value="CANCEL" class="btn btn-default" onclick="cancel_voucher('voucher_code','couponcode')" style="border-radius:5px">
                                                </form>
                                          </div>
                                       </div>
                                       <!--redeem end-->
                                    </div>         
                              </div>
                            </div>
                          </div>
                            
                     </div>
                 </div>
         <div class="clearfix margin-top-25"></div>
    </div>	   
             <!-- Add money END-->       
            <div id="data" class="tab-pane">
                <div class="menu_content">
                    <div class="container">
                        <div class="row margin-top-25 minheight-500">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <form method="post" id="data_form" action="login.php">
                                     <input type="hidden" name="data_id" id="data_id" value="">
                                     <input type="hidden" name="data_max" id="data_max" value="">
                                     <input type="hidden" name="data_min" id="data_min" value="">
                                     <input type="hidden" id="data_flag" name="data_flag" value="3">
                                     <input type="hidden" id="data_product" name="data_product" value="">
                                     <input type="hidden" id="data_circle" name="data_circle" value="">
                                     <h4 class="recharge_header">Recharge Your Data card</h4>
                                     <span id ="incorrect_datacard_mobile_num" style="color:red;"></span>
                                     <div class="form-group">           
                                         <div class="form-group" style="width:280px;"><input type="text" size="30" value="" maxlength="10" name="data_number" id="data_number" onkeypress="getdataoperatorvalue(this.value,'data_provider','data_amount')" class="form-control input" placeholder="Enter your mobile no" onblur="checkValidMobNum('datacard_num')"></div>           
                                     </div>
                                     <br/>
                                     <div class="form-group">
                                         <div class="form-group" style="width:280px;"> 
                                             <select class="form-control" id="data_provider" name="data_provider" onchange="getplanoperatorchange('data')">
                                                     <option value="">Select Service Provider</option>
                                                     <?php foreach ($arrayval["data"] as $key => $val) { ?>
                                                             <option value="<?php echo $val["opr_code"] ?>"><?php echo $val["name"] ?></option>
                                                     <?php } ?>
                                             </select>
                                         </div>
                                     </div>
                                     <br/>
                                     <div class="form-group">            
                                         <div class="form-group" style="width:280px;"><input type="text" name="data_amount"  value=""  id="data_amount" class="form-control"  onkeypress="return isNumberKey(event)"  placeholder="Enter Amount"  maxlength="5"></div>                 
                                     </div>
                                     <br/>
                                     <div class="form-group">           
                                         <div class="form-group"><button class="btn btn-success orderinfo" type="button" data-toggle="modal" value="data"  data-target=".orderconfirmation"  style="width:280px;">Continue to Payment</button>  </div>
                                      </div>
                                </form>
                        </div>
                            <div class="margin-top-25">
                                    <div class="col-md-8 col-sm-8 col-xs-12 data" style="display:none;">
                                        <div class="checkplans" id="showdataplan">
                                            <div class="sliderowbar">
                                                <ul class="nav nav-tabs"  id ="data_plan" role="tablist">

                                                </ul>
                                            </div>

                                             <table class="form-control" id="dat_circle"  style="display:none;">

                                                <tr>
                                   <!--                                                    <th style="border:none;">Select Circle</th>-->
                                                    <td style="border:none;"><select class="form-control" name="data_cir" id="data_cir" onchange="getplanoperatorchange('data')">
                                                <option value="">Select Circle</option>
                                                <?php foreach($getcircle as $key => $val){ if($val["code"]!="SC"){?>
                                                <option value="<?php echo $val["code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php }} ?>

                                        </select></th>
                                                </tr>
                                            </table>

                                            <!-- Tab panes -->
                                            <div class="tab-content mT20" id="data_plandetails">

                                            </div>
                                            <!-- close -->
                                            <div class="text-right">
                                                    <a href="javascript:void(0)" class="hidecheckplans" title="Close">Close &nbsp;</a>
                                            </div>						
                                        </div>
                                    </div>
                            </div>
                        </div>
                     </div>
              </div>

            </div>
            <!--Quick Pay start -->
            <input type="hidden" id="quickpay_id" name="quickpay_id" value="">
            <div id="quick_pay" class="tab-pane">

              <div class="menu_content">
                <div class="container">
                   <div class="row margin-top-25 minheight-500">
                      <div class="col-md-12">


                            <div class="quick_pay container">
                               <div class="row">
                                  <div class="col-md-12"> <div class="recharg_head">Recharge Your Mobile instantly</div></div>
                                  <h4 id="quickmsg" align="center" style="color:red;"></h4>
                                  <div class="row">
                                      <div class="col-md-12 scroll" id="quickpaydata" style="overflow-y: scroll; height:400px;">
                                          <div class="col-sm-6 col-md-6 margin-top-25">

                                             <table class="recharge table-bordered offer_tab">
                                                <tbody><tr>
                                                   <td class="amount" rowspan="2"><i class="fa fa-inr"></i> 250</td>
                                                   <td class="lasttd">  <button class="remove-offer">×</button></td>
                                                </tr>
                                                <tr>
                                                   <td> VODAFONE POSTPAID</td>
                                                </tr>

                                             </tbody></table>
                                          </div>
                                          <div class="col-sm-6 col-md-4 margin-top-25">
                                             <table class="recharge table-bordered offer_tab">
                                                <tbody><tr>
                                                   <td class="amount" rowspan="2"><i class="fa fa-inr"></i> 400</td>
                                                   <td class="lasttd">  <button class="remove-offer">× </button></td>
                                                </tr>
                                                <tr>
                                                   <td> VODAFONE POSTPAID</td>
                                                </tr>

                                             </tbody></table>
                                          </div>
                                      </div>
                                   </div>
                               </div>

                            </div>
                      </div>

                </div>
              <div class=""></div>
          </div>
          <div class="clearfix"></div>
          </div>
       </div>
            
            <!---********************************************model  DELETE quick Pay****************************************** -->
            <div class="modal fade" role="dialog" id="remove-offer-modal">
               <div class="modal-dialog">
                  <div class="modal-content">
                     <div class="modal-header">
                       <button class="model_disbtn" data-dismiss="modal">× </button>
                        <h4>Quick Recharge</h4>
                     </div>
                     <div class="modal-body clearfix text-center">
                        <h4>Are you sure you want to delete quickpay?</h4>
                        <div class="pull-right">
                            <button class="btn btn-save" onclick="deletequickpay()">OK</button>
                           <button class="btn btn-clear" data-dismiss="modal">Cancel</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
       <!---********************************************model end ****************************************** -->

    </div>
                <!---********************************************model Order confirmation****************************************** -->
		
                <div class="" id="orderconfirmation" style="display:none;" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="max-width:440px;">
<!--                        <form method="post" action="" id="payment_form">-->
                    <?php
                    $userData = getUserDetails();
                    $balance = intval($userData['walletbal']);
                    ?>
                        <input type="hidden" name="rec_amount" id="rec_amount" value="">
                        <input type="hidden" name="rec_operator" id="rec_operator" value="">
                        <input type="hidden" name="rec_flag" id="rec_flag" value="">
                        <input type ="hidden" name="rec_number" id="rec_number" value="">
                        <input type ="hidden" name="payment_option" value="" id="payment_option">
                        <input type="hidden" name="service_charge" id ="service_charge" value="">
                        <input type="hidden" name="service_charge_percent" id ="service_charge_percent" value="">
                        <input type="hidden" name="stv" id ="stv" value="">
                        <input type="hidden" name="total_amount" id ="total_amount" value="">
                        <input type="hidden" name="walletbal" id="walletbal" value="<?php echo $balance; ?>">
                        <input type="hidden" name="isloggedin" id="isloggedin" value="<?php echo $isLoggedIn; ?>">
                        <input type="hidden" name="max_amount" id ="max_amount" value="">
                        <input type="hidden" name="min_amount" id ="min_amount" value="">
                        <input type="hidden" name="charge_slab" id ="charge_slab" value="">
                         <div class="modal-content">
                           <div class="modal-header1">
                              <button data-dismiss="modal" style="position: relative; left: -11px;" class="close">× </button>
                              <div class="text-center"><h4>CONFIRMATION</h4></div>
                           </div>
                           <div class="modal-body clearfix text-center">
                              <div class="text-center"><h4>Are you sure you want to recharge?</h4><br><br></div>

                              <div style="border:1px solid #000; width:298px; margin:0px auto;padding:0px" class="">

                                <div class="col-md-3 text-center">
                                  <img class="" alt="" id="operator_logo" src="/assets/images/icons/you_can_pay_2.png"  style="height: 72px;width: 77px;">
                                  <span  id="provider_name" style="display: none"></span>
                                </div>
                                <div class="col-md-9 text-center">
                                 <p class="modelheading" id ="number"></p><b><div class="break"></div></b>
                                 <p class="modelheading"><i class="fa fa-inr"></i>
                                     <input type="textbox" style="display: none; border-top-style: none; border-right-style: none; width:155px" name="postpaid_amt_textbox" id="postpaid_amt_textbox" onkeyup="show_service_msg('1')" >
                                 <span id ="trans_amount"></span></p>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                              <div class="blue_ordertxt" id="service_msg" style="display: none;"></div> 
                              <div style="margin-top:20px;" class="">
                                  <?php $display=""; 
                                  if(empty($balance)){
                                      $display = "none";
                                  }?>
                                  <input type="checkbox" checked="checked" value="balance" name="usewallet" id="usewallet" class="walletbal_text" style="display:<?php echo $display; ?>"> <span class="walletbal_text" style="display:<?php echo $display; ?>">USE WALLET BALANCE ( <i class="fa fa-inr"></i> <span class="walletbal_text" style="display:<?php echo $display; ?>" id="walletbal_val"><?php echo $balance; ?></span> )</span> 
                              </div>
                              <br>
                              <div style="margin-top:20px;" class="">
                                  <table colspan="2" cellspan="2">
                                        <tbody>
                                            <tr>
                                                <td style="width:40px"><span class="loader_voucher" style="float: left; display: none; width: 40px;"><img class="loading-image" src="/images/bx_loader.gif" alt="loading.."></span></td>
                                                <td style="width:160px;padding-left:5px">
                                                    <input type="textbox" style="width:155px"  autofocus name="coupon_code" placeholder="Enter promocode here" id="coupon_code">
                                                </td>
                                                <td style="width:160px;padding-left:5px"><button type="button" onclick="check_voucher_validity()" class="btn btn-info">Apply Voucher</button></td>
                                            </tr>
                                      </tbody>
                                      </table>
                                  
                              </div>
                              <br>
                              <div style="display:none;text-align: center" id="voucher_code_validity"></div>
                              <input type="hidden" id="user_voucher_code" name="user_voucher_code" />
                              <br>
                             
                              <div class="row"> <div style="float:left; width:50%;" class=""><button class="btn btn-default btn-block  payment rnone" id="recharge_mob">CONFIRM</button></div>
                                <div style="float:left; width:50%;" class=""><button data-dismiss="modal" class="btn btn-default btn-block rnone">CANCEL</button></div></div>

                           </div>
                            
                        </div>
        
<!--                      </form>-->
                    </div>
                 </div>
                
                 <!---********************************************END model Order confirmation****************************************** -->
    <!---********************************************START Model Exceed limitation on Locate Pay1 Stores****************************************** -->
    <div class="modal fade" role="dialog" id="locatepay1_maplimit_exceed" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header1">
                <button class="close" data-dismiss="modal">&times; </button>
                <div class="text-center"><h4>This activity doesn't look genuine. </h4></div>
              </div>
                
              <div class="modal-body clearfix text-center">
                   <img src="/assets/images/map/ic_ghost.jpg" alt="">  
                <div class="text-center">
                    <h4 id='grab_error'><strong>Are you a ghost ?</strong></h4>
                </div>
                <div class="text-center">
                    <h4 id='grab_error'>Please reload the page again.</h4>
                </div>          
                <div class="margin-top-50"></div>
                <div class="col-md-4"></div>
                
                <div class="col-md-4" style="text-align:center"><a href="/home" class="btn btn-default" role="button">RELOAD</a></div>
                <!--<div class="col-md-4" style="text-align:center"><button class="btn btn-default btn-block" data-dismiss="modal">DONE</button></div>-->
                <div class="col-md-4"></div>
              </div>
            </div>
        </div>
    </div>
    <!---********************************************END Model Exceed limitation on Locate Pay1 Stores****************************************** -->

    </div>
        <div class="loader" style="display:none;">
            <center>
            <img class="loading-image" src="/images/bx_loader.gif" alt="loading..">
            </center>
	</div>
    </div>
      <script src="https://maps.googleapis.com/maps/api/js"></script>
       <?php
        $tokenvalue = md5(uniqid(mt_rand(), true)); 
        $url = 'http://cashpg.pay1.in/apis/setTokenValue/'.$tokenvalue;
        $handle = curl_init($url);
        $res = curl_exec($handle);
       echo $body = "<input type='hidden' name ='token' id ='token' value='$tokenvalue'>";
       if($res == 1){ ?> 
        <!--<script src="http://maps.google.com/maps/api/js?sensor=true&libraries=places,drawing,geometry"></script>-->
     <?php 
//        require '/application/jsmin-php/jsmin.php';
//        echo "<script>". JSMin::minify(file_get_contents('/assets/js/locatepay1.js'))."</script>";
          echo "<script src='/assets/js/locatepay1.js'></script>";
         } ?>
      
<script>
	  $(".remove-offer").click(function(){
         
         $("#remove-offer-modal").modal('show');
          
         });
         function check_voucher_validity(){
             var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/check_usercashback/?";
             var recharge_amount = $("#rec_amount").val() ;
             var code = $("#coupon_code").val();
             //var user_id = $("#user").val();
            // console.log('recharge_amount '+recharge_amount+' code '+code+' user_id '+user_id);
            // user_id : user_id,
                        
             $.ajax({
                    url: url,
                    type:"GET",
                    data:{
                         recharge_amount : recharge_amount,
                         code : code,
                         api_version:3,
                         res_format : "jsonp"
                        },
                   timeout: 50000,
                   dataType: "jsonp",
                   async:false,
                    success:function(data){ console.log(data);
                       if(data.status=="success"){
                        $("#voucher_code_validity").html(data.description);
                         $("#user_voucher_code").val(data.code);
                          $("#voucher_code_validity").show();
                    }else if(data.status=="failure" && data.errCode=='201'){
                        $("#basicModal").modal('toggle');
                    }
                     else if(data.status=="failure") {
                          $("#voucher_code_validity").html(data.description);
                           $("#voucher_code_validity").show();
                     }
                   },
                    beforeSend: function(){
                        $("#voucher_code_validity").hide();
                       $('.loader_voucher').show();
                    },
                    complete: function(){
                       $('.loader_voucher').hide();
                     },
                    error: function (xhr,status,error) {
                        console.log(xhr);
                        console.log(error);
                        console.log(status);
                    }

               });
         }
     function deletequickpay_quick(id){
          $("#quickpay_id").val(id);
          $("#remove-offer-modal").modal('show');
     }
  function getnumberDetails(value,opertatortype,amount)
        {
            $("#incorrect_mobile_num").html('');
            $("#incorrect_postpaid_mobile_num").html('');
            $("#incorrect_datacard_mobile_num").html('');
            var mobileno = value;
            //alert(mobileno.length);
            var serviceurl =panelUrl+"/apis/receiveWeb/mindsarray/mindsarray/json?method?";
            var no = '';
            var methodtype = "getMobileDetails";
            var plandetails = "getPlanDetails";
            var spitoperatortype = (opertatortype.split("_"));
            var operatorobj = JSON.parse(<?php echo json_encode($prepaiddata); ?>);
            var limit = JSON.parse(<?php echo json_encode($prepaidlimit); ?>);
            if(mobileno.length < 4){
                $("#mob_plan").html('');
                $("#mob_plandetails").html('');  
            }
			
            if((mobileno.length == 9) || (mobileno.length==10 && amount == '')){
            $.ajax({
					url:serviceurl,
					data:{method :methodtype,mobile: value},
					type: "POST",
					dataType: 'jsonp',
					jsonp: 'root',
					success:function(data) {
					 if(data[0].status=="success") {
                                             
                                          console.log('Data '+data);   
					  $("#"+opertatortype).val(data[0].details.operator);
					  $("#"+amount).focus();
					  var operatorid = operatorobj[data[0].details.operator];
					  var productid = limit[data[0].details.operator][4];
					  var circle =  data[0].details.area;
					  $("#mob_cir").val(circle);
					  $("#mob_circle").val(circle);
					  $("#mob_max").val(limit[data[0].details.operator][0]);
					  $("#mob_min").val(limit[data[0].details.operator][1]);
					  $("#mob_id").val(operatorid);
				      $("#mob_product").val(productid);
					  var specialrecharge =limit[data[0].details.operator][2];
					  if(specialrecharge==1){
							$("#spl_rchg").show();
					   }
					   if(circle!=''){
							 $("#mob_checkplan").show();
						}
				      if(mobileno.length == 9){
					  $("#"+amount).focus();
				      }
					  b2c.core.browseplan(plandetails,productid, circle, spitoperatortype[0]);
          }
          else
          {
              $("#"+opertatortype).val('');
          }
        }, error: function (xhr,error) {
              console.log(error);
          },
        });
       }
       
    }
	
function getplanoperatorchange(type){
	  
      var operatorid = $("#"+type+"_provider").val();
      var plandetails = "getPlanDetails";
      var number = $("#"+type+"_number").val();
      if(type=="dth"){
          
      if(operatorid=="AD"){
          $("#dth_number").attr("placeholder", "Enter Customer ID").blur();
      }
      else if(operatorid=="BD" || operatorid=="SD" ){
          $("#dth_number").attr("placeholder", "Enter Smart Card Number").blur();
      }
      else if(operatorid=="DD"){
          $("#dth_number").attr("placeholder", "Enter Viewing Card Number").blur();
      }
      
      else if(operatorid=="TD"){
          $("#dth_number").attr("placeholder", "Enter Subscriber ID").blur();
      }
      else if(operatorid=="VD"){
          $("#dth_number").attr("placeholder", "Enter DTH Number").blur();
      }
      
      var circle = "all";
      $("#dth_plan").html('');
      $("#dth_plandetails").html('');
       var limit = JSON.parse(<?php echo json_encode($dthlimit); ?>);
       var operatorobj = JSON.parse(<?php echo json_encode($dthdata); ?>);
	   $("#"+type+"_max").val(limit[operatorid][0]);
       $("#"+type+"_min").val(limit[operatorid][1]);
       $("#"+type+"_id").val(operatorobj[operatorid]);
        $("#"+type+"_product").val(limit[operatorid][4]);
        $("#"+type+"_circle").val(circle);
        if(circle!=''){
        $("#dth_checkplan").show();
        }
        b2c.core.browseplan(plandetails,limit[operatorid][4],circle,type);
      }
       else if(type=="mob"){
            $("#mob_plan").html('');
            $("#mob_plandetails").html('');  
            var circle = $("#mob_cir").val();
            var limit = JSON.parse(<?php echo json_encode($prepaidlimit); ?>);
            var operatorobj = JSON.parse(<?php echo json_encode($prepaiddata); ?>);
            b2c.core.checkcircle(number,plandetails,limit[operatorid][4],circle,type);
            $("#"+type+"_max").val(limit[operatorid][0]);
            $("#"+type+"_min").val(limit[operatorid][1]);
            $("#"+type+"_id").val(operatorobj[operatorid]);
            $("#"+type+"_product").val(limit[operatorid][4]);
             $("#mob_circle").val(circle);
            var specialrecharge =limit[operatorid][2];
            if(specialrecharge==1){
                 $("#spl_rchg").show();
             } else {
                  $("#spl_rchg").hide();
             }
             if(circle!=''){
                  $("#mob_checkplan").show();
             }

       }
       else if(type=="data"){
            $("#data_plan").html('');
            $("#data_plandetails").html('');  
            var circle = $("#data_cir").val();
            var limit = JSON.parse(<?php echo json_encode($datalimit); ?>);
            var prepaidlimit = JSON.parse(<?php echo json_encode($prepaidlimit); ?>);
            var operatorobj = JSON.parse(<?php echo json_encode($data); ?>);
            b2c.core.checkcircle(number,plandetails,prepaidlimit[operatorid][4],circle,type);
            $("#"+type+"_id").val(operatorobj[operatorid]);
            $("#"+type+"_max").val(limit[operatorid][0]);
            $("#"+type+"_min").val(limit[operatorid][1]);
            $("#"+type+"_product").val(prepaidlimit[operatorid][4]);
            $("#data_circle").val(circle);
             if(circle!=''){
                  $("#data_checkplan").show();
             }
     
       }
       else if(type=="post")
       {
            var limit = JSON.parse(<?php echo json_encode($postpaidlimit); ?>);
            var operatorobj = JSON.parse(<?php echo json_encode($postpaiddata); ?>);
            $("#"+type+"_id").val(operatorobj[operatorid]);
            $("#"+type+"_max").val(limit[operatorid][0]);
            $("#"+type+"_min").val(limit[operatorid][1]);
            $("#"+type+"_charge").val(limit[operatorid][3]);
            $("#"+type+"_servicecharge").val(limit[operatorid][4]);
            $("#service_charge_percent").val(limit[operatorid][4]);
       }
       
 }
	
	      function getdataoperatorvalue(value,opertatortype,amount)
           {
				var spitoperatortype = (opertatortype.split("_"));
				if(spitoperatortype[0]=="post"){
					var operatorobj = JSON.parse(<?php echo json_encode($postpaiddata); ?>);
					var limit = JSON.parse(<?php echo json_encode($postpaidlimit); ?>);
				}
				else{
					var operatorobj = JSON.parse(<?php echo json_encode($data); ?>);
					var limit = JSON.parse(<?php echo json_encode($datalimit); ?>);
					var prepaidlimit = JSON.parse(<?php echo json_encode($prepaidlimit); ?>);
					}
					var mobileno = value;
					var methodtype = "getMobileDetails";
					var plandetails = "getPlanDetails";
					var serviceurl = "<?php echo PANEL_URL; ?>/apis/receiveWeb/mindsarray/mindsarray/json?method?";
					if(mobileno.length == 9){
					$.ajax({
							url:serviceurl,
							data:{method :methodtype,mobile: value},
							type: "POST",
							dataType: 'jsonp',
							jsonp: 'root',
							success:function(data)
							{
							  if(data[0].status=="success"){
								var operatorval = data[0].details.operator;
								$("#"+opertatortype).val(operatorval);
								$("#"+spitoperatortype[0]+"_id").val(operatorobj[operatorval]);
								$("#"+spitoperatortype[0]+"_max").val(limit[operatorval][0]);
								$("#"+spitoperatortype[0]+"_min").val(limit[operatorval][1]);
								$("#"+spitoperatortype[0]+"_charge").val(limit[operatorval][3])
								var operatorid = operatorobj[operatorval];
								$("#"+amount).focus();
								var circle =  data[0].details.area;
								$("#data_circle").val(circle);
								$("#data_cir").val(circle);
								if(spitoperatortype[0]!='post'){
								var productid = prepaidlimit[operatorval][4];
								  $("#data_product").val(productid);
								 b2c.core.browseplan(plandetails,productid,circle,spitoperatortype[0]);
							   }
							  }
							  else
							  {
								  $("#"+opertatortype).val('');
							  }
							}
						});
					}
					   }
    
     $('#wallet').on('submit', function (event) {
         b2c.core.Wallettopup();
    });
    
    $("#redeem_btn").click(function(){
	b2c.core.redeemvoucher();
});
    
    $('#login_modal').on('submit', function (event) {
      var d = new Date();
      var mobileNo = $("#mob-number").val();
      var password = $("#password").val();
      event.preventDefault();
      if (mobileNo == ''){
        $("#login_mobile_num_error").html("<small>Please enter valid 10 digits Mobile Number</small>");
        $("#mob-number").focus();
        event.preventDefault();
        return false;
      }
      else if (password == '') {
        $("#login_password_error").html("<small>Password can not be blank</small>");
        $("#password").focus();
        event.preventDefault();
        return false;
      }
      else {
        var url = b2cUrl+"/index.php/api_new/action/actiontype/signin/api/true/?";
        $.ajax({
            url: url,
            type: "GET",
            data: {username: mobileNo,
                   password: password,
                   uuid: "",
                   latitude: "",
                   logitude: "",
                   device_type: "",
		   api_version:"3",
                   res_format: "jsonp"
            },
            timeout: 50000,
            dataType: "jsonp",
            crossDomain: true,
            success: function(data) {
                if (data.status == "failure"){
//				    console.log(data.description);
                    $("#invalid_user_name_password").html("<small>Invalid username or password</small>");   
                    }
                else{                   
                        $("#invalid_user_name_password").html("");
                        $("#loginmsg").html("Logged in successfully!!!!");
                        localStorage.setItem('passflag',password);
                      //  data.redeem=1
                        b2c.core.setUserData(data);

                        if(localStorage.getItem("redeem") == "1"){ 

                        }else 
                            if($("#mob_number").val() == ""){
                            $("#mob_number").val(mobileNo);
                            $("#post_number").val(mobileNo);
                            getnumberDetails(mobileNo,'mob_provider','');
                            $(".collapse").collapse('show');
                        }
                                       
                }
                
            },
              beforeSend: function(){
              $('.loader').show();
              },
             complete: function(){
             $('.loader').hide();
             },
            error: function(xhr, error) {
				
           },
        });
    }
	return false;

});

    $("#recharge_mob").click(function(){
        b2c.core.payment('rec_amount','rec_operator','rec_flag','rec_number');
    });
    
   $('.orderinfo').click(function(){
	   
	   b2c.core.orderinformation(this.value);
   });
   
   $("#check_otp").click(function(){
	   b2c.core.checkotp(event);
    });
   
   $("#register_user").click(function(){
	   b2c.core.register();
   });
   
   $("#logout").click(function(){
	   b2c.core.logout();
   });
   
   $("#charge_slabvalidatequickpay").click(function(){
	   b2c.core.validateQuickpayamount();
   });
   
   $("#getQuickPaylist").click(function(){
	   b2c.core.getQuickPaylist('1');
   });
  
   <?php 
       if(isset($parameters['status']) && ($parameters['status']== 'success')){ ?>
	  var mobnumber = localStorage.getItem('number');
	  var bal = "<?php echo $parameters['balance']; ?>";
	  if(mobnumber!=''){	
	   $("#trans_mobile").html(localStorage.getItem('number'));
	   $("#transaction_amount").html(localStorage.getItem('amount'));
	   $("#trans_balance").html("<?php echo $parameters['balance']; ?>");
	   $("#service_provider").html(localStorage.getItem('provider'));
	   $("#trans_id").html("<?php echo $parameters['txnid']; ?>");
	   var balance = {'wallet_balance' :bal};
	   b2c.core.updateprofile(balance);
       $("#ordersuccess").modal('toggle');
	   localStorage.setItem('number','');
    }
    <?php } else if(isset($parameters['status']) && ($parameters['status']== 'error')){?>
		
		var mobnumber = localStorage.getItem('number'); 
		var bal = "<?php echo $parameters['balance']; ?>";
	  if(mobnumber!=''){	
	   $("#trans_mobile").html(localStorage.getItem('number'));
	   $("#transaction_amount").html(localStorage.getItem('amount'));
	   $("#trans_balance").html("<?php echo $parameters['balance']; ?>");
	   $("#service_provider").html(localStorage.getItem('provider'));
	   $("#trans_id").html("<?php echo $parameters['txnid']; ?>");
	   var balance = {'wallet_balance' :bal};
	   b2c.core.updateprofile(balance);
	   localStorage.setItem('number','');
       $("#ordererror").modal('toggle');
   }
		
	<?php }?>
	function showquickpay(number, name, amount, operator, delegate_flag, confirmation, flag, id, stv, missedcall, operatorname, type, operatorcode){
		$("#orderconfirmation" ).addClass("modal fade orderconfirmation");
		$("#orderconfirmation").show();
        var wallet_bal = $("#walletbal").val();
          if(flag==2){
              $("#operator_logo").attr("src", '/assets/images/operator/'+opr_dth[operatorcode]);
          }
          else if(flag==3){
              $("#operator_logo").attr("src", '/assets/images/operator/'+opr_data[operatorcode]);
          }
          else{
              $("#operator_logo").attr("src", '/assets/images/operator/'+opr[operatorcode]);
          }
        if (parseInt(wallet_bal) < parseInt(amount) || (parseInt(wallet_bal) == 0)) {
            $("#wallet").hide();
            $("#walletmsg").html("Insufficient balance in wallet");
        }
        else {
            $("#walletmsg").html("");
            $("#wallet").show();
        }
        if (type == "postpaid"){
            var operatorobj = JSON.parse(<?php echo json_encode($postpaiddata); ?>);
            var limit = JSON.parse(<?php echo json_encode($postpaidlimit); ?>);
            $("#service_charge_percent").val(limit[operatorcode][4]);
            $("#charge_slab").val(limit[operatorcode][3])
            $("#max_amount").val(limit[operatorcode][0]);
            $("#min_amount").val(limit[operatorcode][1]);
            $("#quickpay_amount").show();
            $("#trans_amount").hide();
            $("#postpaid_amount").val(amount);
            $("#postpaid_amt_textbox").val();
            $("#postpaid_amt_textbox").show();
            $("#postpaid_amt_textbox").focus();
            var amt = ($("#postpaid_amt_textbox").val())?$("#postpaid_amt_textbox").val():0;
            show_service_msg(amt);
        }
        else {
              $("#service_msg").hide();
              $("#postpaid_amt_textbox").hide();
              $("#quickpay_amount").hide();
              $("#trans_amount").show();
            }
        $("#number").html(number);
        $("#trans_amount").html(amount);
        $("#provider_name").html(operatorname);
        $("#rec_amount").val(amount);
        $("#rec_number").val(number);
        $("#rec_operator").val(operator);
        $("#rec_flag").val(flag);
    }
	
	$(".hidecheckplans").click(function(){
                $(".checkplans").slideUp();
                $(".opencheckplans").prop('disabled', false);
   });
   
   $(document).ready(function(){
	   var url = document.URL;
	   url = url.split("/");
	   if(url.length == 4){
		   var array = JSON.parse(<?php echo json_encode($array); ?>);
		   url = url[3].split("-");
		   if(url.length==4 && url[2]!='dth'&& url[2]!='postpaid' ){
		   var oprId = url[1];
		   var type = url[2];
		   }
		   else if(url.length==4 && url[2]=='dth' || url[2]=='postpaid'){
			   var oprId = url[1]+"-"+url[2];
		       var type = url[2];
		   }
	           else if(url.length==5 && url[3]!='mobile') {
			   var oprId = url[1]+"-"+url[2]+"-"+url[3];
		       var type = url[3];
		   }
		   else if(url.length==5 && url[3]=='mobile'){
			   var oprId = url[1]+"-"+url[2];
		       var type = url[3];
		   }
		   else {
			    var oprId = url[1]+"-"+url[2]+"-"+url[3];
		        var type = url[4];
		   }
		  
		   if(type=='mobile'){
			  type = 'mob';
		   }  else if(type=='postpaid'){
			  type = 'post';
		   }else {
			    type = type;
		   }
		   
		   $("#"+type+"_provider").val(array[oprId]);
		   if(type=='mob'){
		   $("li."+type).addClass('active');
		   $(".dth").removeClass('active');
		   $(".data").removeClass('active');
		   $(".post").removeClass('active');
		   $("#prepaid-mobile").show();
		   $("#dth").hide();
		   $("#data").hide();
		   $("#postpaid-mobile").hide();
		   $("#rechargemenu").toggle(); 
		   } else if(type=='post'){
		   $("#postpaid-mobile").show();
		   $("#dth").hide();
		   $("#data").hide();
		   $("#prepaid-mobile").hide();
		   $("li."+type).addClass('active');
		   $("li.dth").removeClass('active');
		   $("li.data").removeClass('active');
		   $("li.mob").removeClass('active');
		   $("#rechargemenu").toggle(); 
		   }
		   else if(type=='data'){
		   $("#data").show();
		   $("#dth").hide();
		   $("#postpaid-mobile").hide();
		   $("#prepaid-mobile").hide();
		   $("li."+type).addClass('active');
		   $("li.dth").removeClass('active');
		   $("li.post").removeClass('active');
		   $("li.mob").removeClass('active');
		   $("#rechargemenu").toggle(); 
		   }
		   else if(type=='dth'){
		   $("#postpaid-mobile").hide();
		   $("#dth").show();
		   $("#data").hide();
		   $("#prepaid-mobile").hide();
		   $("li."+type).addClass('active');
		   $("li.data").removeClass('active');
		   $("li.post").removeClass('active');
		   $("li.mob").removeClass('active');
		   $("#rechargemenu").toggle(); 
		   }
		   if(type!='bill' || type!=''){
		   getplanoperatorchange(type);
	       }
	   
	   }
           
   });
	
     $('#sign_up').click(function () {
         $(".signup_otp").hide();
         $("#sign_msg").html('');
         $(".mobno").show();
         $(".signup_header").show();
         $("#register_user").show();
         $("#login_signup").hide();
                $('#sign_up_Modal').modal({
                    show: true
                });
     });   
    
         $("#amount").blur( function(){
            if($( this ).val()!=""){
                $('#wallet_btn').prop('disabled', false);
            }
        })
        
        $("#wallet_btn").click(function (){
            b2c.core.Wallettopup();
        });
        
        $("#voucher_code").blur( function(){
            if($( this ).val()!=""){
                $('#redeem_btn').prop('disabled', false);
            }
        })
        
        $("#redeem_btn").click(function (){
            b2c.core.redeemvoucher();
        });
        
        function checkValidMobNum(type){
            var txtMobile;
            var mob = /^[789][0-9]{9}$/;
            if(type=='prepaid_num'){
              txtMobile =  $("#mob_number").val();    
            }else if(type=='postpaid_num'){
              txtMobile =  $("#post_number").val();    
            }else if(type=='datacard_num'){
              txtMobile =  $("#data_number").val();    
            }    
            
            if (mob.test(txtMobile) == false) {
                if(type=='prepaid_num'){
                   $("#incorrect_mobile_num").html("<small>Please enter a valid 10 digits Mobile Number.</small>"); 
                   $("#mob_number").val('');
                   $("#mob_number").focus();
                }else if(type=='postpaid_num'){
                  $("#incorrect_postpaid_mobile_num").html("<small>Please enter a valid 10 digits Mobile Number.</small>");  
                  $("#post_number").val('');
                  $("#post_number").focus();
                }else if(type=='datacard_num'){
                  $("#incorrect_datacard_mobile_num").html("<small>Please enter a valid 10 digits Mobile Number.</small>");  
                  $("#data_number").val('');
                  $("#data_number").focus();   
                }  
                return false;
            }
            $("#incorrect_mobile_num").html("");
            $("#incorrect_postpaid_mobile_num").html("");
            $("#incorrect_datacard_mobile_num").html("");
            return true;    
        }
        
        //clear No Retailer error message on Locate Pay1 Store map
        function clearNoRetailerMsg(){
          $("#NoRetailer").html('');
        }

        function cancel_voucher(id,error_id){
            $("#"+id).val('');
            $("#"+error_id).html('')
            $("#"+error_id).hide();
        }
        function showservicecharge(){
            var amt = $("#post_amount").val();console.log(amt);
            if(amt>0){
                var charge = $("#post_charge").val();
                var servicechrg = charge.split(",");
                var mincharge = servicechrg[0].split(":");
                var maxcharge = servicechrg[1].split(":");

                if(parseInt(amt)>parseInt(mincharge[0])){
                   var totalamt = (parseInt(amt)+parseInt(mincharge[1]));
                   var s_charge = parseInt(mincharge[1]);

               }
               else {
                   var totalamt = (parseInt(amt)+parseInt(maxcharge[1]));
                   var s_charge = parseInt(maxcharge[1]);

               }
                console.log(totalamt);
                $("#post_charge_msg").html("(Total : " + parseInt(totalamt) + " =" + parseInt(amt) + "+" + s_charge + "    service charge inclusive of all taxes)");
                $("#post_charge_msg").show();
            }else{
                $("#post_charge_msg").html('');
                $("#post_charge_msg").hide();
            }
            
            
        }
        function refresh_captcha(){
            var d = new Date();
            var mobileNo = $("#mob-number").val();
           
            // validate user number
            if(mobileNo == ''){
              $("#login_mobile_num_error").html("<small>Please enter valid 10 digits Mobile Number</small>");
              $("#mob-number").focus();
              event.preventDefault();
              return false;
            }
            else{
                var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/refresh_captcha/?";
                $.ajax({
                    url:url,
                    type: "GET",
                    data : {
                        username : mobileNo,
                        api_version:"3",
                        res_format: "jsonp"
                    },
                    timeout: 50000,
                    dataType: "jsonp",
                    crossDomain: true,
                    success: function(data) {
                        if (data.status == "failure"){
//				    console.log(data.description);
                            $("#invalid_user_name_password").html("<small>Invalid username or password</small>");
                        }else{
                            $("#captcha_image").attr("src",'http://b2c.pay1.loc/images/'+data.captcha+'?var='+d.getTime());
                        }
                        
                    },
                    beforeSend: function(){
                       // $('.loader').show();
                    },
                    complete: function(){
                       // $('.loader').hide();
                    },
                   error: function(xhr, error) {

                  },
                });
            }
        }
    </script>