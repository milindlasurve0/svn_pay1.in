<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Recommended</title>
      <link rel="stylesheet" href="assets/css/bootstrap.min.css">
       <link rel="stylesheet" href="assets/css/homepage.css">
      <link rel="stylesheet" href="assets/css/style.css">
    
      <link rel="stylesheet" href="assets/css/navbar.css">
      <link rel="stylesheet" href="assets/css/footer.css">
      <link rel="stylesheet" href="assets/css/normalize.css">
      <link rel="stylesheet" href="assets/css/media_query.css">
      <link rel="stylesheet" href="assets/css/font-awesome.min.css">
      <link rel="stylesheet" href="assets/css/payment.css">
      <link rel="stylesheet" href="assets/validation/validationEngine.jquery.css">
      <link rel="stylesheet" href="assets/css/scrollToTop.css">
      <link rel="stylesheet" href="assets/css/easing.css">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div class="header-section">
         <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navigation" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#"><img src="assets/images/icons/logo_pay1.png" alt="logo"></a>
               </div>
               <div class="collapse navbar-collapse pull-right" id="navigation">
                  <ul class="nav navbar-nav">
                     <li><a href="#">Recharge/Pay bills</a></li>
                     <li><a href="#">Gift Stores</a></li>
                     <li class="active-nav"><a href="#">Redeem Gift Coin</a></li>
                     <li><a href="#">Sign up</a></li>
                     <li><a href="#">Login</a></li>
                  </ul>
               </div>
            </div>
         </nav>
      </div>
      <div class="clearfix" style="clear:both;"></div>

        <!-- =========================    slider STart here       ========================= -->
      <div style="min-height: 50px; margin-top:44px; font-family:'openSans';  font-family: 'opensanslight';">
       
         <div id="slider1_container" style="display: none; position: relative; margin: 0 auto;
            top: 0px; left: 0px; width: 1300px; height: 312px; overflow: hidden;">
             <div u="navigator" class="jssorb05" style="bottom: 400px; right: 6px;">
                <!-- bullet navigator item prototype -->
                <div u="prototype"></div>
            </div>
            <!-- Loading Screen -->
            <div u="loading" style="position: absolute; top: 0px; left: 0px;">
               <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block;
                  top: 0px; left: 0px; width: 100%; height: 100%;">
               </div>
               <div style="position: absolute; display: block; background: url(assets/images/icons/loader.gif) no-repeat center center;
                  top: 0px; left: 0px; width: 100%; height: 100%;">
               </div>

            </div>
            <!-- Slides Container -->
            <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1300px; height: 540px; overflow: hidden;">
              <div>
                  <img u="image" src2="assets/images/slider/slider_image_1.jpg" />
                 
                  <div style="position:absolute;width:1300px;height:100px;top:100px;left:30px;padding:5px;text-align:center;line-height:36px;font-size:20px;color:#FFFFFF; font-weight:bold">
                   
                  </div>
            
            </div>
               <div>
                  <img u="image" src2="assets/images/slider/slider_image_2.jpg" />
                  
                  <div style="position:absolute;width:1300px;height:100px;top:100px;left:30px;padding:5px;text-align:center;line-height:36px;font-size:20px;color:#FFFFFF;font-weight:bold;">
                 
                  </div>
               </div>
               <div>
                  <img u="image" src2="assets/images/slider/slider_image_2.jpg" />
                  
                  <div style="position:absolute;width:1300px;height:100px;top:100px;left:30px;padding:5px;text-align:center;line-height:36px;font-size:20px;color:#FFFFFF;font-weight:bold;">
                 
                  </div>
               </div>
             

            </div>
             
         </div>

      </div>
      <div class="bottom_bg" style=""></div>
      <!-- ========================slider end========================== -->
       <div class="clearfix"></div>
     
      </div>





      <div class="container-fluid">
         <div class="container border-both">
            <div class="row">
               <div class="col-md-12 text-center margin-top-25">
                  <div class="row">
                 <form action="" method="">
                        <div class="col-md-4 col-md-offset-4">
                           <input type="text" class="form-control" placeholder="Search by typing the product or brand name...">
                        </div>
                        <div class="col-md-1">
                            <input type="submit" value="SEARCH" class="btn btn-save search-btn">
                         </div>
                      </form>
               </div>
            </div>
               <div class="col-md-3 margin-top-30 ">
                  <div id="service_navigation" class="margin-top-50">
                    

                  <div id="accordian" class="border-right">
                     <ul>
                        <li><a href="">featured</a></li>
                        <li><a href="#contact">Offers of the Day</a></li>
                        <li class="active"><a href="#terms">Recommended</a></li>
                        <li><a href="#security">Near you</a></li>
                        <li >
                           <p class="mslide">Categories <i class="glyphicon glyphicon-chevron-down sicon"></i></p>
                           <ul>
                              <li><a href="#">Service</a></li>
                              <li><a href="#">E-voucher</a></li>
                              <li><a href="#">Beauty</a></li>
                           </ul>
                        </li>
                      
                        <li>
                          <p class="mslide">Locations <i class="glyphicon glyphicon-chevron-down sicon"></i></p>
                           <ul>
                              <li><a href="#">Reports</a></li>
                              <li><a href="#">Search</a></li>
                              <li><a href="#">Graphs</a></li>
                              <li><a href="#">Settings</a></li>
                           </ul>
                        </li>                 
                       
                     </ul>
                     
                     <div class='col-md-11 clearfix'><h3 class="gcoin">Gift Coins</h3></div>
                     <div class="row">
                       <div class="col-md-3 col-xs-2"><button class="range_btn" data-change="-20"><img src="assets/images/icons/minus.png" alt=""></button></div>
                          <div class="col-md-6 col-xs-4 row"><input id="slider1" type="range" min="100" max="500" step="10" class="" /></div>
                           <div class="col-md-3 col-xs-2"> <button class="range_btn" data-change="20"><img src="assets/images/icons/plus.png" alt=""></button></div>
                          <div class="clearfix"></div><br>
                    </div>

                      <img src="assets/images/services/iconall.png" alt=""> <input id="rangeValue1" type="text" size="5" >
                   
                  </div>
               </div>

               </div>
               <div class="col-md-9">
                  <div class="page_title">Recommended</div>
                  <div class='padding-both'>

                      <div id="products" class="row list-group">



                       <div class="item col-md-4 col-xs-6">
                           <div class="thumbnail">
                              <div class="col-md-10 col-xs-10">
                                  <p class="p_detail">get pastry  free on purchase of a cake</p>
                              </div>
                              <div class="col-md-2 col-xs-2">
                                 <p class="p_icon"><i class="glyphicon glyphicon-heart-empty"></i></p>
                              </div>
                             
                               <img class="group list-group-image img-responsive" src="assets/images/services/image1.png"  alt="" />
                               <div class="caption">
                                 
                                   <div class="row">
                                       <div class="col-md-6 col-xs-6">
                                          <img src="assets/images/services/all.png" alt="">
                                       </div>
                                       <div class="col-md-6 col-xs-6">
                                           <img src="assets/images/services/iconall.png" alt=""> <span>100</span>
                                       </div>
                                       <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                          <p class="group inner list-group-item-text">Birdy's</p>
                                          <p><img src="assets/images/services/icongift.png" alt=""> <span> Malad</span></p>
                                       </div>
                                   </div>

                              </div>
                           </div>
                       </div>


                         <div class="item col-md-4 col-xs-6">
                           <div class="thumbnail">
                              <div class="col-md-10 col-xs-10">
                                  <p class="p_detail">get pastry  free on purchase of a cake</p>
                              </div>
                              <div class="col-md-2 col-xs-2">
                                 <p class="p_icon"><i class="glyphicon glyphicon-heart-empty"></i></p>
                              </div>
                             
                               <img class="group list-group-image img-responsive" src="assets/images/services/2.png"  alt="" />
                               <div class="caption">
                                 
                                   <div class="row">
                                       <div class="col-md-6 col-xs-6">
                                          <img src="assets/images/services/all.png" alt="">
                                       </div>
                                       <div class="col-md-6 col-xs-6">
                                           <img src="assets/images/services/iconall.png" alt=""> <span>100</span>
                                       </div>
                                       <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                          <p class="group inner list-group-item-text">Birdy's</p>
                                          <p><img src="assets/images/services/icongift.png" alt=""> <span> Malad</span></p>
                                       </div>
                                   </div>

                              </div>
                           </div>
                       </div>

                         <div class="item col-md-4 col-xs-6">
                           <div class="thumbnail">
                              <div class="col-md-10 col-xs-10">
                                  <p class="p_detail">get pastry  free on purchase of a cake</p>
                              </div>
                              <div class="col-md-2 col-xs-2">
                                 <p class="p_icon"><i class="glyphicon glyphicon-heart-empty"></i></p>
                              </div>
                             
                               <img class="group list-group-image img-responsive" src="assets/images/services/3.png" alt="" />
                               <div class="caption">
                                 
                                   <div class="row">
                                       <div class="col-md-6 col-xs-6">
                                          <img src="assets/images/services/all.png" alt="">
                                       </div>
                                       <div class="col-md-6 col-xs-6">
                                           <img src="assets/images/services/iconall.png" alt=""> <span>100</span>
                                       </div>
                                       <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                          <p class="group inner list-group-item-text">Birdy's</p>
                                          <p><img src="assets/images/services/icongift.png" alt=""> <span> Malad</span></p>
                                       </div>
                                   </div>

                              </div>
                           </div>
                       </div>


                          <div class="item col-md-4 col-xs-6">
                           <div class="thumbnail">
                              <div class="col-md-10 col-xs-10">
                                  <p class="p_detail">get pastry  free on purchase of a cake</p>
                              </div>
                              <div class="col-md-2 col-xs-2">
                                 <p class="p_icon"><i class="glyphicon glyphicon-heart-empty"></i></p>
                              </div>
                             
                               <img class="group list-group-image img-responsive" src="assets/images/services/4.png"  alt="" />
                               <div class="caption">
                                 
                                   <div class="row">
                                       <div class="col-md-6 col-xs-6">
                                          <img src="assets/images/services/all.png" alt="">
                                       </div>
                                       <div class="col-md-6 col-xs-6">
                                           <img src="assets/images/services/iconall.png" alt=""> <span>100</span>
                                       </div>
                                       <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                          <p class="group inner list-group-item-text">Birdy's</p>
                                          <p><img src="assets/images/services/icongift.png" alt=""> <span> Malad</span></p>
                                       </div>
                                   </div>

                              </div>
                           </div>
                       </div>

                         <div class="item col-md-4 col-xs-6">
                           <div class="thumbnail">
                              <div class="col-md-10 col-xs-10">
                                  <p class="p_detail">get pastry  free on purchase of a cake</p>
                              </div>
                              <div class="col-md-2 col-xs-2">
                                 <p class="p_icon"><i class="glyphicon glyphicon-heart-empty"></i></p>
                              </div>
                             
                               <img class="group list-group-image img-responsive" src="assets/images/services/5.png" alt="" />
                               <div class="caption">
                                 
                                   <div class="row">
                                       <div class="col-md-6 col-xs-6">
                                          <img src="assets/images/services/all.png" alt="">
                                       </div>
                                       <div class="col-md-6 col-xs-6">
                                           <img src="assets/images/services/iconall.png" alt=""> <span>100</span>
                                       </div>
                                       <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                          <p class="group inner list-group-item-text">Birdy's</p>
                                          <p><img src="assets/images/services/icongift.png" alt=""> <span> Malad</span></p>
                                       </div>
                                   </div>

                              </div>
                           </div>
                       </div>


                         <div class="item col-md-4 col-xs-6">
                           <div class="thumbnail">
                              <div class="col-md-10 col-xs-10">
                                  <p class="p_detail">get pastry  free on purchase of a cake</p>
                              </div>
                              <div class="col-md-2 col-xs-2">
                                 <p class="p_icon"><i class="glyphicon glyphicon-heart-empty"></i></p>
                              </div>
                             
                               <img class="group list-group-image img-responsive" src="assets/images/services/6.png"  alt="" />
                               <div class="caption">
                                 
                                   <div class="row">
                                       <div class="col-md-6 col-xs-6">
                                          <img src="assets/images/services/all.png" alt="">
                                       </div>
                                       <div class="col-md-6 col-xs-6">
                                           <img src="assets/images/services/iconall.png" alt=""> <span>100</span>
                                       </div>
                                       <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                          <p class="group inner list-group-item-text">Birdy's</p>
                                          <p><img src="assets/images/services/icongift.png" alt=""> <span> Malad</span></p>
                                       </div>
                                   </div>

                              </div>
                           </div>
                       </div>

                        <div class="clearfix"></div>
                          <div class="item col-md-4 col-xs-6">
                           <div class="thumbnail">
                              <div class="col-md-10 col-xs-10">
                                  <p class="p_detail">get pastry  free on purchase of a cake</p>
                              </div>
                              <div class="col-md-2 col-xs-2">
                                 <p class="p_icon"><i class="glyphicon glyphicon-heart-empty"></i></p>
                              </div>
                             
                               <img class="group list-group-image img-responsive" src="assets/images/services/7.png" class="img-responsive" alt="" />
                               <div class="caption">
                                 
                                   <div class="row">
                                       <div class="col-md-6 col-xs-6">
                                          <img src="assets/images/services/all.png" alt="">
                                       </div>
                                       <div class="col-md-6 col-xs-6">
                                           <img src="assets/images/services/iconall.png" alt=""> <span>100</span>
                                       </div>
                                       <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                          <p class="group inner list-group-item-text">Birdy's</p>
                                          <p><img src="assets/images/services/icongift.png" alt=""> <span> Malad</span></p>
                                       </div>
                                   </div>

                              </div>
                           </div>
                       </div>


                          <div class="item col-md-4 col-xs-6">
                           <div class="thumbnail">
                              <div class="col-md-10 col-xs-10">
                                  <p class="p_detail">get pastry  free on purchase of a cake</p>
                              </div>
                              <div class="col-md-2 col-xs-2">
                                 <p class="p_icon"><i class="glyphicon glyphicon-heart-empty"></i></p>
                              </div>
                             
                               <img class="group list-group-image img-responsive" src="assets/images/services/image1.png" alt="" />
                               <div class="caption">
                                 
                                   <div class="row">
                                       <div class="col-md-6 col-xs-6">
                                          <img src="assets/images/services/all.png" alt="">
                                       </div>
                                       <div class="col-md-6 col-xs-6">
                                           <img src="assets/images/services/iconall.png" alt=""> <span>100</span>
                                       </div>
                                       <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                          <p class="group inner list-group-item-text">Birdy's</p>
                                          <p><img src="assets/images/services/icongift.png" alt=""> <span> Malad</span></p>
                                       </div>
                                   </div>

                              </div>
                           </div>
                       </div>

                         <div class="item col-md-4 col-xs-6">
                           <div class="thumbnail">
                              <div class="col-md-10 col-xs-10">
                                  <p class="p_detail">get pastry  free on purchase of a cake</p>
                              </div>
                              <div class="col-md-2 col-xs-2">
                                 <p class="p_icon"><i class="glyphicon glyphicon-heart-empty"></i></p>
                              </div>
                             
                               <img class="group list-group-image img-responsive" src="assets/images/services/image1.png"  alt="" />
                               <div class="caption">
                                 
                                   <div class="row">
                                       <div class="col-md-6 col-xs-6">
                                          <img src="assets/images/services/all.png" alt="">
                                       </div>
                                       <div class="col-md-6 col-xs-6">
                                           <img src="assets/images/services/iconall.png" alt=""> <span>100</span>
                                       </div>
                                       <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                          <p class="group inner list-group-item-text">Birdy's</p>
                                          <p><img src="assets/images/services/icongift.png" alt=""> <span> Malad</span></p>
                                       </div>
                                   </div>

                              </div>
                           </div>
                       </div>
                      

                   </div>





                  </div>
                
            <div class="margin-top-30 clearfix"></div>
               </div>


            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="footer-section">
         <div class="container">
            <div class="row no-disp">
               <div class="col-md-4">
                  <div class="footer_list">
                     <ul class="footer-list">
                        <li class="footer_heading">MOBILE RECHARGE</li>
                        <li><a href="#">Airtel (Plans)</a></li>
                        <li><a href="#">Aircel (Plans)</a></li>
                        <li><a href="#">Vodafone (Plans)</a></li>
                        <li><a href="#">BSNL (Plans)</a></li>
                        <li><a href="#">Tata Docomo GSM (Plans)</a></li>
                        <li><a href="#">Idea (Plans)</a></li>
                        <li><a href="#">Indicom Walky (Plans)</a></li>
                        <li><a href="#">MTNL Delhi (Plans)</a></li>
                        <li><a href="#">Reliance CDMA (Plans)</a></li>
                        <li><a href="#">Reliance GSM (Plans)</a></li>
                        <li><a href="#">Tata Indicom (Plans)</a></li>
                        <li><a href="#">Uninor (Plans)</a></li>
                        <li><a href="#">MTS (Plans)</a></li>
                        <li><a href="#">Videocon (Plans)</a></li>
                        <li><a href="#">Virgin CDMA (Plans)</a></li>
                        <li><a href="#">Virgin GSM (Plans)</a></li>
                        <li><a href="#">Tata Docomo CDMA (Plans)</a></li>
                     </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">SUPPORT</li>
                        <li class="no-marg">For help, send email to<br><a href='#'>listen@pay1.in</a></li>
                     </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">SECURED</li>
                        <li class="no-marg"><img src="assets/images/footer/secured_footer.png" alt=""></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="footer_list">
                     <ul class="footer-list">
                        <li class="footer_heading">DATA CARD RECHARGE</li>
                        <li><a href="#">Tata Photon</a></li>
                        <li><a href="#">MTS MBlaze</a></li>
                        <li><a href="#">MTS MBrowse</a></li>
                        <li><a href="#">Reliance NetConnect</a></li>
                        <li><a href="#">Airtel</a></li>
                        <li><a href="#">BSNL</a></li>
                        <li><a href="#">Aircel</a></li>
                        <li><a href="#">MTNL Delhi</a></li>
                        <li><a href="#">Vodafone</a></li>
                        <li><a href="#">Idea</a></li>
                        <li><a href="#">Reliance-GSM</a></li>
                        <li><a href="#">MTNL Mumbai</a></li>
                        <li><a href="#">T24</a></li>
                        <li><a href="#">Tata Docomo</a></li>
                     </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">DTH (TV) RECHARGE</li>
                        <li><a href="#">Airtel Digital TV</a></li>
                        <li><a href="#">Reliance DIgital TV</a></li>
                        <li><a href="#">Dish TV</a></li>
                        <li><a href="#">Tata Sky</a></li>
                        <li><a href="#">Sun Direct</a></li>
                        <li><a href="#">Videocon D2H</a></li>
                     </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">SECURED</li>
                        <li><a href="#">My Gifts</a></li>
                        <li><a href="#">Gift Coins</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="footer_list">
                     <ul class="footer-list">
                        <li class="footer_heading">POSTPAID</li>
                        <li><a href="#">Airtel Bill Payment</a></li>
                        <li><a href="#">BSNL Bill Payment</a></li>
                        <li><a href="#">Tata Docomo GSM Bill Payment</a></li>
                        <li><a href="#">Tata Docomo CDMA Bill Payment</a></li>
                        <li><a href="#">Idea Bill Payment</a></li>
                        <li><a href="#">Vodafone Bill Payment</a></li>
                        <li><a href="#">Reliance GSM Bill Payment</a></li>
                        <li><a href="#">Reliance CDMA Bill Payment</a></li>
                     </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">PAY1</li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Support</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Sitemap</a></li>
                        <li><a href="#">T&amp;C </a></li>
                        <li><a href="#">Blog </a></li>
                        <li><a href="#">Security Policy </a></li>
                     </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">APP DOWNLOAD</li>
                        <li><a href="#">Android App</a></li>
                        <li><a href="#">Windows</a></li>
                        <li><a href="#">iOS</a></li>
                     </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">CONNECT US ON</li>
                        <li class="social"><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li class="social"><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="social"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="margin-top-30"></div>
         <div class="margin-top-30"></div>
         <div class="copyright">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <p><span class="glyphicon glyphicon-copyright-mark"></span>Mindsarray Technologies Pvt. Ltd. All Righrs Reserved.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" role="dialog" id="failure">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="cls" data-dismiss="modal">&times;</button>
                  <h3>Payment Failure</h3>
               </div>
               <div class="modal-body">
                  <p>Sorry! Your previous payment attempt failed.</p>
                  <div class="pull-right">
                     <button class="modal_button" id="try-again">Try Again</button>
                     <button class="modal_button" id="cancel-button" data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/bootstrap.min.js"></script>
      <script src="assets/js/payment.js"></script>
       <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
      <script type="text/javascript" src="assets/js/jssor.slider.mini.js"></script>
      <script src="assets/js/slider.js"></script>

      <script src="assets/js/jquery-scrollToTop.js"></script>
      <script type="text/javascript">
         $(document).ready(function($) {
             $('body').scrollToTop({skin: 'cycle'});
         });
      </script>
        <script type="text/javascript" src="assets/js/gift_custom.js"> </script>

      
   </body>
  
</html>