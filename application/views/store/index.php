<?php $this->load->view('common/header'); ?>
<?php echo $this->load->view('common/gift_slider'); ?>
<br>
<br>
<div class="col-lg-6 col-lg-offset-3">
    <div class="input-group">
        <input type="text" class="form-control input-sm" placeholder="Search by typing the product or brand name">
        <span class="input-group-btn">
            <button class="btn btn-primary btn-sm" type="button">Search</button>
        </span>
    </div>
</div>

<div class="col-lg-10 col-lg-offset-1" id="gift_store">

    <div class="row top30" id="categoryList">
        <div class="row">
            <div class="col-lg-12 text-center"><h1>Recommended</h1></div>
            <div class="col-lg-10 col-lg-offset-1">
                <div align="right"><a href="/categories/Recommended">See All <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></div>
                <ul class="bxslidercat_store">
                    <li><img class="sliderimages" src="http://placehold.it/250x150" alt="Image" style="max-width:100%;"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/250x150" alt="Image" style="max-width:100%;"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/250x150" alt="Image" style="max-width:100%;"><p class="text-center">Hello</p></li>
                </ul>
            </div>
        </div>
    </div>

    <div id="gift_types">            
        <hr/>
        <div class="row top30">
            <div class="row">
                <div class="col-lg-12 text-center"><h1>Popular</h1></div>
                <div class="col-lg-10 col-lg-offset-1">
                    <div align="right"><a href="/categories/Popular">See All <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></div>
                    <ul class="bxslider">
                        <li><img class="sliderimages" src="http://placehold.it/150x150" alt="Image" style="max-width:100%;"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/150x150" alt="Image" style="max-width:100%;"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/150x150" alt="Image" style="max-width:100%;"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages"  src="http://placehold.it/150x150" alt="Image" style="max-width:100%;"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/150x150" alt="Image" style="max-width:100%;"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/150x150" alt="Image" style="max-width:100%;"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/150x150" alt="Image" style="max-width:100%;"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/150x150" alt="Image" style="max-width:100%;"><p class="text-center">Hello</p></li>
                    </ul>
                </div>
            </div>
        </div>
        <hr/>


        <div class="row top30">
            <div class="row">                
                <div class="col-lg-12 text-center"><h1>Featured</h1></div>
                <div class="col-lg-10 col-lg-offset-1">
                    <div align="right"><a href="/store/featuredoffer">See All <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></div>
                    <ul class="bxslidercat_store">
                        <li><img  class="sliderimages"  src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img  class="sliderimages" src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img  class="sliderimages" src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages"  src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages"  src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img  class="sliderimages" src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img  class="sliderimages" src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img  class="sliderimages" src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages"  src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img  class="sliderimages" src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                        <li><img class="sliderimages" src="http://placehold.it/250x150" alt="Image"><p class="text-center">Hello</p></li>
                    </ul>
                </div>                
            </div>
        </div>
    </div>
</div>                


<?php $this->load->view('common/footer'); ?>
