<?php echo $this->load->view('common/header'); ?>

<!-- Write logic to load slider based on category in URL-->
<?php echo $this->load->view('categories/categoryslider'); ?>

<!--<script>
    $(document).ready(function() {
        $('.addtofavdiv img').click(function(e) {
            var _this = $(this);
            var current = _this.attr("src");
            var swap = _this.attr("data-swap");
            var isfav = _this.attr("value");
            _this.attr('src', swap).attr("data-swap", current);
            if (isfav == 0) {
                $(this).attr('value', '1');
                //--api to add in favourite
            } else {
                $(this).attr('value', '0');
                //--api to remove in favourite
            }
        });
    });
</script>-->
<div class="row">
    <div class="col-lg-10 col-lg-offset-1">



        <div class="row">
            <div class="col-lg-10 col-lg-offset-2">

                <div align="center" class="col-lg-4 col-lg-offset-4">

                    <div class="form-group">

                        <div class="input-group">
                            <input type="text" class="form-control input-sm" placeholder="Search by name"  name="searchbyname" />
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-sm" type="button">Search!</button>
                            </span>
                        </div>

                    </div>

                </div>

            </div>
        </div>


        <div class="row">

            <div class="col-lg-12">


                <div class="col-lg-3">
                    <div  class="list-group panel">
                        
                          <?php  if(!empty($sidebarCategories)):  ?>
                                        
                                    
                                        
                                        <?php foreach($sidebarCategories->gift as $key=>$val): ?>
                                        
                                              <?php $class=$val->url==$this->uri->segment(2)?"addclassunderline":"";  ?>
                        
                                            <a  class="list-group-item list-group-item <?php echo  $class; ?>" href="/store/<?php echo $val->url; ?>/<?php echo $val->id; ?>"><b><?php echo $val->name; ?></b></a>
                                                
                                        <?php endforeach; ?>
                                            
                                            <a class="list-group-item list-group-item"  href="/store/nearyou"><b>Near You</b></a>
                                            
                              
                                            <a  style="cursor:pointer;" onclick="javascript:showOverlay();"   class="list-group-item list-group-item" ><b>Categories</b><i class="glyphicon glyphicon-chevron-down arrowDown" ></i></a>
                                                 
                                                 

                                                           <?php foreach($sidebarCategories->categories as $key=>$val): ?>
                                                                
                                                 
                                                                <?php  if($val->url==$this->uri->segment(2)):  ?>
                                                            
                                                                   <a  class="sidebarselectedcat list-group-item list-group-item " href="/categories/<?php echo $val->url; ?>/<?php echo $val->id; ?>"><b><?php echo $val->name; ?></b></a>

                                                                <?php endif; ?> 
                                                                  
                                                          <?php endforeach; ?>

                                                   
                                        
                                  
                                        
                                        <?php endif;  ?>
 


                    </div>

                     <div class="categoryOverlaydiv" style="display: none;">
                                    <div class="categoryOverlay"></div>
                                    <div class="categoryOverlayText">
                                        <?php  if(!empty($sidebarCategories)):  ?>
                                        
                                        <ul>
                                        
                                        <?php foreach($sidebarCategories->gift as $key=>$val): ?>
                                        
                                              <?php $class=$val->url==$this->uri->segment(2)?"addclassunderline":"";  ?>
                                            <li><a class="<?php echo  $class; ?>"  href="/store/<?php echo $val->url; ?>/<?php echo $val->id; ?>"><?php echo $val->name; ?></a></li>
                                                
                                        <?php endforeach; ?>
                                            
                                            <li><a href="/store/nearyou">Near You</a></li>
                                            
                                             <li>
                                                 <a href="/store">Categories</a>
                                                 
                                                        <ul>

                                                           <?php foreach($sidebarCategories->categories as $key=>$val): ?>
                                                                
                                                                <?php $class=$val->url==$this->uri->segment(2)?"addclassunderline":"";  ?>
                                                            
                                                                  <li><a  class="<?php echo  $class; ?>" href="/categories/<?php echo $val->url; ?>/<?php echo $val->id; ?>"><?php echo $val->name; ?></a></li>

                                                          <?php endforeach; ?>

                                                       </ul>
                                                 
                                                 
                                             </li>
                                        
                                        </ul>
                                        
                                        <?php endif;  ?>
                                    </div>
                            </div>

                    <div>
                        <h3>Gift Coins</h3>
                        <div>
                            <input id="ex2" type="text" class="span2 giftcoins" value="" data-slider-min="0" data-slider-max="1000" data-slider-step="2" data-slider-value="[0,200]"/>
                        </div>
                    </div>     
                </div>



                <div class="col-lg-9">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-3 col-sm-3 pull-left">
                                <h1>Top Offer</h1>
                            </div>
                        </div>
                    </div>

                    <div id="" class="">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 dealslisting gift_types">
                                
                                <?php foreach($response->description as $deal): ?>
                
                                    <div data-gift-points="<?php echo $deal->min_amount; ?>" class="col-lg-3 col-sm-3  text-center nearbylisting">
                                        <div class="thumbnail tileheight">
                                                <div class="addtofavdiv"><img  style="cursor: pointer" value="<?php echo $deal->id; ?>"  data-swap="/images/addtofavorities_red.png" src="/images/addtofavorities.png"></div>
                                                <div><img width="150px" height="100px" src="<?php echo $deal->img_url; ?>"></div>
                                                <div class="card_infolabel pull-right showpoints">
                                                    <img src="/images/gift_ic.png">
                                                    <span class="card_infolabel"><?php echo $deal->min_amount; ?></span>
                                                </div>
                                                <div class="caption prod-caption top15">
                                                    <a href="/gifts/detail/<?php echo $deal->id; ?>"><h5><?php echo $deal->deal; ?></h5></a>
                                                    <span class="listingspan"><?php echo $deal->offer; ?></span>
                                                </div>
                                        </div>
                                    </div>
                                    
                                <?php endforeach;  ?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php echo $this->load->view('common/footer'); ?>