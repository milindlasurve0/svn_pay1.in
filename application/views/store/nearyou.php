<?php echo $this->load->view('common/header'); ?>


<?php echo $this->load->view('store/maps'); ?>

<div class="row" style="margin-top: 35px;">
    <div class="col-lg-10 col-lg-offset-1">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-2">

                <div align="center" class="col-lg-4 col-lg-offset-4">

                    <div class="form-group">

                        <div class="input-group">
                            <input type="text" class="form-control input-sm" placeholder="Search by name"  name="searchbyname" />
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-sm" type="button">Search!</button>
                            </span>
                        </div>

                    </div>

                </div>

            </div>
        </div>


        <div class="row">

            <div class="col-lg-12">


                <div class="col-lg-3">
                  
                      <?php  if(!empty($sidebarCategories)):  ?>
                                        
                                    
                                        
                                        <?php foreach($sidebarCategories->gift as $key=>$val): ?>
                                        
                                              <?php $class=$val->url==$this->uri->segment(2)?"addclassunderline":"";  ?>
                        
                                            <a  class="list-group-item list-group-item <?php echo  $class; ?>" href="/store/<?php echo $val->url; ?>/<?php echo $val->id; ?>"><b><?php echo $val->name; ?></b></a>
                                                
                                        <?php endforeach; ?>
                                            
                                            <a class="list-group-item list-group-item addclassunderline"  href="/store/nearyou"><b>Near You</b></a>
                                            
                              
                                            <a  style="cursor:pointer;" onclick="javascript:showOverlay();"   class="list-group-item list-group-item" ><b>Categories</b><i class="glyphicon glyphicon-chevron-down arrowDown" ></i></a>
                                                 
                                                 

                                                           <?php foreach($sidebarCategories->categories as $key=>$val): ?>
                                                                
                                                 
                                                                <?php  if($val->url==$this->uri->segment(2)):  ?>
                                                            
                                                                   <a  class="sidebarselectedcat list-group-item list-group-item " href="/categories/<?php echo $val->url; ?>/<?php echo $val->id; ?>"><b><?php echo $val->name; ?></b></a>

                                                                <?php endif; ?> 
                                                                  
                                                          <?php endforeach; ?>

                                                   
                                        
                                  
                                        
                                        <?php endif;  ?>
                        
               

                    <div class="categoryOverlaydiv" style="display: none;">
                                    <div class="categoryOverlay"></div>
                                    <div class="categoryOverlayText">
                                        <?php  if(!empty($sidebarCategories)):  ?>
                                        
                                        <ul>
                                        
                                        <?php foreach($sidebarCategories->gift as $key=>$val): ?>
                                        
                                              <?php $class=$val->url==$this->uri->segment(2)?"addclassunderline":"";  ?>
                                            <li><a class="<?php $class;?>"  href="/store/<?php echo $val->url; ?>/<?php echo $val->id; ?>"><?php echo $val->name; ?></a></li>
                                                
                                        <?php endforeach; ?>
                                            
                                            <li><a class="addclassunderline" href="/store/nearyou">Near You</a></li>
                                            
                                             <li>
                                                 <a href="/store">Categories</a>
                                                 
                                                        <ul>

                                                           <?php foreach($sidebarCategories->categories as $key=>$val): ?>
                                                                
                                                                <?php $class=$val->url==$this->uri->segment(2)?"addclassunderline":"";  ?>
                                                            
                                                                  <li><a  class="<?php echo  $class; ?>" href="/categories/<?php echo $val->url; ?>/<?php echo $val->id; ?>"><?php echo $val->name; ?></a></li>

                                                          <?php endforeach; ?>

                                                       </ul>
                                                 
                                                 
                                             </li>
                                        
                                        </ul>
                                        
                                        <?php endif;  ?>
                                    </div>
                            </div>
                        
                        
                         <div>
                        <h3>Gift Coins</h3>
                        <div>
                            <input id="ex2" type="text" class="span2 giftcoins" value="" data-slider-min="0" data-slider-max="1000" data-slider-step="2" data-slider-value="[0,200]"/>
                        </div>
                    </div>    
 
                </div>

                <div class="col-lg-9">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-3 col-sm-3 pull-left">
                                <h1>Near You</h1>
                            </div>
                        </div>
                    </div>

                         <div class="row">
                             <div class="col-lg-12 col-md-12 col-sm-12 gift_types dealslisting"  id="nearbyme" style="text-align: center;" >
                        
                            </div>
                        </div>
                        
                </div>
            </div>
        </div>

    </div>
</div>

<?php echo $this->load->view('common/footer'); ?>