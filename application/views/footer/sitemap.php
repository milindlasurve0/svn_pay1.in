<?php $this->load->view('common/doctype_html'); 
    $current_page_url =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $meteTagData = getMetaTagDetails();
    foreach ($meteTagData as $row){
      
      if($row->page_url == $current_page_url){
        $new_page_tittle =  $row->page_tittle;
        $new_page_meta_description =  $row->page_meta_description;  
        $new_page_meta_tag =  $row->page_meta_tag;  
      }
      
    }
?>
<meta name="description" content="<?php if($new_page_meta_description){ echo $new_page_meta_description; }else{ ?>Having difficulty to find a page on Pay1? Why not consult the sitemap, it will give you the whole list of pages.  <?php } ?> ">
<meta name="keywords" content="<?php if($new_page_meta_tag){ echo $new_page_meta_tag; }else{ ?>Sitemap , Locate the page you desire , Pay1 <?php } ?>" />
<title><?php if($new_page_tittle){ echo $new_page_tittle; }else{ ?> Sitemap | Locate the page you desire | Pay1  <?php } ?></title>
<?php $this->load->view('common/header'); 
      $isLoggedIn = $this->session->userdata('isLoggedIn'); ?>

    <link rel="stylesheet" href="/assets/css/style.css">
      <div class="clearfix" style="clear:both;"></div>
      <!-- ======================== Header Image ========================== -->
      <input type="hidden" id="sitemap_img_hdn" value="/assets/images/background/sitemap.jpg">
      <div class="career_page1 margin-top-50">
         <img src="/assets/images/background/sitemap.jpg" class="img-responsive" alt="" id="sitemap_img">
      </div>
      <!-- ======================== Header Image END========================== -->
      <div class="container-fluid">
         <div class="container border-both">
            <div class="row">
               <div class="col-md-3 col-sm-3 ">
                  <div class=''>
                     <div id="contact_navigation">
                        <ul class="tabs">
                           <li><a href="/about-us">About us</a></li>
                           <li><a href="/contact-us">Contact Us</a></li>
                           <li><a href="/terms-and-conditions">Terms & Conditions</a></li>
                           <!--<li><a href="#security" data-toggle="tab">Security Policy</a></li>-->
                           <li><a href="/faqs">FAQs</a></li>
                           <li><a href="/privacy-policy">Privacy Policy</a></li>
                           <li><a href="/careers">Careers</a></li>
                           <li><a target="_blank" href="/partners-blog/" >Blog</a></li>
                           <li class="active" ><a href="#sitemap" data-toggle="tab">Sitemap</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-sm-9">
                  <div class='padding-both margin-top-30'>
                     <h1><b>Sitemap</b></h1>
                                  
                        
                   <!--    <div class="sitemap-section">-->
         <div class="container">
            <div class="row no-disp">
               <div class="col-md-3">
                  <div class="sitemap_list">
                     <ul class="sitemap-list">
                        <li class="sitemap_heading">Recharge/Pay Bills</li>
                        <li><a href="/home">Prepaid Mobile</a></li>
                        <li><a href="/home">Postpaid Mobile</a></li>
                        <li><a href="/home">DTH</a></li>
                        <li><a href="/home">Datacard</a></li>
                        <li><a href="/home">Quick Recharge</a></li>
                        <li><a href="/home">Add Money</a></li>
                        <li><a href="/home">Locate Pay1 Store</a></li>
                        <li><a href="/home">Online Payment</a></li>
                        <li><a href="/gift-store">Redeem Voucher</a></li>
                     </ul>
                     <ul class="sitemap-list">
                        <li class="sitemap_heading">View Profile</li>
                       <li><a href="/my-likes">My Likes</a></li>
                        <li><a href="/my-gifts">My Gifts</a></li>
                        <li><a href="/profile/index">Transaction History</a></li>
                        <li><a href="/profile/index">Wallet History</a></li>
                        <li><a href="/profile/index">Gift Coin History</a></li>
                     </ul>
                     </div>
               </div>
               <div class="col-md-3">
                  <div class="sitemap_list">
                     <ul class="sitemap-list">
                        <li class="sitemap_heading">Gift Store</li>
                         <?php foreach($sidebarCategories->gift as $row): ?>
                            <li  ><a href="<?php echo 'categories-'.$row->url.'-cid-'.$row->id; ?>"><?php echo $row->name; ?></a></li>
                        <?php endforeach; ?>
                       
                        <!--<li><a href="#">Near You</a></li>-->
                        <!--<li><a href="/giftstore">Redeem Gift Coins</a></li>-->
                     </ul>
                     <ul class="sitemap-list">
                        <li class="sitemap_heading">Categories</li>
                        <?php foreach($sidebarCategories->categories as $row): ?>
                        <li><a href="<?php echo $row->url.'-cid-'.$row->id; ?>"><?php echo $row->name; ?></a></li>
                        <?php endforeach; ?>
                       
                     </ul>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="sitemap_list">
                     <ul class="sitemap-list">
                        <li class="sitemap_heading">Pay1</li>
                        <li><a href="/about-us">About Us</a></li>
                        <li><a href="/careers">Careers</a></li>
                        <li><a href="/faqs">FAQ</a></li>
                        <li><a href="/contact-us">Contact Us</a></li>
                        <li><a href="/privacy-policy">Privacy Policy</a></li>
                        <li><a href="/sitemap">Sitemap</a></li>
                        <li><a href="/terms-and-conditions">Terms & Condition</a></li>
                        <li><a target="_blank" href="/partners-blog/">Blog</a></li>
                        <!--<li><a href="#">Security Policy</a></li>-->
                     </ul>
                     <ul class="sitemap_list">
                        <li class="sitemap_heading">Mobile</li>
                        <li><a target="_blank" href="https://goo.gl/lqAaqU" >Android App</a></li>
                        <li><a target="_blank" href="http://goo.gl/XF3d12"  >Windows App</a></li>
                     </ul>
                     
                  </div>
               </div>
            </div>
         </div>


                  </div>
                 
                    <div class="margin-top-30 clearfix"></div>
               </div>


            </div>
         </div>
      </div>
      <div class="clearfix"></div>

      <div class="modal fade" role="dialog" id="failure">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="cls" data-dismiss="modal">&times;</button>
                  <h3>Payment Failure</h3>
               </div>
               <div class="modal-body">
                  <p>Sorry! Your previous payment attempt failed.</p>
                  <div class="pull-right">
                     <button class="modal_button" id="try-again">Try Again</button>
                     <button class="modal_button"id="cancel-button"data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript">
         $(document).ready(function(){
             $('[data-toggle="popover"]').popover(); 
              $('body').scrollToTop({skin: 'cycle'});
              <?php if($isLoggedIn != 1 ){ ?>
                localStorage.setItem("redeem", "1");
              <?php } ?> 
           $("#sitemap_img").attr("src",$("#sitemap_img_hdn").val());
         });
      </script>
      <script src="/assets/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
      <script src="/assets/js/jquery-scrollToTop.js"></script>
<?php $this->load->view('common/footer'); ?>

      
      