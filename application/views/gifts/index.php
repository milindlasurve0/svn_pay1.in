<?php $this->load->view('common/doctype_html');  ?>
<?php $this->load->view('common/header'); ?>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    var parliament = new google.maps.LatLng(<?php echo $response['description']['location_detail'][0]['lat'] ?>, <?php echo $response['description']['location_detail'][0]['lng'] ?>);
    var marker;
    
  function initialize() {
    var myOptions = {
      zoom: 12,
      center: parliament ,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"),
        myOptions);
    
     var marker = new google.maps.Marker({
      position: parliament, 
      map: map, 
      title:""
  });   
  }

    $(window).load(function(){
    initialize();
});
</script>
<div class="container-fluid">
    <div class="row offer_width" style="margin-left: 20px;">

        <div class="col-sm-3 col-md-3 col-xs-12">

            <div class="offer_bg">
                <div class="offer_bg_img"><img class="img-responsive" src="/images/offer_bgleft.jpg"></div>
                <div class="row"><div class="col-lg-6 col-lg-offset-3"> <div class="offer_logo"><img class="img-responsive" src="<?php echo $response['description']['offer']['logo_url']; ?>"> </div></div>
                </div>
            </div>

            <div class="offer_info_coin">
                <div class="row"><div><img src="/images/coins.png" class="divic"><?php echo $response['description']['offer']['min_amount']; ?></div></div>
                <div class="row"><div><img src="/images/offer_whtsappic.png" class="divic">4</div></div>
                <div class="row"><div><img src="/images/offer_callic.png" class="divic">02232226074</div></div>
            </div>

        </div>

        <div class="col-sm-9 col-md-9 col-xs-12">

            <div class="offer_banner_img"><img class="img-responsive" src="<?php echo $response['description']['offer']['L_img_url']; ?>"></div>
            <div class="offer_ban_likeic addtofavdiv1"><img style="cursor: pointer"  value="0"  data-swap="/images/addtofavorities_red.png" src="/images/like_baner.png"></div>

        </div>

    </div>


    <div class="row">

        <div class="row offer_width_tit" style="margin-left: 5%;">

            <div class="col-sm-8 col-md-8 col-xs-12">
                <h2><?php echo ucfirst($response['description']['offer']['deal']);  ?> - <img src="/images/Rupee-sign.png">&nbsp;&nbsp;<?php echo $response['description']['offer']['min_amount'];  ?></h2>
                <div class="row">
                    <p></p><h4 class="h4bold">Offer details:</h4>
                    <ul class="list-unstyled">
                       <?php if(!empty($response['description']['offer_details']['time'])): ?> <li class="bulletwatch"><?php echo $response['description']['offer_details']['time']; ?></li><?php endif; ?>
                         <?php if(!empty($response['description']['offer_details']['day'])): ?><li class="bulletcalender"><?php echo $response['description']['offer_details']['day']; ?></li><?php endif; ?>
                         <?php if(!empty($response['description']['offer_details']['appointment'])): ?><li class="bulletappointment"><?php echo $response['description']['offer_details']['appointment']; ?></li><?php endif; ?>
                        <?php if(!empty($response['description']['offer_details']['exclusive_gender'])): ?> <li class="bulletstar"><?php echo $response['description']['offer_details']['exclusive_gender']; ?></li><?php endif; ?>
                    </ul>

                 

                    <div class="row" style="margin-top: 156px;">
                        <h2>Similar Offers</h2>
                        <div class="col-xs-12 col-md-4 col-sm-4"><div class=" img-responsive"><img src="/images/offer_thumb1.png"></div></div>
                        <div class="col-xs-12 col-md-4 col-sm-4"><div class=" img-responsive"><img src="/images/offer_thumb2.png"></div></div>
                        <div class="col-xs-12 col-md-4 col-sm-4"><div class=" img-responsive"><img src="/images/offer_thumb3.png"></div></div>
                    </div>   

                </div>
            </div>

            <div class="col-sm-4 col-md-4 col-xs-12 space30top">
                <div class="row offer_rightpanel">
                    <div align="right">
                        <button class="btn btn-warning" id="redeem_gift" type="button">REDEEM</button></div>
                </div>

                <div class="row">
                    <h4 class="h4bold">Address:</h4>
                    <?php
                       $loc = "<ul style='list-style:none;margin-left: -39px;'>";
                    foreach ($response['description']['location_detail'] as $cnt => $location) {
                       
                        $loc .= "<li>";
                        $loc .= $location['address'] . ", ";
                        $loc .= isset($location['area']) ? $location['area'] . ", " : "";
                        $loc .= isset($location['city']) ? $location['city'] . ", " : "";
                        $loc .= isset($location['state']) ? $location['state'] . ", " : "";
                        $loc .= "</li>";
                        
                      
                    }
                    $loc .= "</ul>";
                      echo $loc;
                    ?>

     <!--<p>3rd Floor, R City Mall, LBS Marg,<br> Ghatkopar West, Mumbai</p>-->   

                </div>
                <div class="row">
                    <h4 class="h4bold">Map:</h4>
                   <div id="map_canvas" style="width:325px; height:180px"></div>
                    
                </div>
                <div class="row">
                    <h4 class="h4bold">Ratings:</h4>
                    <div class="row">

                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <a onmouseover="MM_swapImage('Image5', '', '/images/5O.svg', 1)" onmouseout="MM_swapImgRestore()" class="rollover_linerm" href="#">
                                <div class="ratingbox">Love it<br><img id="Image5" name="Image5" src="/images/5.svg"><br>
                                    <span class="ratnum">5</span></div></a>
                        </div>


                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <a onmouseover="MM_swapImage('Image4', '', '/images/4O.svg', 1)" onmouseout="MM_swapImgRestore()" class="rollover_linerm" href="#">
                                <div class="ratingbox">Like it!<br><img id="Image4" name="Image4" src="/images/4.svg"><br>
                                    <span class="ratnum">4</span></div></a>
                        </div>


                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <a onmouseover="MM_swapImage('Image1', '', '/images/3O.svg', 1)" onmouseout="MM_swapImgRestore()" class="rollover_linerm" href="#">
                                <div class="ratingbox">Just okay! <br><img id="Image1" name="Image1" src="/images/3.svg"><br>
                                    <span class="ratnum">3</span></div></a>
                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <a onmouseover="MM_swapImage('Image2', '', '/images/2O.svg', 1)" onmouseout="MM_swapImgRestore()" class="rollover_linerm" href="#">
                                <div class="ratingbox">Poor!<br><br><img id="Image2" name="Image2" src="/images/2.svg"><br>
                                    <span class="ratnum">2</span></div></a>
                        </div>


                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <a onmouseover="MM_swapImage('Image3', '', '/images/1O.svg', 1)" onmouseout="MM_swapImgRestore()" class="rollover_linerm" href="#">
                                <div class="ratingbox">It's bad!<br><img id="Image3" name="Image3" src="/images/1.svg"><br>
                                    <span class="ratnum">1</span></div></a>
                        </div>



                    </div>

                </div>

            </div>

            <div class="row scrollalign">
                <div class="col-xs-12 col-md-4 col-sm-4" style=" margin-top: 70px;">
                    <h4 class="h4bold">Terms &amp; Conditions</h4>
                    <div class="" id="content-3" style="width: 100%; height: 180px; padding: 0px; margin: 0px; position: relative; overflow: visible;">
                                <ol class="">
                                    <li>Coupons Should be produced before placing the order.</li>
                                    <li>Taxes as applicable.</li>
                                    <li>This offer cannot be clubbed with any othergoing offer.</li>
                                    <li>Offer cannot be redeemed for cash.</li>
                                    <li>Only one coupon per table/order.</li>
                                    <li>Offer valid from Monday to Friday.</li>
                                </ol>
                            </div></div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php $this->load->view('common/footer'); ?>