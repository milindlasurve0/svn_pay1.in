<?php $gifttype = $this->config->item($id,'GiftTypes'); 
        
        $current_page_url =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $meteTagData = getMetaTagDetails();

        foreach ($meteTagData as $row){

            if($row->page_url == $current_page_url){
              $new_page_tittle =  $row->page_tittle;
              $new_page_meta_description =  $row->page_meta_description;  
              $new_page_meta_tag =  $row->page_meta_tag;  
            }

        }
?>
<?php $this->load->view('common/doctype_html');  ?>
<meta name="description" content="<?php if($new_page_meta_description){ echo $new_page_meta_description; }else{ echo $gifttype['description']; } ?>">
<meta name="keywords" content="<?php if($new_page_meta_tag){ echo $new_page_meta_tag; }else{ echo $gifttype['title']; ?> , My Gifts , Pay1  <?php } ?>" />
<title><?php if($new_page_tittle){ echo $new_page_tittle; }else{ echo $gifttype['title']; ?> | My Gifts | Pay1   <?php } ?></title>
<?php $this->load->view('common/header');  
    
    $domain_url = CDEV_URL;
    $imageData = getImageDetails();    
    
    foreach ($imageData as $row){
        if($row->image_flag == 2){
            
        switch ($row->gift_type) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                $new_img_url = $domain_url.$row->image_url;   
                break;
            default:
                $new_img_url = $domain_url.$row->image_url;   
            }   
        }
    }
    

    switch ($id) {
        case 1:
            $img_url = '/assets/images/background/Featured-Offer.jpg';
            break;
        case 2:
            $img_url = '/assets/images/background/Top-Offer.jpg';
            break;
        case 3:
            $img_url = '/assets/images/background/Popular.jpg';
            break;
        case 4:
            $img_url = '/assets/images/background/Recommended.jpg';
            break;
        case 5:
            $img_url = '/assets/images/background/offer-of-the-day.jpg';
            break;
        default:
             $img_url = '/assets/images/background/Gift-Store.jpg';
    }
?>
<style type="text/css">
    .autocomplete1-suggestions {border: 1px solid #999; background: #fff; cursor: default; overflow: auto; }
    .autocomplete1-suggestion { padding: 10px 5px; font-size: 1.0em; white-space: nowrap; overflow: hidden; }
    .autocomplete1-selected { background: #f0f0f0; }
    .autocomplete1-suggestions strong { font-weight: normal; color: #3399ff; }

    .autocomplete-suggestions {border: 1px solid #999; background: #fff; cursor: default; overflow: auto; }
    .autocomplete-suggestion { padding: 10px 5px; font-size: 1.0em; white-space: nowrap; overflow: hidden; }
    .autocomplete-selected { background: #f0f0f0; }
    .autocomplete-suggestions strong { font-weight: normal; color: #3399ff; }
</style>


      <link rel="stylesheet" href="/assets/css/style.css">
     
      <div class="clearfix" style="clear:both;"></div>

     <!-- =========================  Gift Types Header Image     ========================= -->
     <input type="hidden" id="gifttypes_img_hdn" value="<?php if(isset($new_img_url)){  echo $new_img_url; }else{ echo $img_url;}  ?>">
      <div class="service_page1">
          <img src="/assets/images/default_img/pay1_loader.jpg" id="gifttypes_img" alt="" class="img-responsive">
      </div>
      <div class="bottom_bg" style=""></div>
      <!-- ======================== Header Image end========================== -->
       <div class="clearfix"></div>
     

      <div class="container-fluid">
         <div class="container border-both">
            <div class="row">
               <div class="col-md-12 text-center margin-top-25">
                  <div class="row">
                 <form action=" " >
                        <div class="col-md-4 col-md-offset-4">
                           <input type="text" id ="search_brand" name="search_brand" class="form-control autocomplete1" placeholder="Search by typing the product or brand name...">
                           <!--<input type="text" id ="search_brand" name="search_brand" class="form-control " placeholder="Search by typing the product or brand name...">-->
                        </div>
                        <div class="col-md-1">
                            <!--<input type="submit" value="SEARCH" class="btn btn-save search-btn">-->
                            <button type="button" class="btn btn-save search-btn" onclick="search_brands()" style="border-radius:5px" >SEARCH</button>
                         </div>
                      </form>
               </div>
            </div>
               <div class="col-md-3 margin-top-30 ">
                  <div id="service_navigation" class="margin-top-50">
                    

                  <div id="accordian" class="border-right">
                     <ul>
                         <?php foreach($sidebarCategories->gift as $row): ?>
                            <li <?php if($row->id== $id){ ?> class="active" 
                            <?php } ?> ><a href="<?php echo 'categories-'.$row->url.'-cid-'.$row->id; ?>"><?php echo $row->name; ?></a></li>
                            <?php if($id == $row->id){ $gift_name = $row->name; }
                         endforeach; ?>
                        
                        <!--<li><a href="/giftstore/near_you/" >Near you</a></li>-->    
                        <li><a href="javascript:window.location.href='/near-you/'+lat_lng" >Near you</a></li>
                         

                         
                        <li>
                           <p class="mslide" style="cursor:pointer">Categories <i class="glyphicon glyphicon-chevron-down sicon"></i></p>
                           <ul>
                               <?php foreach($sidebarCategories->categories as $row): ?>
                               <li><a href="<?php echo $row->url.'-cid-'.$row->id; ?>"><?php echo $row->name; ?></a></li>
                               <?php endforeach; ?>
                               
<!--                              <li><a href="/giftcategory/service">Service</a></li>
                              <li><a href="#">E-voucher</a></li>
                              <li><a href="#">Beauty</a></li>-->
                           </ul>
                        </li>
                      
                        <li>
                          <p class="mslide">Locations <i class="glyphicon glyphicon-chevron-down sicon"></i></p>
                            <ul>
                             <input type="text" id ="search_location" class="form-control autocomplete" name="search_location" style="width:150px;" onblur="search_location(this.value)" placeholder="Area Name" >
                             <!--<input type="text" id ="search_location" class="form-control " name="search_location" style="width:150px;" onblur="search_location(this.value)" placeholder="Area Name" >-->
                            </ul>
                        </li>                 
                       
                     </ul>
                      <!--<span id="gift_coin"></span>-->
                      
                     <div class='col-md-11 clearfix'><h3 class="gcoin">Gift Coins</h3></div>
                     <div class="row">
                         <div class="col-md-3 col-xs-2"><button class="range_btn" data-change="-10"><img src="/assets/images/icons/minus.png" alt=""  onclick="gift_filter_button('minus','<?php echo $id; ?>','gift')"></button></div>
                          <div class="col-md-6 col-xs-4 row"><input id="slider1" type="range" min="10" max="500" step="10" class="" onchange="showVal(this.value,'<?php echo $id; ?>','gift')"/></div>
                           <div class="col-md-3 col-xs-2"> <button class="range_btn" data-change="10"><img src="/assets/images/icons/plus.png" alt=""  onclick="gift_filter_button('plus','<?php echo $id; ?>','gift')"></button></div>
                          <div class="clearfix"></div><br>
                    </div>

                      <img src="/assets/images/gift/iconall.png" alt=""> <input id="rangeValue1" type="text" size="5" >
                   
                  </div>
               </div>

               </div>
               <div class="col-md-9">
                  <div class="page_title"><?php echo $gift_name ; $deal_name_arr = array(); $deal_area_arr = array(); ?></div>
                  <div class='padding-both'>

                      <div id="products" class="row list-group gift_types">
                          
                        <?php $k=1; foreach($response['description'] as $row): 
                                $deal_name_arr[] = array("value" => $row['deal'],"data"=> $row['deal']);
                                $deal_area_arr[] = array("value" => ($row['area'] == null)?'':$row['area'],
                                                         "data"=>   ($row['area'] == null)?'':$row['area']);
                        ?>
                            
                       <div  data-gift-points="<?php echo $row['min_amount']; ?>" class="item col-md-4 col-xs-6">
                           <div class="thumbnail">
                              <div class="col-md-10 col-xs-10">
                                 
                                  <p class="p_detail"> <?php echo $row['offer_desc']; ?></p>
                              </div>
                              <div class="col-md-2 col-xs-2">
                                 <p class="<?php echo $cls = (($row['mylikes']==0)?"p_icon":"p_icon red_active") ; ?>"  id="<?php echo $k."_".$row['id']; ?>" onclick="set_mylike('<?php echo $k."_".$row['id']; ?>',<?php echo $row['id'];  ?>)" value="<?php echo $row['id'];  ?>" ><i class="glyphicon glyphicon-heart-empty"></i></p>
                              </div>
<!--                               <a href="/giftstore/giftdetails/<?php // echo $id_area = $row['deal_id'].'_'.trim($row['area']); ?>" style="padding-left: 1px;">
                                    <img class="group list-group-image img-responsive" src="<?php // echo $row['img_url']; ?>"  alt=""  style="width: 189px;height: 96px" />
                               </a>-->
                               <a onclick="GoToGiftDetails('<?php echo $row['deal_id']; ?>','<?php echo (isset($row['deal_url']) && !empty($row['deal_url'])) ? $row['deal_url'] : 'gift'; ?>')" style="padding-left: 1px;padding-bottom: 0px">
                                <img class="group list-group-image img-responsive" src="<?php echo $row['img_url']; ?>"  alt="" style="width:189px;height: 96px" />
                               </a>
                               
                               <div class="caption" style="padding-top:0px">
                                   <div class="row">
                                        <?php if(isset($row['by_voucher']) && !empty($row['by_voucher']) && $row['by_voucher']==1){ ?>
                                        <div class="col-md-4 col-xs-4">
                                        <?php }else{ ?>
                                        <div class="col-md-6 col-xs-6">
                                        <?php } ?>
                                       
                                             <?php if($row['logo'] == ''){ ?>
                                        <img class="logo" src="/public/assets/images/default_img/ic_logo_evoucher.png" alt="">
                                        <?php }else{ ?>
                                            <img src="<?php echo $row['logo']; ?>" class="logo"> 
                                        <?php } ?>
                                       </div>
                                       
                                       <?php if(isset($row['by_voucher']) && !empty($row['by_voucher']) && $row['by_voucher']==1){ ?>
                                    <!-- Showing both amount amount and  Loyaty points-->
                                    <div class="col-md-8 col-xs-8" style="text-align:right">
                                        <i class="fa fa-inr"></i> <?php echo $row['offer_price']; ?> + <img src="/assets/images/gift/iconall.png" alt=""> <span><?php echo $row['min_amount']; ?></span>
                                     </div>
                                        
                                    <?php }else{ ?> 
                                        <!-- Showing Only Loyaty points-->
                                        <div class="col-md-6 col-xs-6">
                                            <img src="/assets/images/gift/iconall.png" alt=""> <span><?php echo $row['min_amount']; ?></span>
                                        </div>
                                    <?php } ?>
                                        
                                      
                                       <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                          <p class="group inner list-group-item-text"><?php echo $row['deal']; ?></p>
                                          <?php if(!empty($row['area'])){  ?>
                                          <p><img src="/assets/images/gift/icongift.png" alt=""> <span><?php echo $row['area']; ?></span></p>
                                          <?php } ?>
                                       </div>
                                   </div>

                              </div>
                           </div>
                       </div>
                     <?php $k++; endforeach; ?>
                          
                    </div>
                    <div class="loader row" style="display: none;">
                        <div class="col-md-2 col-md-offset-4" ></div>
                        <img src="/public/images/bx_loader.gif">
                    </div>      
                   </div>
               
            <div class="margin-top-30 clearfix"></div>
               </div>


            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      
      
      <div class="modal fade" role="dialog" id="failure">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="cls" data-dismiss="modal">&times;</button>
                  <h3>Payment Failure</h3>
               </div>
               <div class="modal-body">
                  <p>Sorry! Your previous payment attempt failed.</p>
                  <div class="pull-right">
                     <button class="modal_button" id="try-again">Try Again</button>
                     <button class="modal_button" id="cancel-button" data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript" src="/public/js/jquery.autocomplete.min.js"></script>
       <script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
      <script type="text/javascript" src="/assets/js/jssor.slider.mini.js"></script>
<!--      <script src="/assets/js/slider.js"></script>
       <script type="text/javascript" src="<?php echo base_url(''); ?>"></script>-->
      <script type="text/javascript">
         $(document).ready(function($) {
             $('body').scrollToTop({skin: 'cycle'});
             $("#gifttypes_img").attr("src",$("#gifttypes_img_hdn").val());
             
         });
        
        
//        $(window).scroll(function() {
//        if($(window).scrollTop() >= $(document).height() - $(window).height()-1000) {
//               // ajax call get data from server and append to the div
//               scrolldown_more_gift(<?php // echo $id; ?>);
//            }
//        });

        var lat_lng = localStorage.getItem("latitude")+'_'+localStorage.getItem("longitude");
        
        var deal_name_data = <?php echo json_encode($deal_name_arr); ?> ;
        $('.autocomplete1').autocomplete({
        lookup: deal_name_data,
        onSelect: function (suggestion) {
          search_brands();
         }
        });
        
        var deal_area_data = <?php echo json_encode($deal_area_arr); ?> ;
        $('.autocomplete').autocomplete({
        lookup: deal_area_data,
        onSelect: function (suggestion) {
          search_location(suggestion.value);
         }
        });
        
        
        function giftdetail(ID){
//            alert("hello"+ID);
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/get_deal_details/?";
                        $.ajax({
                            url: url,
                            type: "GET",
                            data: {id: ID,
                                   latitude: "",
                                   longitude: "",
                                   api_version:"3",
                                   res_format: "jsonp"
                            },
                            timeout: 50000,
                            dataType: "jsonp",
                            crossDomain: true,
                            success: function(data) {
                                if (data.status == "failure"){
                    alert(data.description);
                                }
                                else{
                   console.log(data.description);
                                }
                            },
                              beforeSend: function(){
                              $('.loader').show();
                              },
                             complete: function(){
                             $('.loader').hide();
                             },
                            error: function(xhr, error) {

                           },
                        });
                    }
                
        
       

         
      </script>
        <script type="text/javascript" src="/assets/js/gift_custom.js"> </script>

<?php $this->load->view('common/footer'); ?>