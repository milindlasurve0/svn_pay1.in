<?php $this->load->view('common/doctype_html');  

    $current_page_url =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $meteTagData = getMetaTagDetails();
    foreach ($meteTagData as $row){
      
      if($row->page_url == $current_page_url){
        $new_page_tittle =  $row->page_tittle;
        $new_page_meta_description =  $row->page_meta_description;  
        $new_page_meta_tag =  $row->page_meta_tag;  
      }
      
    }
?>
<meta name="description" content=" <?php if($new_page_meta_description){ echo $new_page_meta_description; }else{ ?>Look around near you to discover the great gifts from the gift store. Using your gift coin at Pay1 grab a gift today. <?php } ?>">
<meta name="keywords" content="<?php if($new_page_meta_tag){ echo $new_page_meta_tag; }else{ ?>Discover local area near you to Grab a gift , Pay1 <?php } ?>" />
<title> <?php if($new_page_tittle){ echo $new_page_tittle; }else{ ?>Discover local area near you to Grab a gift | Pay1 <?php } ?></title>
<?php $this->load->view('common/header');  

    $domain_url = CDEV_URL;
    $imageData = getImageDetails();

    foreach ($imageData as $row){
        if(($row->image_flag == 2) && ($row->gift_type == 6)){
        $new_img_url = $domain_url.$row->image_url;   
        }
    }

?>

      <link rel="stylesheet" href="/assets/css/style.css">

      <div class="clearfix" style="clear:both;"></div>

  <!-- =========================    Near You Header Image     ========================= -->
      <input type="hidden" id="nearyou_img_hdn" value="<?php if(isset($new_img_url)){  echo $new_img_url; }else{ echo '/assets/images/background/Near-You.jpg';}  ?>">
      <div class="service_page1">
        <img src="/assets/images/default_img/pay1_loader.jpg" id="nearyou_img" alt="" class="img-responsive">
      </div>
      <div class="bottom_bg" style=""></div>
      <!-- ======================== Header Image end========================== -->
       <div class="clearfix"></div>
     
    
      <div class="container-fluid">
         <div class="container border-both">
            <div class="row">
               <div class="col-md-12 text-center margin-top-25">
                  <div class="row">
                 <form action="/giftstore/search_brand" method="POST">
                        <div class="col-md-4 col-md-offset-4">
                           <input type="text" id ="search_brand" name="search_brand" class="form-control" placeholder="Search by typing the product or brand name...">
                        </div>
                        <div class="col-md-1">
                            <input type="submit" value="SEARCH" class="btn btn-save search-btn">
                         </div>
                      </form>
               </div>
            </div>
               <div class="col-md-3 margin-top-30 ">
                  <div id="service_navigation" class="margin-top-50">
                    

                  <div id="accordian" class="border-right">
                     <ul>
                         <?php foreach($sidebarCategories->gift as $row): ?>
                            <li><a href="<?php echo 'categories-'.$row->url.'-cid-'.$row->id; ?>    "><?php echo $row->name; ?></a></li>
                         <?php endforeach; ?>
                        <!--<li class="active"><a href="near_you">Near you</a></li>-->
                          <li class="active"><a href="javascript:window.location.href='/near-you/'+lat_lng" >Near you</a></li>
                        <li >
                           <p class="mslide" style="cursor:pointer">Categories <i class="glyphicon glyphicon-chevron-down sicon"></i></p>
                           <ul>
                               <?php foreach($sidebarCategories->categories as $row): ?>
                               <li><a href="<?php echo $row->url.'-cid-'.$row->id; ?>"><?php echo $row->name; ?></a></li>
                               <?php endforeach; ?>

                           </ul>
                        </li>
                      
                        <li>
                          <p class="mslide">Locations <i class="glyphicon glyphicon-chevron-down sicon"></i></p>
                           <ul>
     <input type="text" id ="search_location" class="form-control autocomplete" name="search_location" style="width:150px;" onblur="search_location(this.value)" placeholder="Area Name" >                      </ul>
                        </li>                 
                       
                     </ul>
                     
                     <div class='col-md-11 clearfix'><h3 class="gcoin">Gift Coins</h3></div>
                     <div class="row">
                       <div class="col-md-3 col-xs-2"><button class="range_btn" data-change="-10"><img src="/assets/images/icons/minus.png" alt="" onclick="gift_filter_button('minus',<?php echo $id; ?>)"></button></div>
                          <div class="col-md-6 col-xs-4 row"><input id="slider1" type="range" min="10" max="500" step="10" class="" onchange="showVal(this.value)" /></div>
                           <div class="col-md-3 col-xs-2"> <button class="range_btn" data-change="10"><img src="/assets/images/icons/plus.png" alt="" onclick="gift_filter_button('plus',<?php echo $id; ?>)"></button></div>
                          <div class="clearfix"></div><br>
                    </div>

                      <img src="/assets/images/gift/iconall.png" alt=""> <input id="rangeValue1" type="text" size="5" >
                   
                  </div>
               </div>

               </div>
            <div class="col-md-9">
                <div class="page_title">Near you</div>
                  <div class='padding-both'>

                    <div id="products" class="row list-group">
                             
                      <?php $k=1; foreach($response['description'] as $row): ?>

                      <div  data-gift-points="<?php echo $row['actual_price']; ?>" class="item col-md-4 col-xs-6">
                        <div class="thumbnail">
                          
                          <div class="col-md-10 col-xs-10">
                            <p class="p_detail"> <?php echo $row['offer_desc']; ?></p>
                          </div>
                          
                          <div class="col-md-2 col-xs-2">
                            <p class="p_icon"   id="<?php echo $k."_".$row['id']; ?>" onclick="set_mylike('<?php echo $k."_".$row['id']; ?>',<?php echo $row['id'];  ?>)" ><i class="glyphicon glyphicon-heart-empty"></i></p>
                          </div>
<!--                            <a href="/giftstore/giftdetails/<?php // echo $id_area = $row['id'].'_'.trim("Malad West"); ?>" style="padding-left: 1px;">
                               <img class="group list-group-image img-responsive" src="<?php // echo $row['img_url']; ?>"  alt=""  />
                            </a>-->
                            
                            <a onclick="GoToGiftDetails('<?php echo $row['id'] ?>','<?php echo (isset($row['deal_url']) && !empty($row['deal_url'])) ? $row['deal_url'] : 'gift'; ?>')" style="padding-left: 1px;">
                               <img class="group list-group-image img-responsive" src="<?php echo $row['img_url']; ?>"  alt=""  />
                            </a>
                            
                            <!--<img class="group list-group-image img-responsive" src="<?php echo $row['img_url']; ?>"  alt="" />-->
                          <div class="caption">
                              <div class="row">
                                  <?php if(isset($row['by_voucher']) && !empty($row['by_voucher']) && $row['by_voucher']==1){ ?>
                                    <div class="col-md-4 col-xs-4">
                                    <?php }else{ ?>
                                    <div class="col-md-6 col-xs-6">
                                    <?php } ?>
                                     <?php if($row['logo'] == ''){ ?>
                                      <img class="logo" src="/public/assets/images/default_img/ic_logo_evoucher.png" alt="">
                                     <?php }else{ ?>
                                      <img src="<?php echo $row['logo']; ?>" class="logo"> 
                                     <?php } ?>
                                  </div>
                                  <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                       <p class="group inner list-group-item-text"><?php echo $row['dealname']; ?></p>
                                      <?php if(!empty($row['area'])){  ?>
                                       <p><img src="/assets/images/gift/icongift.png" alt=""> <span><?php echo $row['area']; ?></span></p>
                                      <?php } ?>
                                  </div>
                                </div>

                           </div>
                        </div>
                       </div>
                     <?php $k++; endforeach; ?>   
                    </div>





                  </div>
                
            <div class="margin-top-30 clearfix"></div>
               </div>


            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      
      <div class="modal fade" role="dialog" id="failure">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="cls" data-dismiss="modal">&times;</button>
                  <h3>Payment Failure</h3>
               </div>
               <div class="modal-body">
                  <p>Sorry! Your previous payment attempt failed.</p>
                  <div class="pull-right">
                     <button class="modal_button" id="try-again">Try Again</button>
                     <button class="modal_button" id="cancel-button" data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
        <script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
      <script type="text/javascript">
          
            var lat_lng = localStorage.getItem("latitude")+'_'+localStorage.getItem("longitude");
          
         $(document).ready(function($) {
             $('body').scrollToTop({skin: 'cycle'});
             $("#nearyou_img").attr("src",$("#nearyou_img_hdn").val());
             
             
         });
      </script>
        <script type="text/javascript" src="/assets/js/gift_custom.js"> </script>

<?php $this->load->view('common/footer'); ?>