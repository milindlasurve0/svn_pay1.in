<?php $this->load->view('common/doctype_html'); 

    $current_page_url =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $meteTagData = getMetaTagDetails();
    foreach ($meteTagData as $row){
      
      if($row->page_url == $current_page_url){
        $new_page_tittle =  $row->page_tittle;
        $new_page_meta_description =  $row->page_meta_description;  
        $new_page_meta_tag =  $row->page_meta_tag;  
      }
      
    }
?>
<meta property="og:url"           content="<?php echo $current_page_url; ?>" />
<meta property="og:type"          content="website" />
<meta property="og:image"         content="<?php echo $Img_url['description']['image_list'][0]['L_img'] ; ?>" />
<meta name="keywords" content="<?php if($new_page_meta_tag){ echo $new_page_meta_tag; }else{ echo $response['description']['offer_detail'][0]['name']; ?>, Gift Store , Pay1 <?php } ?>" />
<meta property="og:title"         content="Pay1 - <?php echo $response['description']['offer_detail'][0]['name']; ?> " />
<meta property="og:description"    content="<?php echo $response['description']['offer_detail'][0]['offer_desc'] ; ?>" />
<link rel="stylesheet" href="/assets/css/style.css">
 <?php $this->load->view('common/header');  ?>
 <?php 
$userData = getUserDetails();

//       echo '<pre>----';
//       print_r($Img_url['description']);
//       print_r($response);
     
//       echo 'Descr----';
//       print_r($response);
//       echo 'similar_deal----';
//       print_r($similar_deal['description']);
//       echo $area;
//       
//        echo 'lat-----'.$response['description']['location_detail'][0]['lat'];
//        echo 'lng-----'.$response['description']['location_detail'][0]['lng'];



?>
         
      
     
       <!---******************************************** Hidden Elements  ****************************************** -->
    
    <input type='hidden' id='offer_id' value='<?php echo $response['description']['offer_detail'][0]['id']; ?>'>
    <input type='hidden' id='deal_id' value='<?php echo $response['description']['offer_detail'][0]['deal_id']; ?>'>
    <input type='hidden' id='offer_price' value='<?php echo $response['description']['offer_detail'][0]['offer_price']; ?>'>
    <input type='hidden' id='actual_price' value='<?php echo $response['description']['offer_detail'][0]['actual_price']; ?>'>
    <input type='hidden' id='amount1' value="<?php echo $response['description']['offer_detail'][0]['min_amount']; ?>">
    <input type='hidden' id='by_voucher' value='<?php echo $response['description']['offer_detail'][0]['by_voucher']; ?>'>
    <input type='hidden' id='wallet_balance' value='<?php echo $userData['walletbal']; ?>'>
    <input type='hidden' id='loyalty_balance' value='<?php echo $userData['loyaltypoints']; ?>'>
    
    <input type='hidden' id='giftgrab' value='<?php if(isset($gift_status['status']) && $gift_status['status']=='success'){ echo '1'; }else if(isset($gift_status['status']) && $gift_status['status']=='failure'){ echo '2'; }else{ echo '0'; } ?>'>
    
    
    
    <!---******************************************** Hidden Elements  ****************************************** -->

      <div class="clearfix" style="clear:both;"></div>
      <div class="container-fluid">
         <div class="container border-both1 margin-top-50">
            <div class="bg col-md-12"><img src="<?php echo $Img_url['description']['image_list'][0]['L_img'] ; ?>" class="img-responsive margin-top-20" style="width:100%;" alt=""></div>
            <div class="row">
               <div class="col-md-3"><img src="<?php echo $Img_url['description']['image_list'][0]['logo_url'] ; ?>" class="img-responsive margin-top-20" alt=""  style="height:170px;width:170px;z-index: 2;border-radius: 85px; border: 1px solid #c7c7c7;"></div>
               <div class="col-md-9">
                  <p class="page_title"><?php echo $response['description']['offer_detail'][0]['offer_desc'] ; ?></p>
                  <div class="">
                     <div class="col-md-7 page_title"><?php if($response['description']['offer_detail'][0]['by_voucher']==1){ ?><i class="fa fa-inr"></i> <?php echo $response['description']['offer_detail'][0]['offer_price'] ; ?> + <?php } ?><img src="/assets/images/gift/gifti.png"><span><?php echo $response['description']['offer_detail'][0]['min_amount'] ; ?></span></div>
                     <!--<div class="col-md-4 page_title">Share<a name="fb_share" target="_blank" href="https://www.facebook.com/sharer.php?<?php // echo $current_page_url; ?>&t=TEst"><i class="fa fa-share-alt"></i></a></div>-->
                    
                     <div id="fb-root"></div>
                     
                     <div class="fb-share-button" data-href="<?php echo $current_page_url; ?>" data-layout="button_count"></div>
                    
                     <div class="col-md-5 text-right">
                        <!---********************************************model confirm****************************************** -->
                        <div class="modal fade" role="dialog" id="confirm_gift" role="dialog" aria-hidden="true">
                           <div class="modal-dialog" style="max-width:440px;">
                              <div class="modal-content paddnone">
                                 <div style="margin-right:10px;" class="modal-header1 clearfix">
                                    <button class="close" data-dismiss="modal">&times; </button>
                                    <br>
                                    <br>
                                    <div class="text-center"><h4>CONFIRMATION</h4></div>
                                 </div>
                                 <div class="modal-body clearfix text-center">
                                    <div class="text-center"><h4><?php if(isset($response['description']['offer_detail'][0]['by_voucher']) && $response['description']['offer_detail'][0]['by_voucher']==0){ ?> Do you want to pocket this gift? <?php }else { ?> Are you sure you want to grab this Voucher? <?php } ?></h4></div>
                                    <?php if(isset($response['description']['offer_detail'][0]['by_voucher']) && $response['description']['offer_detail'][0]['by_voucher']==1){ ?>
                                    <br>
                                    <br>
                                    <div class="row">
                                        <div class="" style="border:1px solid #000; width:350px; margin:0px auto;padding:0px">
                                            <div class="col-md-2"></div>
                                       <div class="col-md-3">
                                         <img style="border:1px solid #000; height:47px;width:47px;z-index: 2;border-radius: 30px;margin-top: 12px" src="<?php echo $Img_url['description']['image_list'][0]['logo_url'] ; ?>" class="img-responsive">
                                       </div>
                                       <div class="col-md-7 text-left"><br>
                                         <p class="modelheading"><i class="fa fa-inr"></i> <?php echo $response['description']['offer_detail'][0]['offer_price']; ?> + <img src="/assets/images/gift/gifti.png" alt="" width="15%" class=""> <?php echo $response['description']['offer_detail'][0]['min_amount'] ; ?></p><span class="break"></span>

                                       </div>
                                       <div class="clearfix"></div>
                                       </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                           
                                            
                                            <?php if($userData['loyaltypoints'] < $response['description']['offer_detail'][0]['min_amount']){ ?>
                                               
                                                            You have <?php echo  $userData['loyaltypoints']; ?> gifts coins. 
                                                            Recharge to earn <?php echo $response['description']['offer_detail'][0]['min_amount'] ; ?> Gifts coins <br>
                                                            OR <br>
                                                            Continue to pay full Rs. <?php echo $response['description']['offer_detail'][0]['actual_price'] ; ?>
                                                        
                                                <?php } ?>
                                                 
                                            
                                        </div>
                                    </div>
                                    <?php if(intval($userData['walletbal'])>0){ ?>
                                    <div class="row">
                                        <div class="col-md-12 text-center margin-top-20">
                                            
                                              <input type="checkbox" id="usewallet_voucher"><span style="padding-left: 10px">USE WALLET BALANCE </span>   
                                                             
                                        </div>
                                    </div>
                                     <?php } ?>
                                 <?php } ?>
                                      <div class="margin-top-50 clearfix"></div>
                                        <div class="row">
                                            <div style="float:left; width:50%;" class=""><button id="confirmyes" data-dismiss="modal" class="btn btn-default btn-block rnone">CONFIRM</button></div>
                                            <div style="float:left; width:50%;" class=""><button data-dismiss="modal" class="btn btn-default btn-block rnone">LATER</button></div>
                                        </div>
                                      
<!--                                    <div class="margin-top-50"></div>
                                    <div class="col-md-6"><button class="btn btn-default btn-block" id="confirmyes" data-dismiss="modal">CONFIRM</button></div>
                                    <div class="col-md-6"><button class="btn btn-default btn-block" data-dismiss="modal">CANCEL</button></div>-->

                                 </div>

                                
                              </div>
                           </div>
                        </div>
                       <!---********************************************model end ****************************************** -->


                        <!---********************************************model more gift coins****************************************** -->
                        <div class="modal fade" role="dialog" id="confirm_gift_error">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                 <div class="modal-header1" style="margin-right:10px;" >
                                    <button class="close" data-dismiss="modal">× </button>
                                    <br>
                                    <div class="text-center"><h4>OOPS ...</h4></div>
                                 </div>
                                 <div class="modal-body clearfix text-center">
                                     <div class="text-center" ><h4><label id="gift_grab_error"></label></h4></div>
                                    <div class="margin-top-50"></div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4"><button class="btn btn-default btn-block" data-dismiss="modal">OK</button></div>
                                    <div class="col-md-4"></div>
                                    
                                 </div>
                                  
                              </div>
                           </div>
                        </div>
                  
                       <!---********************************************model end ****************************************** -->
                       
                       <!---********************************************model less gift coins****************************************** -->
                       <div class="modal fade" role="dialog" id="less_giftcoin_error">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                 <div class="modal-header1" style="margin-right:10px;" >
                                     <br>
                                    <button class="close" data-dismiss="modal">× </button>
                                    <div class="text-center"><h4>OOPS YOU NEED MORE <?php echo ($response['description']['offer_detail'][0]['min_amount']-$userData['loyaltypoints']); ?></span> GIFT COINS </h4></div>
                                 </div>
                                 <div class="modal-body clearfix text-center">
                                    <div class="text-center" id='grab_error'><h4>You have <?php echo $userData['loyaltypoints'] ; ?> Gift Coin.<br> Recharge now to earn more gift coins and get this gifts</h4></div>
                                    <div class="margin-top-50"></div>
                                    <div class="col-md-6"><button class="btn btn-default btn-block" id="confirmyes">RECHARGE</button></div>
                                    <div class="col-md-6"><button class="btn btn-default btn-block" data-dismiss="modal">LATER</button></div>

                                 </div>
                                  
                              </div>
                           </div>
                        </div> 
                       <!---********************************************model end ****************************************** -->

                        <!---********************************************model confirm success ****************************************** -->
                        <div class="modal fade" role="dialog" id="confirm_congratulation" role="dialog" aria-hidden="true">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                 <div class="modal-header1" style="margin-right:10px;" >
                                    <button class="close" data-dismiss="modal">× </button>
                                    <br>
                                    <div class="text-center"><h4>CONGRATULATIONS</h4></div>
                                 </div>
                                 <div class="modal-body clearfix text-center">
                                    <div class="text-center"><h4>This is yours now, go to the shop and get it.</h4></div>
                                    <div class="margin-top-50 "></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <button class="btn btn-default btn-block " data-dismiss="modal">DONE</button>
                                    </div>
                                     <div class="col-md-3"></div>
                                 </div>
                                
                              </div>
                           </div>
                        </div>
                       
                       <!---********************************************model end ****************************************** -->

                       <div class="loader" style="display:none;">
			<center>
			<img class="loading-image" src="/images/bx_loader.gif" alt="loading..">
			</center>
                    </div>
                       <div class="pull-right"><input type="submit" value="GET IT NOW" id="confirm" class="btn getitnow btn-save btn-block" style="width:148px;height:45px;border-radius:5px"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-12 margin-top-20">
               <div class="row">
                   <div class="col-md-3 gift_tel"><?php if(isset($response['description']['offer_detail'][0]['dealer_contact']) && !empty($response['description']['offer_detail'][0]['dealer_contact'])){ ?><span style="color:#17a086;"><i class="fa fa-phone"></i>  </span> tel:<?php echo  $response['description']['offer_detail'][0]['dealer_contact']; } ?></div> 
                  <div class="col-md-6"><?php if(isset($response['description']['location_detail'][0]['distance']) && !empty($response['description']['location_detail'][0]['distance'])){ ?><img src="/assets/images/gift/gift1.jpg" alt="">  <span class="gifthed"><?php echo $area.' : '.ceil($response['description']['location_detail'][0]['distance']).'KM'; ?></span><?php } ?></div>
                  <div class="clearfix"></div>
                  <div class="row margin-top-20" id='coupon_details' style="display: <?php if(isset($gift_status['status']) && $gift_status['status']=='success'){ echo "block"; }else{ echo "none"; } ?>">
                     <div class="col-md-3"></div>
                     <div class="col-md-6">
                          <h4 >Coupon Details:</h4>
                          <ul>
                              <li>Mobile Number : xxxxxx<?php echo substr($this->session->userdata['user']['mobile'],6); ?></li>
                              <li>Voucher Code : <span id="voucher"><?php if(isset($gift_status['code']) && !empty($gift_status['code'])){ echo $gift_status['code']; } ?></span></li>
                              <li>Pin : <span id="pin"><?php if(isset($gift_status['pin']) && !empty($gift_status['pin'])){ echo $gift_status['pin']; } ?></span></li>
                              <li>Expiry Date : <span id='expiry_date'><?php if(isset($gift_status['expiry_date']) && !empty($gift_status['expiry_date'])){ echo $gift_status['expiry_date']; } ?></span></li>
                          </ul>
                     </div>
                     <div class="col-md-3"></div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="row margin-top-20">
                     <div class="col-md-3"></div>
                     <div class="col-md-6 ">
                         <h4>Offer Details:</h4>
                        <div class="ul_bullet">
                            <?php echo isset($response['description']['offer_info_html']) ? $response['description']['offer_info_html'] : ""; ?>
                        </div>
                        <div class="margin-top-20"></div>
                        
                        <?php if(isset($response['description']['location_detail'][0]['full_address'])) { ?>
                        <h4>Address:</h4>
                        <ul class="gift">
                           <li><?php echo isset($response['description']['location_detail'][0]['full_address']) ? $response['description']['location_detail'][0]['full_address'] : ""; ?>
                               <br><?php // echo isset($response['description']['location_detail'][0]['city']) ? $response['description']['location_detail'][0]['city'] : "" ; ?>
                           </li>
                        </ul><?php } ?>
                        <?php if(isset($response['description']['location_detail'][0]['address'])) { ?>
                        
                        <h4>Map:</h4>
                        <div class='col-md-8'>
                              <div id="giftmap">
                              </div>
                        </div><?php } ?>
                        <div class="margin-top-50"></div>
                        
                        <?php if(isset($response['description']['deal_detail'][0]['content_txt']) && !empty($response['description']['deal_detail'][0]['content_txt'])) { ?>
                           <div class="termcondition" style="clear:both;padding-top:10px">
                              <h4>Terms & Conditions</h4>
                              <div class="ul_bullet">
                                 <?php echo  $response['description']['deal_detail'][0]['content_txt']; ?>
                               </div>
                             
                            </div><?php } ?>
                        
                        <div class="text-right col-md-12">
                           <p id="viewmore"> VIEW MORE &nbsp; &nbsp; <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                     <div class="text-right col-md-12" id="viewless"> VIEW LESS &nbsp; &nbsp; <i class="glyphicon glyphicon-chevron-up "> </i></div>
                        
                     </div>
                     <div class="col-md-3">
                        <h4>
                        Similar Offers:
                        </h4>
                        <div class="margin-top-20"></div>
                        <div class=" text-center">
                            
                           <?php foreach($similar_deal['description']['SimilarDeal'] as $row): ?> 
                            <a href='<?php echo $id_area = $row['id'].'_'.trim($area); ?>'>
                              <div class="parent_pos">
                                 <img src="<?php echo $row['S_img'] ?>" alt="">
                                 <div class="child_pos white discount_item" style="padding: 0px; position: relative;"><?php echo $row['name'] ?></div>
                              </div>
                            </a>
                           <?php endforeach; ?> 
                            
                        </div>
                     </div>
                     <div class="">
                        
                            
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      </div><div class="margin-top-30 clearfix"></div>
      </div>
      <div class="clearfix"></div>
      
      <div class="modal fade" role="dialog" id="failure">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="cls" data-dismiss="modal">&times;</button>
                  <h3>Payment Failure</h3>
               </div>
               <div class="modal-body">
                  <p>Sorry! Your previous payment attempt failed.</p>
                  <div class="pull-right">
                     <button class="modal_button" id="try-again">Try Again</button>
                     <button class="modal_button" id="cancel-button"data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
            <script src="/assets/js/bootstrap.min.js"></script>
       <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script type="text/javascript">
            function initialize() {
                var lat = '<?php echo $response['description']['location_detail'][0]['lat'] ;?>';
                var lng = '<?php echo $response['description']['location_detail'][0]['lng'] ;?>';
              var mapOptions = {
                zoom: 12,
                center: new google.maps.LatLng(lat,lng)
              };
              var image2 = '/assets/images/map/0.png';
              var mapDiv = document.getElementById('giftmap');
              var map = new google.maps.Map(mapDiv, mapOptions);
              var myLatLng = new google.maps.LatLng(lat, lng);
              var beachMarker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image2
            });
            }
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
        
      <script>
         $(document).ready(function(){
             $(".ul_bullet").children("ul").addClass('list_bullet')
             $('body').scrollToTop({skin: 'cycle'});
             if($('#giftgrab').val()=='1'){
                   b2c.core.update_profile();
                    
                  $('#confirm_congratulation').modal({
                    show: true
                });
             }else if($('#giftgrab').val()=='2'){
                 $('#confirm_gift_error').modal({
                    show: true
                });
             }
            if($('.termcondition').is(':visible')){
               $('#viewmore').hide();
         
            }else{
               $('#viewless').hide();
            }
            $('#viewmore').click(function(){
               $('.termcondition').toggle('slow');
               $('#viewless').show();
               $(this).hide();
            });
            $('#viewless').click(function(){
               $('.termcondition').toggle('slow');
               $('#viewmore').show();
               $(this).hide();
            });
              
            $('#confirm').click(function () { 
                var amount = parseInt($("#amount1").val());
                var by_voucher =  $("#by_voucher").val();
                var balance = parseInt($('#loyalty_balance').val());
                console.log('amount '+amount+' by_voucher '+by_voucher+' '+$('#loyalty_balance').val());
               if(balance < amount && by_voucher==0){ // not e-voucher
                    $('#less_giftcoin_error').modal({
                        show: true
                    });
                }
                else{
                    $('#confirm_gift').modal({
                        show: true
                    });
                }
        
                
            });
            $('#confirmyes').click(function(){
               // $('#confirm_gift').hide();
                $('#confirm_gift').modal({
                    hide: true
                });
                    
                 var offer_id = $("#offer_id").val();
		 var deal_id = $("#deal_id").val();
		 var offer_price = $("#offer_price").val();
		 var amount = parseInt($("#amount1").val());
		 var by_voucher =  $("#by_voucher").val();
                 
                b2c.core.purchase_deal(offer_id,deal_id,offer_price,amount,by_voucher);
               
         });
         (function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1636095206674498&link=<?php echo urlencode($current_page_url); ?>";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
     });
            
            
            

      </script>
<?php $this->load->view('common/footer'); ?>
      
