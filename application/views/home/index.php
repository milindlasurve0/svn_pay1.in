<?php $this->load->view('common/doctype_html');  

    $current_page_url =  "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $meteTagData = getMetaTagDetails();
    foreach ($meteTagData as $row){
      
      if($row->page_url == $current_page_url){
        $new_homepage_tittle =  $row->page_tittle;
        $new_homepage_meta_description =  $row->page_meta_description;  
        $new_homepage_meta_tag =  $row->page_meta_tag;  
      }
      
    }
?>
<meta name="description" content=" <?php if($new_homepage_meta_description){ echo $new_page_meta_description; }else{ ?>Pay1 - Easy & fast way to Recharge any Mobile, DTH or postpaid & with paying in Cash option at store. Get Gift Coins on every recharge & grab Exciting Gifts. <?php } ?>">      
<meta name="keywords" content="<?php if($new_homepage_meta_tag){ echo $new_homepage_meta_tag; }else{ ?>Online Mobile, DTH & Data Card Recharge , Postpaid , Pay1 <?php } ?>" />
<title><?php if($new_homepage_tittle){ echo $new_homepage_tittle; }else{ ?>Online Mobile, DTH & Data Card Recharge | Postpaid | Pay1 <?php } ?></title>
<?php $this->load->view('common/header');  

$domain_url = CDEV_URL;
$imageData = getImageDetails();

    foreach ($imageData as $row){
        
        if($row->image_flag == 1){
            
        
        switch ($row->image_sequence) {
            case 1:
                $new_image_sequence_1 = 1;  
                $new_img_url_1 = $domain_url.'/'.$row->image_url;   
                $large_text_1  = $row->big_text;
                $small_text_1  = $row->small_text;
                break;
            case 2:
                $new_image_sequence_2 = 2;  
                $new_img_url_2 = $domain_url.'/'.$row->image_url;   
                $large_text_2  = $row->big_text;
                $small_text_2  = $row->small_text;
                break;
            case 3:
                $new_image_sequence_3 = 3;  
                $new_img_url_3 = $domain_url.'/'.$row->image_url;   
                $large_text_3  = $row->big_text;
                $small_text_3  = $row->small_text;
                break;
            case 4:
                $new_image_sequence_4 = 4;  
                $new_img_url_4 = $domain_url.'/'.$row->image_url;   
                $large_text_4  = $row->big_text;
                $small_text_4  = $row->small_text;
                break;
            case 5:
                $new_image_sequence_5 = 5;  
                $new_img_url_5 = $domain_url.'/'.$row->image_url;   
                $large_text_5  = $row->big_text;
                $small_text_5  = $row->small_text;
                break;
            default:
                $new_image_sequence_1 = 1;  
                $new_img_url_1 = $domain_url.'/'.$row->image_url;   
                $large_text_1  = $row->big_text;
                $small_text_1  = $row->small_text;
            }
        }
    }
    
?>
      <div class="clearfix"></div>
        <!-- =========================    slider STart here------       ========================= -->
      <div style="min-height: 530px; font-family:'openSans';  font-family: 'opensanslight';margin-top:0px">
       
         <div id="slider1_container" style="display: none; position: relative; margin: 0 auto;
            top: 0px; left: 0px; width: 1300px; height: 540px; overflow: hidden;">
            <!-- Loading Screen -->
            <div u="loading" style="position: absolute; top: 0px; left: 0px;">
               <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block;
                  top: 0px; left: 0px; width: 100%; height: 100%;">
               </div>
               <div style="position: absolute; display: block; background: url(../images/loading.gif) no-repeat center center;
                  top: 0px; left: 0px; width: 100%; height: 100%;">
               </div>
            </div>
            <!-- Slides Container -->
            <div u="slides" style="cursor: pointer; position: absolute; left: 0px; top: 0px; width: 1300px; height: 540px; overflow: hidden;">
            <div>
                <?php if($new_image_sequence_1){ ?>
                <img u="image" src2=<?php echo $new_img_url_1 ; ?> />
                <?php }else{ ?> 
                <img u="image" src2="/assets/images/slider/slide_image0.jpg" />
                <?php } ?>
                <div style="height: 100%;width: 100%;position: absolute;">
                  <div style="position:absolute;width:1300px;height:100px;top:220px;left:30px;padding:5px;text-align:center;line-height:36px;font-size:50px;color:#FFFFFF; font-weight:bold">
                    <?php if(isset($new_image_sequence_1)){ echo $large_text_1.'<br><br><h3><strong>'.$small_text_1.'</strong></h3>'; }else{ ?>     
                      Recharge wala dost <br><br>
                     <h3><strong> Recharge anywhere, anytime </strong></h3>
                    <?php } ?>
                  </div>
                </div>
            </div>
            <div>
                <?php if(isset($new_image_sequence_2)){ ?>
                <img u="image" src2=<?php echo $new_img_url_2 ; ?> />
                <?php }else{ ?> 
                <img u="image" src2="/assets/images/slider/slide_image1.jpg" />
                <?php } ?>
                <div style="background-color: rgba(0,0,0,0.5); height: 100%;width: 100%;position: absolute;">
                  <div style="position:absolute;width:1300px;height:100px;top:220px;left:30px;padding:5px;text-align:center;line-height:36px;font-size:50px;color:#FFFFFF; font-weight:bold">
                    <?php if(isset($new_image_sequence_2)){ echo $large_text_2.'<br><br><h3><strong>'.$small_text_2.'</strong></h3>'; }else{ ?>     
                      Instant online recharge<br><br>
                     <h3><strong> Quick & convenient to use </strong></h3>
                    <?php } ?>
                  </div>
                </div>
            </div>
            <div>
                <?php if(isset($new_image_sequence_3)){ ?>
                <img u="image" src2=<?php echo $new_img_url_3 ; ?> />
                <?php }else{ ?> 
                <img u="image" src2="/assets/images/slider/slider_image2.jpg" />
                <?php } ?>
                <div style="background-color: rgba(0,0,0,0.5); height: 100%;width: 100%;position: absolute;">
                  <div style="position:absolute;width:1300px;height:100px;top:220px;left:30px;padding:5px;text-align:center;line-height:36px;font-size:50px;color:#FFFFFF;font-weight:bold;">
                    <?php if(isset($new_image_sequence_3)){ echo $large_text_3.'<br><br><h3><strong>'.$small_text_3.'</strong></h3>'; }else{ ?>     
                      Cash topup available<br><br>
                     <h3><strong> Recharge with cash at Pay1 store </strong></h3>
                    <?php } ?> 
                  </div>
                </div>
            </div>
            <div>
                <?php if(isset($new_image_sequence_4)){ ?>
                <img u="image" src2=<?php echo $new_img_url_4 ; ?> />
                <?php }else{ ?> 
                <img u="image" src2="/assets/images/slider/slider_image3.jpg" />
                <?php } ?>
                <div style="background-color: rgba(0,0,0,0.5); height: 100%;width: 100%;position: absolute;">
                  <div style="position:absolute;width:1300px;height:100px;top:220px;left:30px;padding:5px;text-align:center;line-height:36px;font-size:50px;color:#FFFFFF; font-weight:bold">
                    <?php if(isset($new_image_sequence_4)){ echo $large_text_4.'<br><br><h3><strong>'.$small_text_4.'</strong></h3>'; }else{ ?>  
                      Safe & secure transactions<br><br>
                    <h3><strong> Customer money is always safe  </strong></h3>
                    <?php } ?> 
                  </div>
                </div>
            </div>
            <!--<div>-->
                <?php // if(isset($new_image_sequence_5)){ ?>
                <!--<img u="image" src2=<?php // echo $new_img_url_5 ; ?> />-->
                <?php // }else{ ?> 
                <!--<img u="image" src2="/assets/images/slider/slider_image4.jpg" />-->
                <?php // } ?>
                <!--<div style="background-color: rgba(0,0,0,0.5); height: 100%;width: 100%;position: absolute;">-->
                  <!--<div style="position:absolute;width:1300px;height:100px;top:220px;left:30px;padding:5px;text-align:center;line-height:36px;font-size:50px;color:#FFFFFF;font-weight:bold;">-->
                    <?php // if(isset($new_image_sequence_5)){ echo $large_text_5.'<br><br><h3><strong>'.$small_text_5.'</strong></h3>'; }else{ ?>  
                      <!--Safe & Secure transaction<br><br>-->
                     <!--<h3><strong> Safety & security is on our priority list </strong></h3>-->
                    <?php // } ?>  
                  <!--</div>-->
                <!--</div>-->
            <!--</div>-->    
            </div>
         </div>
      </div>
      <!-- ========================slider end========================== -->
<!--       <div class="clearfix"></div>
      <div class="phone_number child_pos visible-md visible-lg">

<span id ="validate_mobile_num" style="color:red;"></span><br/>
<span class="phone-prefix">+91<input type="tel" maxlength="10" id="enterNumber" onkeypress="return isNumberKey(event)" onchange="return isNumberKey(event)" placeholder="Enter your mobile no"></span><input type="button" id="go" onkeypress="return isNumberKey(event)" class="phone_submit" value="Start">

      </div>-->
<div class="phone_number child_pos visible-md visible-lg" style="top: -47px; height: 46px; margin-top: 0px;">
<!--         <form class="inline-form">-->
<span style="color:red;" id="validate_mobile_num"></span>
<span class="phone-prefix" style="padding-top: 5px;">+91<input type="tel" placeholder="Enter your mobile no" onchange="return isNumberKey(event)" onkeypress="return isNumberKey(event)" id="enterNumber" maxlength="10" onclick="this.select()"></span>
<!--<input type="button" value="Start" class="phone_submit" onkeypress="return isNumberKey(event)" id="go">-->
 <button id="go" type="button" class="btn btn-danger phone_submit" onkeypress="return isNumberKey(event)">Start</button>
<!--         </form>-->
      </div>

      <div class="content">
         <div class="container">
            <div class="row how_does">
               <p class="heading black mobile_text padd-bottom">How does it work? </p>
               <div class="col-xs-12 col-sm-4  text-center icon_des">
                  <div class=""> 
                     <img src="/assets/images/icons/recharge_red.png" alt="" >
                  </div>
                  <p class="product_text">1.Recharge</p>
                  <img src="/assets/images/homepage/arrowright.png" class="img-responsive arrow_home visible-md visible-lg visible-sm" alt="">
               </div>
               <div class="col-xs-12 col-sm-4 text-center icon_des">
                  <div class=""> 
                     <img src="/assets/images/icons/collect_gift_coins_yellow.png" alt="" >
                  </div>
                  <p class="text-padding no-text-padding-10 product_text" >2.Collect Gift Coins</p>
                  <img src="/assets/images/homepage/arrow.png" alt="" class="img-responsive arrow_home_right visible-md visible-lg visible-sm">
               </div>
               <div class="col-xs-12 col-sm-4 text-center icon_des">
                  <div class=""> 
                     <img src="/assets/images/icons/exchange_coins_for_gift_blue.png" alt="" >
                  </div>
                  <p class="text-padding-10 no-text-padding product_text">3.Exchange Coins For Gifts</p>
                  <div class="clearfix"></div>
               </div>
            </div>
            <div class="text-center row clearfix">
               <button id="LetsGetStart" class="get-started">Let's Get Started</button>
            </div>
         </div>
      </div>
      <div class="content blue_bg_img white why_download">
         <div class="margin-top-40 margin-bottom-40 clearfix"></div>
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                  <p class="heading">Why Download Pay1 App?</p>
               </div>
               <div class="margin-top-br"></div>
               <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                  <img src="/assets/images/icons/recharge_2.png" alt="">
                  <h3 class="text-padding">Recharge Anything</h3>
                  <p class="text-padding why_text">Recharge ANY mobile phone, DTH service or bill payment.</p>
               </div>
               <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                  <img src="/assets/images/icons/convenience_of_cash_2.png" alt="">
                  <h3 class="text-padding">Convenience of Cash</h3>
                  <p class="text-padding why_text">No credit/debit card? No problem. Enjoy the convenience of paying in CASH!</p>
               </div>
               <div class="clear"></div>
               <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                  <img src="assets/images/icons/get_exciting_gifts_2.png" alt="">
                  <h3 class="text-padding">Get Exciting Gifts</h3>
                  <p class="text-padding why_text">Earn Gift Coins on every recharge & exchange for fun gifts! (Rs. 1 Recharge = 1 Gift Coin!)</p>
               </div>
               <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                  <img src="assets/images/icons/fast_secure_tech_2.png" alt="">
                  <h3 class="text-padding">Fast, Secure Technology</h3>
                  <p class="text-padding why_text">Experience the most reliable recharge platform for quick, error-free recharge!</p>
               </div>
               <div class="clearfix"></div>
                <div class="text-center clearfix col-md-12">
                  <a style="color:#FFF;" href="https://play.google.com/store/apps/details?id=com.pay1&hl=en" target="_blank"><button class="download_app">Download App</button></a>
                </div>
            </div>
         </div>
         <div class=" margin-bottom-40 clearfix"></div>
      </div>
      
      <div class="clearfix"></div>
      <div class="content no-padding-top">
         <div class="container">
            <div class="row">
               <p class="heading margin-top-40 margin-bottom-40 mobile_padd clearfix">How will it benefit me?</p>
               <div class="col-xs-12">
                  <ul class="benefits"  style="font-size:18px">
                     <li class="row margin-bottom-20">
                        <div class="col-sm-12 visible-sm visible-xs">
                           <img src="assets/images/icons/its_fastest_2.png" class="img-responsive img_center home_img" alt="">
                        </div>
                        <div class= "col-md-11 col-sm-12">
                           <p class="text-right mobile_center" id="its_fast">It's the fastest, most secure recharge app available. Don't take our word for it,experience it yourself.</p>
                        </div>
                        <div class="col-md-1 visible-md visible-lg ">
                           <img src="assets/images/icons/its_fastest_2.png" alt="" class="img-responsive img_center home_img">
                        </div>
                     </li>
                     <li class="row margin-bottom-20">
                        <div class="col-sm-12 visible-sm visible-xs">
                           <img class="img-responsive img_center home_img" src="assets/images/icons/you_can_pay_2.png" alt="">
                        </div>
                        <div class="col-md-1 visible-md visible-lg ">
                           <img class="img-responsive img_center home_img" src="assets/images/icons/you_can_pay_2.png" alt="">
                        </div>
                        <div class= "col-md-11 col-sm-12">
                           <p id="you_can_pay" class="text-left mobile_center"> You can pay cash to topup your Pay1 wallet from your neighbourhood store, and recharge anytime anywhere - For yourself, friends and family.</p>
                        </div>
                     </li>
                     <li class="row margin-bottom-20">
                        <div class="col-sm-12 visible-sm visible-xs">
                           <img src="assets/images/icons/each_rupee_ofrecharge_2.png" alt="" class="img-responsive img_center home_img">
                        </div>
                        <div class= "col-md-11 col-sm-12">
                           <p  class="text-right mobile_center" id="each_rupee"> There's more! Each rupee of recharge, earns you FREE Gift Coins that you can exchange for a wide range of Fun Gifts and offers from neighbourhood stores and online shops!</p>
                        </div>
                        <div class="col-md-1 visible-md visible-lg "><img src="assets/images/icons/each_rupee_ofrecharge_2.png" class="img-responsive img_center home_img" alt="">
                        </div>
                     </li>
                  </ul>
               </div>
               <div class="margin-top-40 margin-bottom-40 clearfix"></div>
            </div>
         </div>
      </div>
      <div class="content red_bg_img white">
         <div class="container ">
            <div class="row">
               <div class="col-md-12">
                  <p class="heading margin-top-40">Our Brand Partners</p>
               </div>
            </div>
            <div class=" margin-bottom-40 clearfix"></div>
            <div class="container mobile-width">
               <div class="row">
                  <div class="col-md-2 col-xs-6">
                     <div class="brand_img">
                        <img src="assets/images/brands/ccd.png" alt="" class="img-responsive">
                     </div>
                  </div>
                  <div class="col-md-2 col-xs-6">
                     <div class="brand_img">
                        <img src="assets/images/brands/mongini.png" alt="" class="img-responsive">
                     </div>
                  </div>
                  <div class="col-md-2 col-xs-6">
                     <div class="brand_img">
                        <img src="assets/images/brands/birdys.png" alt="" class="img-responsive">
                     </div>
                  </div>
                  <div class="col-md-2 col-xs-6">
                     <div class="brand_img">
                        <img src="assets/images/brands/bookmyshow.png" alt="" class="img-responsive">
                     </div>
                  </div>
                  <div class="col-md-2 col-xs-6">
                     <div class="brand_img">
                        <img src="assets/images/brands/puma.png" alt="" class="img-responsive">
                     </div>
                  </div>
                  <div class="col-md-2 col-xs-6">
                     <div class="brand_img">
                        <img src="/assets/images/brands/shopper.png" alt="" class="img-responsive">
                     </div>
                  </div>
               </div>
               <div class="margin-top-40 margin-bottom-40 clearfix"></div>
            </div>
         </div>
      </div>
      <div class="map1 visible-md visible-lg">
         <div class="container-fluid">
            <div class="row">
            <div class="maphome visible-md visible-lg" style="margin-top: -78px;">
              <img src="/assets/images/homepage/homepagemap.png" alt="" class="img-responsive">
            </div>
            </div>
         </div>
      </div>
      <div class="content">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <p class="heading margin-top-40 mobile_left ">Exciting Gifts & Offers <span class="pull-right v"><a class="see_offers" href="/giftstore">SEE ALL <span class=""><i class="glyphicon glyphicon-chevron-right mob"></i></span></a></span>
               </div>
               <div class="margin-bottom-40 clearfix"></div>
               <div class="col-md-4 text-center margin-bottom-30">
                  <a href='/giftstore'>
                     <div class="parent_pos">
                        <img src="assets/images/discount/discount_food.png" alt="">
                        <div class="child_pos white discount_item">Enjoy15% Discount on Biryani</div>
                     </div>
                  </a>
               </div>
               <div class="visible-sm visible-xs border_img clearfix"></div>
               <div class="col-md-4 text-center margin-bottom-30">
                  <a href='/giftstore'>
                     <div class="parent_pos">
                        <img src="assets/images/discount/discount_clothes.png" alt="">
                        <div class="child_pos white discount_item">20% Discount on Clothing</div>
                     </div>
                  </a>
               </div>
               <div class="visible-sm visible-xs border_img clearfix"></div>
               <div class="col-md-4 text-center margin-bottom-30">
                  <a href='/giftstore'>
                     <div class="parent_pos">
                        <img src="assets/images/discount/discount_gym.png" alt="">
                        <div class="child_pos white discount_item">Get 2 Months Membership FREE!</div>
                     </div>
                  </a>
               </div>
            </div>
            <div class="margin-top-40 margin-bottom-40 clearfix"></div>
         </div>
      </div>
      <div class="margin-top-30"></div>
      <div class="margin-top-30"></div>
       <script type="text/javascript">
         $(document).ready(function($) {
            
            //Collapse Rechargemenu tap after entering user mobile number and press on red START button  
              $("#go").click(function(){
			var number = $("#enterNumber").val();
	            if(number !='' && (IsMobileNumber(number))){
		        $(this).attr('data-target','#rechargemenu');
			$(this).attr('data-toggle','collapse');
			$("#mob_number").val(number);
			$("#post_number").val(number);
			$("#data_number").val(number);
			$("#dth_number").val(number);
			getnumberDetails(number,'mob_provider','');
	            } else {
                        $("#validate_mobile_num").html("<small><strong>Please enter valid 10 digits Mobile Number</strong></small>");
		        $("#enterNumber").val('');
                        $("#enterNumber").focus();
	            }
		});
            
            //Fcousing on Enter Mobile number textbox bydefault
            $("#enterNumber").focus();     
         });
         
        //Validate mobile number only starts with 7,8,9 and 10 digits
            function IsMobileNumber(txtMobile) {
             var mob = /^[789][0-9]{9}$/;
                if (mob.test(txtMobile) == false) {
                    return false;
                }
                $("#validate_mobile_num").html(" ");
             return true;
            }

        //Collapse Rechargemenu tap on clicking Let's Get Start button   
         $("#LetsGetStart").click(function() {
            $(".collapse").collapse('show');
          });
        
        //Collapse Rechargemenu tap after entering user mobile number and press on ENTER key  
        document.getElementById('enterNumber').onkeydown = function(event) {
            if (event.keyCode == 13) {
                var number = $("#enterNumber").val();
                if(number !='' && (IsMobileNumber(number))){
                    $(".collapse").collapse('show');
                    $("#mob_number").val(number);
                    $("#post_number").val(number);
                    $("#data_number").val(number);
                    $("#dth_number").val(number);
                    getnumberDetails(number,'mob_provider','');
                } else {
                    $("#validate_mobile_num").html("<small><strong>Please enter valid 10 digits Mobile Number</strong></small>");
                    $("#enterNumber").val('');
                    $("#enterNumber").focus();
                }
            }
        };  
          
      </script>
      <?php $this->load->view('common/footer');  ?>
   </body>
</html>
