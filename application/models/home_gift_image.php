<?php

class Home_Gift_Image extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    //function to change home page image from backend panel
    function getImageDetails($data=NULL)
    {   
       $query = $this->db->query('SELECT * FROM web_section');
       $result = $query->result();
       return  $result;
    }
    
     //function to change meta tags , description and page tittle from backend panel
    function getMetaTagDetails($data=NULL)
    {   
       $query = $this->db->query('SELECT * FROM web_metatag_details');
       $result = $query->result();
       return  $result;
    }
    
     //function to retrive all the deatails from HR_panel including job tittle and job description from backend panel
    function getAllJobDetails($data=NULL)
    {   
       $query = $this->db->query('SELECT * FROM HR_panel');
       $result = $query->result();
       return  $result;
    }
    
}